using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TCG.Utility;
using TCG.Enums;
using Attribute = TCG.Enums.Attribute;
using System.Text;

public class Card : BaseNonBehaviour, ICard
{
	public string Name { get; protected set; }
    public string ImageString { get; protected set; }
    public int Cost { get { return _cost + TemporaryCardChanges.Sum(x => x.First.Cost); } protected set { _cost = value; } }
    public List<Attribute> Attributes { get; protected set; }
    public string BaseText { get; private set; }
    public List<CardType> Type { get; protected set; }
    public string SubType {
        get { return _subType + string.Join(" ", TemporaryCardChanges.Select(x => x.First.SubType).ToArray()); }
        protected set { _subType = value; }
    }


    public int Attack { get { return _attack + TemporaryCardChanges.Sum(x => x.First.Attack); } protected set { _attack = value; } }
    public DamageTypes DamageType { get; protected set; }
    public int Defense { get { return _defense + TemporaryCardChanges.Sum(x => x.First.Defense); } protected set { _defense = value; } }

    public int _cost = 0;
    protected string _subType = "";
    protected int _attack = 0;
    protected int _defense = 0;
    public List<IAction> _actions = new List<IAction>();

    public List<Tuple<CardChangeData, int>> TemporaryCardChanges = new List<Tuple<CardChangeData, int>>();

    public Color ImageColoration { get; protected set; }
    public CardRarity Rarity { get; protected set; }
    public int CardNumber { get; set; }
    public string Artist { get; set; }
    public string FlavorText { get; set; }
    public string CollectorsInfo { get; set; }
    public string Set { get; set; }
    public string CardSetId { get; set; }

    public List<Traits> Traits { get; protected set; }
    public List<IAction> Actions { get { return _actions.Union(TemporaryCardChanges.SelectMany(x => x.First.Actions)).ToList(); } }

    public Tuple<string, string> BaseCardNameAndSet { get; set; }
    public List<Card> RelatedCards
    {
        get {
            if (Actions != null && Actions.Any())
                return Actions.SelectMany(a => a.GetRelatedCards()).ToList();
            else
                return new List<Card>();
        }
            
    }

    public BasePlayerController Owner { get; private set; }
    public BasePlayerController Controller { get; private set; }
    public ICardObject Animatable { get; private set; }

    public string Text {
        get
        {
            var sb = new StringBuilder();
            foreach(var action in Actions)
            {
                sb.AppendLine(action.ToString());
            }

            return sb.ToString();
        }
    }

    public Card (JSONNode cardJson, JSONNode csJson, JSONNode setNode) : base()
	{
		Name = cardJson["Name"];
        Set = setNode["Set Id"];
        CardSetId = csJson["id"].QValue;
		ImageString = csJson["Card Image"].AsArray[0]["url"].Value;
		Cost = cardJson["Cost"].AsInt;
		BaseText = cardJson["Text"];
        Attributes = ResolveAttributeFromString(cardJson["Attribute Requirement"].QValue);

        Attack = cardJson["Attack"].AsInt;
        DamageType = ResolveDamageTypeFromString(cardJson["Damage Type"].Value);
        Defense = cardJson["Defense"].AsInt;

        SubType = ResolveCardSubtype(cardJson["Subtype"].AsArray);
        Type = ResolveCardTypeFromString(cardJson["Type"].AsArray);

        CardNumber = csJson["Card Number"].AsInt;
        Artist = csJson["Artist"].QValue.ToUpperInvariant();
        FlavorText = csJson["FlavorText"].Value;
        Rarity = ResolveRarity(csJson["Card Rarity"]);
        CollectorsInfo = string.Format("{0} - {1}/{2} {3}", Set, CardNumber, 0, ShortFormRarity(Rarity));

        //processEffects
        Traits = new List<Traits>();
        SetupActionsAndRelated();
        UpdateCardState();
        BaseCardNameAndSet = new Tuple<string, string>(Name, CardSetId);

        Subscribe(EventNames.StartOfTurn, OnStartOfTurn);
	}
    public Card(string name, string set, string cardsetId, string imgStr, int cost, string text, 
                List<Attribute> attr, int atk, DamageTypes dt, int def, string subtype,
                List<CardType> type, CardRarity rarity, List<IAction> acts,
                string collectors, string artist, int cardNumber, string flavor,
                List<Card> related, BasePlayerController owner, BasePlayerController controller) : base()
    {
        Name = name;
        Set = set;
        CardSetId = cardsetId;
        ImageString = imgStr;
        Cost = cost;
        BaseText = text;
        Attributes = attr;
        Attack = atk;
        DamageType = dt;
        Defense = def;
        SubType = subtype;
        Type = type;
        Rarity = rarity;
        CollectorsInfo = collectors;
        Artist = artist;
        CardNumber = cardNumber;
        FlavorText = flavor;
        Traits = new List<Traits>();
        Owner = owner;
        Controller = controller;
        SetupActionsAndRelated();

        UpdateCardState();
        BaseCardNameAndSet = new Tuple<string, string>(Name, CardSetId);
        Subscribe(EventNames.StartOfTurn, OnStartOfTurn);
    }
    public Card()
    {
        Name = "TestCard";
        ImageString = "";
        Cost = 1;
        BaseText = "This is some text";

        Attack = 1;
        DamageType = DamageTypes.Melee;
        Defense = 1;

        SubType = "Minion";
        Type = new List<CardType>{ CardType.UNIT };

        BaseCardNameAndSet = new Tuple<string, string>(Name, CardSetId);
        Subscribe(EventNames.StartOfTurn, OnStartOfTurn);
    }

    public Card GetCard()
    {
        return this;
    }

    #region Card Information Strings
    private List<CardType> ResolveCardTypeFromString(JSONArray typeArray)
    {
        var output = new List<CardType>();
        var typeString = new List<string>();

        foreach(JSONNode node in typeArray)
        {
            typeString.Add(node.Value);
        }

        if (typeString.Contains(CardType.UNIT.ToString()) || typeString.Contains("Unit"))
            output.Add(CardType.UNIT);
        if (typeString.Contains(CardType.ABILITY.ToString()) || typeString.Contains("Ability"))
            output.Add(CardType.ABILITY);
        if (typeString.Contains(CardType.FAST.ToString()) || typeString.Contains("Fast"))
            output.Add(CardType.FAST);
        if (typeString.Contains(CardType.CHARM.ToString()) || typeString.Contains("Charm"))
            output.Add(CardType.CHARM);
        if (typeString.Contains(CardType.RELIC.ToString()) || typeString.Contains("Relic"))
            output.Add(CardType.RELIC);
        if (typeString.Contains(CardType.HERO.ToString()) || typeString.Contains("Hero"))
            output.Add(CardType.HERO);
        if (typeString.Contains(CardType.ATTACHMENT.ToString()) || typeString.Contains("Attachment"))
            output.Add(CardType.ATTACHMENT);
        if (typeString.Contains(CardType.KEYWORD.ToString()) || typeString.Contains("Keyword"))
            output.Add(CardType.KEYWORD);
        if (typeString.Contains(CardType.LOCATION.ToString()) || typeString.Contains("Location"))
            output.Add(CardType.LOCATION);
        if (typeString.Contains(CardType.UNIQUE.ToString()) || typeString.Contains("Unique"))
            output.Add(CardType.UNIQUE);
        if (typeString.Contains(CardType.RESTRICTED.ToString()) || typeString.Contains("Restricted"))
            output.Add(CardType.RESTRICTED);

        return output;
    }

    public string ResolveCardSubtype(JSONArray subtypes)
    {
        var output = new StringBuilder();

        foreach(JSONNode node in subtypes)
        {
            output.Append(node.Value + " ");
        }

        var outputStr = output.ToString();
        return outputStr.Substring(0, outputStr.Length - 1);
    }

    public virtual string TypeLine()
    {
        var sb = new List<string>();

        if (Type.Contains(CardType.FAST))
            sb.Add("Fast");
        if (Type.Contains(CardType.RESTRICTED))
            sb.Add("Restricted");
        if (Type.Contains(CardType.UNIQUE))
            sb.Add("Unique");
        if (Type.Contains(CardType.ATTACHMENT))
            sb.Add("Attachment");
        if (Type.Contains(CardType.RELIC))
            sb.Add("Relic");
        if (Type.Contains(CardType.ABILITY))
            sb.Add("Ability");
        if (Type.Contains(CardType.CHARM))
            sb.Add("Charm");
        if (Type.Contains(CardType.UNIT))
            sb.Add("Unit");
        if (Type.Contains(CardType.HERO))
            sb.Add("Hero");
        if (Type.Contains(CardType.KEYWORD))
            sb.Add("Keyword");
        if (Type.Contains(CardType.LOCATION))
            sb.Add("Location");

        var output = "";
        for(var i = 0; i < sb.Count; i++)
        {
            output += sb[i];

            if (i < sb.Count - 1)
                output += " ";
        }

        if(!string.IsNullOrEmpty(SubType))
        {
            output += " - " + SubType;
        }

        return output;
    }

    private DamageTypes ResolveDamageTypeFromString(string typeString)
    {
        if (typeString.Contains(DamageTypes.Melee.ToString()) || typeString.Contains("Melee"))
            return DamageTypes.Melee;
        else if(typeString.Contains(DamageTypes.Ranged.ToString()) || typeString.Contains("Ranged"))
            return DamageTypes.Ranged;
        else if(typeString.Contains(DamageTypes.Fire.ToString()) || typeString.Contains("Fire"))
            return DamageTypes.Fire;
        else if(typeString.Contains(DamageTypes.Cold.ToString()) || typeString.Contains("Cold"))
            return DamageTypes.Cold;
        else if(typeString.Contains(DamageTypes.Light.ToString()) || typeString.Contains("Light"))
            return DamageTypes.Light;
        else if(typeString.Contains(DamageTypes.Shadow.ToString()) || typeString.Contains("Shadow"))
            return DamageTypes.Shadow;
        else if(typeString.Contains(DamageTypes.Acidic.ToString()) || typeString.Contains("Acidic"))
            return DamageTypes.Acidic;
        else
            return DamageTypes.Electric;
    }

    private List<Attribute> ResolveAttributeFromString(string attributeString)
    {
        if (string.IsNullOrEmpty(attributeString))
        {
            return new List<Attribute>();
        }
        var output = new List<Attribute>();

        for(var i = 0; i < attributeString.Length; i++)
        {
            var attr = attributeString[i];

            switch (attr)
            {
                case 'V':
                    output.Add(Attribute.VIGOR);
                    break;
                case 'U':
                    output.Add(Attribute.UNITY);
                    break;
                case 'W':
                    output.Add(Attribute.WRATH);
                    break;
                case 'D':
                    output.Add(Attribute.DECAY);
                    break;
                case 'P':
                    output.Add(Attribute.PENANCE);
                    break;
                case 'A':
                    output.Add(Attribute.AVARICE);
                    break;
                default:
                    throw new ArgumentException("Argument " + attr + " not in VUWDPA with string '" + attributeString + "'.");
            }
        }

        return output.OrderBy(x => Constants.AttributesOrder.IndexOf(x)).ToList();
    }

    public CardRarity ResolveRarity(string rarityString)
    {
        switch(rarityString)
        {
            case "Super Rare":
                return CardRarity.Orange;
            case "Rare":
                return CardRarity.Blue;
            case "Promo":
                return CardRarity.Purple;
            case "Uncommon":
                return CardRarity.Green;
            default:
                return CardRarity.White;
        }
    }

    public string ShortFormRarity(CardRarity rarity)
    {
        switch (rarity)
        {
            case CardRarity.White:
                return "C";
            case CardRarity.Green:
                return "U";
            case CardRarity.Blue:
                return "R";
            case CardRarity.Purple:
                return "P";
            case CardRarity.Orange:
                return "SR";
            case CardRarity.Violet:
                return "V";
            case CardRarity.Pink:
                return "I";
            case CardRarity.Pearlescent:
                return "X";
            default:
                return "^";
        }
    }

    #endregion

    #region Events
    public void OnStartOfTurn(IBaseEventUser source, object args)
    {
        TemporaryCardChanges.ForEach(x => x.Second -= 1);
        TemporaryCardChanges.RemoveAll(x => x.Second <= 0);
        if(Animatable != null)
        {
            Animatable.OnStartOfTurn();
        }

        UpdateCardState();
    }
    #endregion

    public List<IAction> GetAdditionalCosts()
    {
        return new List<IAction>();
    }

    public Triple<int, IEnumerable<Attribute>, int> GetAetherizedContributions()
    {
        //TODO How to handle special cases
        return new Triple<int, IEnumerable<Attribute>, int>(1, Attributes, 1);
    }

    public static Tuple<int, Dictionary<Attribute, int>> GetTotalAetherizationContributions(IEnumerable<Card> cards)
    {
        var attributeDict = new Dictionary<Attribute, int>();
        var listOfContributions = cards.Select(x => x.GetAetherizedContributions());
        var listOfAttributes = listOfContributions.SelectMany(x => x.Second);

        attributeDict.Add(Attribute.VIGOR, listOfAttributes.Where(x => x == Attribute.VIGOR).Count());
        attributeDict.Add(Attribute.UNITY, listOfAttributes.Where(x => x == Attribute.UNITY).Count());
        attributeDict.Add(Attribute.WRATH, listOfAttributes.Where(x => x == Attribute.WRATH).Count());
        attributeDict.Add(Attribute.DECAY, listOfAttributes.Where(x => x == Attribute.DECAY).Count());
        attributeDict.Add(Attribute.PENANCE, listOfAttributes.Where(x => x == Attribute.PENANCE).Count());
        attributeDict.Add(Attribute.AVARICE, listOfAttributes.Where(x => x == Attribute.AVARICE).Count());

        return new Tuple<int, Dictionary<Attribute, int>>(listOfContributions.Sum(x => x.First), attributeDict);
    }

    public static Dictionary<Attribute, int> GetAttributesOfCard(Card c) { return GetAttributesOfCards(new Card[] { c }); }
    public static Dictionary<Attribute, int> GetAttributesOfCards(IEnumerable<Card> cards)
    {
        var output = new Dictionary<Attribute, int>();
        var listOfAttributes = cards.SelectMany(x => x.Attributes);

        output.Add(Attribute.VIGOR, listOfAttributes.Where(x => x == Attribute.VIGOR).Count());
        output.Add(Attribute.UNITY, listOfAttributes.Where(x => x == Attribute.UNITY).Count());
        output.Add(Attribute.WRATH, listOfAttributes.Where(x => x == Attribute.WRATH).Count());
        output.Add(Attribute.DECAY, listOfAttributes.Where(x => x == Attribute.DECAY).Count());
        output.Add(Attribute.PENANCE, listOfAttributes.Where(x => x == Attribute.PENANCE).Count());
        output.Add(Attribute.AVARICE, listOfAttributes.Where(x => x == Attribute.AVARICE).Count());

        return output;
    }

    public bool IsInHand()
    {
        return Animatable != null && Animatable.GetType().IsAssignableFrom(typeof(CardController));
    }

    public bool IsOnField()
    {
        return Animatable != null && Animatable.GetType().IsAssignableFrom(typeof(PawnController));
    }

    public bool IsApartOfTheFilter(TargetSelectionFilter filter)
    {
        return filter.IsTarget(this);
    }

    public bool HasSpecialActivation()
    {
        return false; //Change this later
    }

    public Card Clone()
    {
        var newCard = new Card(
            this.Name,
            this.Set,
            this.CardSetId,
            this.ImageString,
            this.Cost,
            this.BaseText,
            this.Attributes,
            this.Attack,
            this.DamageType,
            this.Defense,
            this.SubType,
            this.Type,
            this.Rarity,
            this.Actions,
            this.CollectorsInfo,
            this.Artist,
            this.CardNumber,
            this.FlavorText,
            this.RelatedCards,
            this.Owner,
            this.Controller
        );

        return newCard;
    }

    public void TransformIntoAnotherCard(string cardName, string setId)
    {
        ChangeCurrentCardToCard(CardDatabase.Instance.GetCard(cardName, setId));
    }
    public void TransformIntoAnotherCard(Card c)
    {
        ChangeCurrentCardToCard(c);
    }

    public void Revert()
    {
        ChangeCurrentCardToCard(CardDatabase.Instance.GetCard(BaseCardNameAndSet.First, BaseCardNameAndSet.Second));
    }

    public void ChangeCurrentCardToCard(Card card)
    {
        Name = card.Name;
        Set = card.Set;
        CardSetId = card.CardSetId;
        ImageString = card.ImageString;
        Cost = card.Cost;
        BaseText = card.BaseText;
        Attributes = card.Attributes;
        Attack = card.Attack;
        DamageType = card.DamageType;
        Defense = card.Defense;
        SubType = card.SubType;
        Type = card.Type;
        Rarity = card.Rarity;
        CollectorsInfo = card.CollectorsInfo;
        Artist = card.Artist;
        CardNumber = card.CardNumber;
        FlavorText = card.FlavorText;
        Traits = card.Traits;
        _actions = card._actions;

        UpdateCardState();
    }

    public void AddChangesToCard(CardChangeData data, int numberOfTurns = -1)
    {
        if(numberOfTurns > 0)
        {
            TemporaryCardChanges.Add(new Tuple<CardChangeData, int>(data, numberOfTurns));
        }
        else
        {
            Cost += data.Cost;
            Attack += data.Attack;
            Defense += data.Defense;
            SubType += " " + data.SubType;

            if(data.Actions != null && Actions != null)
                Actions.AddRange(data.Actions);
        }

        UpdateCardState();
    }

    public void AssignOwner(BasePlayerController owner)
    {
        Owner = owner;
        Controller = owner;
        UpdateCardState();
    }

    public void AssignController(BasePlayerController controller) {
        Controller = controller;
        UpdateCardState();
    }

    public void AssignAnimatable(ICardObject anim)
    {
        Animatable = anim;
        UpdateCardState();
    }

    public void RemoveAnimatable()
    {
        Animatable = null;
        UpdateCardState();
    }

    public void UpdateCardState()
    {
        for(var i = 0; i < Actions.Count; i++)
        {
            Actions[i].AssignCard(this);
        }

        if(Animatable != null)
        {
            Animatable.UpdateCardState(this);
        }
    }

    public void SetupActionsAndRelated()
    {
        var cardName = this.Name.Replace(" ", "").Replace(",", "").Replace("'", "");
        Type type = System.Type.GetType(string.Format(Constants.ImplementedClassAction, cardName));

        if (type == null)
        {
            _actions = new List<IAction>();
            return;
        }

        ICardActionImplementation implementActionsClass = Activator.CreateInstance(type) as ICardActionImplementation;

        _actions = implementActionsClass.Construct(this);
    }
}

public interface ICard
{
    Card GetCard();
}