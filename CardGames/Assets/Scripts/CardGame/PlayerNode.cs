﻿using System.Collections.Generic;

public abstract class BasicPlayerNode
{
    public BasePlayerController Player { get; set; }
    public List<BasePlayerController> TeamMembers { get; set; }

    public BasicPlayerNode(BasePlayerController player, List<BasePlayerController> teamMembers)
    {
        this.Player = player;
        this.TeamMembers = teamMembers;
    }
}

public class PlayerNode : BasicPlayerNode
{
    public PlayerNode(BasePlayerController player) : base(player, new List<BasePlayerController>())
    {
        
    }
}