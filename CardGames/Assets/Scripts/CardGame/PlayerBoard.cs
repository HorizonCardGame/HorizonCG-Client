﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerBoard : MonoBehaviour {

    public float DistanceBetweenRadi = 1;
    public float MinimumSeparator = 20;
    public float TotalArch = 200;

	private Vector3 BoardCenter {  get { return transform.position; } }
    private Vector3 PlayerPosition { get; set; }

    private bool DebugThis = true;

    public List<BoardRing> BoardRings { get; set; }

    public void Constructor(Vector3 center, Quaternion rotation)
    {
        transform.position = center;

        BoardRings = new List<BoardRing>();
        BoardRings.Add(new BoardRing(center, DistanceBetweenRadi, TotalArch, MinimumSeparator));
    }

    private void FixedUpdate()
    {
        if(BoardRings != null && DebugThis)
        {
            foreach(var ring in BoardRings)
            {
                foreach(var node in ring.Nodes)
                {
                    Debug.DrawLine(node.Position, new Vector3(node.Position.x, node.Position.y + 5, node.Position.z), Color.red);
                }
            }
            Debug.DrawLine(transform.position, new Vector3(transform.position.x, transform.position.y + 5, transform.position.z), Color.green);
            Debug.DrawLine(transform.position, transform.forward, Color.cyan);
        }
    }

    public BoardRing AddNewRing()
    {
        BoardRings.Add(new BoardRing(GetBoardCenter(), (BoardRings.Count + 1) * DistanceBetweenRadi));

        return BoardRings.LastOrDefault();
    }

    public void RemoveRing(int index)
    {
        BoardRings.RemoveAt(index);
    }

    public void Add(IPlacable placable)
    {
        placable.GetGameObject().transform.parent = this.transform;
        for (var i = 0; i < BoardRings.Count; i++)
        {
            if (!BoardRings[i].IsFull())
            {
                BoardRings[i].Add(placable);
                return;
            }
        }

        AddNewRing().Add(placable);
    }

    public void Remove(IPlacable placable)
    {
        for (var i = 0; i < BoardRings.Count; i++)
        {
            if (BoardRings[i].Contains(placable))
            {
                BoardRings[i].Remove(placable);
                return;
            }
        }
    }

    public Vector3 GetBoardCenter()
    {
        return BoardCenter;
    }

    public List<IPlacable> GetAll()
    {
        return BoardRings.SelectMany(x => x.Nodes).Where(y => y.Value != null).Select(t => t.Value).ToList();
    }
}

public class BoardRing
{
    private const float DEFAULT_P_M_DEGREES = 100;
    private const float MINIMUM_SPACING = 20;

    public Vector3 RingCenter { get; set; }
    public float Radius { get; set; }
    public float TotalArch { get; set; }
    public float Spacing { get; set; }
    public List<RingNode> Nodes { get; set; }

    private float MaximumDegreePlusOrMinus { get; set; }
    private int MaximumCount { get; set; }

    public BoardRing(Vector3 ringCenter, float ringDistance)
    {
        RingCenter = ringCenter;
        Radius = ringDistance;
        Nodes = new List<RingNode>();
        TotalArch = DEFAULT_P_M_DEGREES * 2;
        MaximumDegreePlusOrMinus = DEFAULT_P_M_DEGREES;
        Spacing = MINIMUM_SPACING;
        ConstructRing();
    }

    public BoardRing(Vector3 ringCenter, float ringDistance, float totalArch, float ms)
    {
        RingCenter = ringCenter;
        Radius = ringDistance;
        Nodes = new List<RingNode>();
        TotalArch = totalArch;
        MaximumDegreePlusOrMinus = totalArch / 2;
        Spacing = ms;
        ConstructRing();
    }

    public void ConstructRing()
    {
        MaximumCount = 0;
        var spacePerSide = Mathf.FloorToInt(MaximumDegreePlusOrMinus / Spacing);
        if (MaximumDegreePlusOrMinus%Spacing == 0)
        {
            Nodes.Add(new RingNode(GetNodePosition(0)));
            MaximumCount++;
        }

        for(var p = 0; p < spacePerSide; p++)
        {
            Nodes.Add(new RingNode(GetNodePosition(Spacing * (p+1))));
            Nodes.Add(new RingNode(GetNodePosition(Spacing * -(p+1))));
            MaximumCount += 2;
        }
    }

    private Vector3 GetNodePosition(float currentRadians)
    {
        //Finding new position for (x,y) for placement where (a,b) equals center (x,y) respectively 
        //and r is radius and T is angle is (x, y) = (a + r cos T, b + r sin T)

        var x = (Radius * Mathf.Cos(Mathf.Deg2Rad * (currentRadians)));
        var z = (Radius * Mathf.Sin(Mathf.Deg2Rad * (currentRadians)));
        var vector = new Vector3(x, 0, z);

        //Debug.Log(string.Format("({0} + {1} cos {2}, {3} + {1} sin {2}) = {4}", RingCenter.x, Radius, currentRadians, RingCenter.z, vector));
        return new Vector3(x, 0, z);
    }

    public void Add(IPlacable obj)
    {
        if(IsFull())
            throw new Exception("Ring full.");

        var i = 0; var firstFound = false;
        while (i < Nodes.Count && !firstFound)
        {
            var node = Nodes[i];

            if (node.Value == null)
            {
                firstFound = true;
                node.SetValue(obj);
            }

            i++;
        }
    }

    public void Remove(IPlacable obj)
    {
        if (!Any())
            throw new Exception("Ring empty.");

        var i = 0; var firstFound = false;
        while(i < Nodes.Count && !firstFound)
        {
            var node = Nodes[i];

            if (node.Value.Equals(obj))
            {
                firstFound = true;
                node.Clear();
            }

            i++;
        }
    }

    public bool Contains(IPlacable obj)
    {
        var i = 0; var firstFound = false;
        while (i < Nodes.Count && !firstFound)
        {
            var node = Nodes[i];

            if (node.Value.Equals(obj))
            {
                firstFound = true;
            }

            i++;
        }

        return firstFound;
    }

    public bool IsFull()
    {
        return MaximumCount <= Nodes.Where(x => x.Value != null).Count();
    }

    public bool Any()
    {
        return Nodes.Where(x => x.Value != null).Any();
    }
}

public class RingNode
{
    public Vector3 Position { get; set; }
    public IPlacable Value { get; set; }

    public RingNode(Vector3 placement)
    {
        Position = placement;
    }

    public void SetValue(IPlacable p)
    {
        Value = p;
        Value.ChangePosition(Position, Quaternion.Euler(0,90,0));
    }

    public void ChangePosition(Vector3 newPlace)
    {
        Position = newPlace;
        Value.ChangePosition(newPlace);
    }

    public void Clear()
    {
        Value = null;
    }

    public void Remove()
    {
        Value.Remove();
    }
}
