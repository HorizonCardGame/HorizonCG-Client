﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TCG.Utility;
using System.Linq;
using System.Text;

public class BasicDeck : IDeck{
    private DeckList _deckList { get; set; }

    public BasePlayerController Owner { get; set; }
    public Card MasterHero { get; set; }
    public List<Card> Cards { get; set; }
    public bool IsRandomized { get; private set; }

    public BasicDeck(string deckList, BasePlayerController owner)
    {
        Owner = owner;
        _deckList = new DeckList(DEFAULT_DECKTEMPLATE, deckList, owner);
        Cards = _deckList.GetList("MAINDECK") as List<Card>;
        MasterHero = (_deckList.GetList("MASTER HERO") as List<Card>)[0];
    }

    public void Shuffle()
    {
        var temp = new List<Card>();

        while(Cards.Count > 0)
        {
            var rn = Randomizer.Instance.Random.Next(Cards.Count - 1);
            temp.Add(Cards[rn]);
            Cards.RemoveAt(rn);
        }
        Cards = temp;

        IsRandomized = true;
    }

    public IEnumerable<Card> DrawCard(int num = 1)
    {
        var output = new List<Card>();

        for(var i = 0; i < num; i++)
        {
            var card = Cards.FirstOrDefault();
            output.Add(card);
            Cards.RemoveAt(0);
        }

        return output;
    }

    public void PutCardsInDeck(IEnumerable<Card> cards, int placement)
    {
        Cards.InsertRange(placement, cards);
    }

    public void PutCardsOnTop(IEnumerable<Card> cards)
    {
        PutCardsInDeck(cards, 0);
    }

    public void PutCardsOnBottom(IEnumerable<Card> cards)
    {
        PutCardsInDeck(cards, Cards.Count - 1);
    }

    public int GetNumberOfCardsInDeck()
    {
        return Cards.Count;
    }

    public static void DebugListOfCards(IEnumerable<Card> cards)
    {
        var sb = new StringBuilder();
        var dict = new Dictionary<Guid, int>();

        foreach(var card in cards)
        {
            if (dict.ContainsKey(card.BaseId))
            {
                var temp = dict[card.BaseId];
                dict[card.BaseId] = temp + 1;
            }
            else
            {
                dict.Add(card.BaseId, 1);
            }
        }

        sb.Append("Count: " + cards.Count() + " | ");
        foreach(var key in dict.Keys)
        {
            sb.AppendFormat("({0} : {1}), ", key, dict[key]);
        }

        Debug.Log(sb.ToString());
    }

    public const string DEFAULT_DECKTEMPLATE = "{ \"CardMaximum\":4," +
        "\"DeckCategories\":[" +
        "{\"Category\":\"MASTER HERO\", \"Minimum\":1, \"Maximum\":1, \"Restrictions\":[\"RequiredType-Hero\", \"NON-REPEAT\"]}," +
        "{\"Category\":\"MAINDECK\", \"Minimum\":60}," +
        "{\"Category\":\"SIDEDECK\", \"Minimum\":0, \"Maximum\":15}," +
        "]}";
}

public class DeckList
{
    private Dictionary<string, IEnumerable<CardSet>> _decklist { get; set; }
    private BasePlayerController Owner { get; set; }

    public DeckList(string deckTemplate, string deckList, BasePlayerController owner)
    {
        _decklist = new Dictionary<string, IEnumerable<CardSet>>();
        Owner = owner;

        var template = JSON.Parse(deckTemplate);
        var deckCategories = template["DeckCategories"].AsArray;
        var dlist = JSON.Parse(deckList);

        foreach(JSONNode node in deckCategories)
        {
            var cat = node["Category"].Value;
            var board = dlist[cat].AsArray;
            if (board != null)
            {
                _decklist.Add(cat, ChangeCardStringsToCardSets(board));
            }
        }
    }

    public IEnumerable<CardSet> ChangeCardStringsToCardSets(JSONArray cards)
    {
        var output = new List<CardSet>();

        foreach (JSONNode card in cards)
        {
            var cardObject = CardDatabase.Instance.GetCard(card["Name"].Value, card["Set"].Value);
            cardObject.AssignOwner(Owner);
            output.Add(new CardSet(cardObject, card["Number"].AsInt));
        }

        return output;
    }

    public IEnumerable<Card> ExpandCardSetList(IEnumerable<CardSet> cardSets)
    {
        var output = new List<Card>();

        foreach(var set in cardSets){
            for (var i = 0; i < set.Number; i++)
            {
                var card = set.Card.Clone();
                output.Add(card);
            }
        }

        return output;
    }

    public IEnumerable<Card> GetList(string key)
    {
        if (_decklist.ContainsKey(key))
        {
            return ExpandCardSetList(_decklist[key]);
        }
        else
        {
            throw new ArgumentOutOfRangeException("No category " + key + " exists in template by that name");
        }
    }
}

public class CardSet {
    public Card Card { get; set; }
    public int Number { get; set; }

    public CardSet(Card c, int n)
    {
        this.Card = c;
        this.Number = n;
    }
}
