﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using UnityEngine.EventSystems;
using TCG.Enums;

public interface IAction
{
    void Setup(Card c);
    void AddToEffect(EffectNode effectNode);
    void AssignCard(Card card);
    bool HasOptionMenuItem(CardZones zone);
    RightClickItem GetRightClickItem();
    List<Card> GetRelatedCards();
    string ToString();
}

public abstract class BaseAction : IAction
{
    public ICard Card { get; set; }
    public Dictionary<string, object> StoredVariables { get; set; }
    protected EffectNode Head { get; set; }
    protected EffectNode Tail { get; set; }
    public List<Card> RelatedCards { get; protected set; }

    public BaseAction(ICard c) { Card = c; StoredVariables = new Dictionary<string, object>(); RelatedCards = new List<Card>(); }

    public void AddToEffect(EffectNode effectNode)
    {
        effectNode.AddParent(this);
        if(Head == null)
        {
            Head = effectNode;
        }
        else
        {
            Tail.Next = effectNode;
            effectNode.Previous = Tail;
        }
        Tail = effectNode;
    }

    public virtual void Setup(Card c)
    {
        Card = c;
    }

    public virtual void AssignCard(Card card)
    {
        this.Card = card;

        var current = Head;
        while (current != null)
        {
            current.Reset(this, card);
            current = current.Next;
        }
    }

    public virtual bool HasOptionMenuItem(CardZones zones)
    {
        return false;
    }

    public virtual RightClickItem GetRightClickItem()
    {
        throw new NotImplementedException();
    }

    public override string ToString()
    {
        var output = new StringBuilder();
        List<INode> list = EffectNodes;
        list.Sort((INode n1, INode n2) => {
            if (n1.GetSortOrder() > n2.GetSortOrder())
            {
                return 1;
            }
            else if (n1.GetSortOrder() < n2.GetSortOrder())
            {
                return -1;
            }
            else
            {
                return 0;
            }
        });

        foreach (var current in list)
        {
            if(current.GetTextProperties().Missing(CardTextProperties.None))
            {
                var str = current.ToString();
                if (current.GetTextProperties().Has(CardTextProperties.PeriodAtEnd))
                    str += ".";
                else if (current.GetTextProperties().Has(CardTextProperties.CommaAtEnd))
                    str += ",";

                output.Append(str + " ");
            }
        }

        var outputStr = (output.ToString().Length != 0) ? output.ToString().Substring(0, output.ToString().Length - 1) : "";
        return output.ToString().Substring(0, output.ToString().Length - 1);
    }

    public virtual List<INode> AllNodes
    {
        get
        {
            return EffectNodes;
        }
    }

    public List<INode> EffectNodes
    {
        get
        {
            var output = new List<INode>();
            var current = Head;

            while (current != null)
            {
                output.Add(current);

                current = current.Next;
            }

            return output;
        }
    }

    public void AddToRelatedCards(Card card)
    {
        RelatedCards.Add(card);
    }

    public List<Card> GetRelatedCards()
    {
        return RelatedCards;
    }

    #region TriggeringActions
    private int _currentIndex = 0;
    private List<INode> _nodeList;
    public virtual void TriggerAction()
    {
        _currentIndex = 0;
        _nodeList = AllNodes;
        ContinueBeforeTriggeringActions();
    }

    public void ContinueBeforeTriggeringActions()
    {
        if (_currentIndex > _nodeList.Count - 1)
        {
            FinishTriggeringActions();
        }
        else
        {
            _nodeList[_currentIndex++].BeforeTriggeringAction(ContinueBeforeTriggeringActions);
        }
    }

    public void FinishTriggeringActions()
    {
        var data = new ActionData { Action = this };
        if(Card.GetCard().Animatable != null)
        {
            var pos = Card.GetCard().Animatable.GetTransform().position;
            data.Origin = GameMaster.Instance.MainCamera.WorldToScreenPoint(pos);
        }
            
        GameMaster.Instance.AddToTheStack(data);
    }

    public void Invoke()
    {
        Head.Invoke();
    }

    public void CompleteActions()
    {
        _currentIndex = 0;
        ContinueAfterTriggeringActions();
    }

    public void ContinueAfterTriggeringActions()
    {
        if (_currentIndex > _nodeList.Count - 1)
        {
            _currentIndex = 0;
            _nodeList.Clear();
        }
        else
        {
            _nodeList[_currentIndex++].AfterTriggeringAction(ContinueAfterTriggeringActions);
        }
    }
    #endregion
}

public class AbilityAction : BaseAction
{
    public AbilityAction(ICard c) : base(c) { }
}

public class StaticAction : BaseAction
{
    public List<ConditionNode> ConditionNodes { get; set; }

    public StaticAction(ICard c) : base(c)
    {
        ConditionNodes = new List<ConditionNode>();
    }

    public bool AreAllRequirementsMet()
    {
        return ConditionNodes.Where(n => n.GetType() == typeof(IRequirementNode))
            .Where(m => !((IRequirementNode)m).IsRequirementMet()).Count() == 0;
    }
}

public class ConditionalAction : BaseAction
{
    public List<ConditionNode> ConditionNodes { get; set; }
    public CardZones ActivationZones { get; set; }

    public ConditionalAction(ICard c) : base(c)
    {
        ConditionNodes = new List<ConditionNode>();
    }

    public override void Setup(Card c)
    {
        ConditionNodes.ForEach(n => n.Setup(c));
    }

    public void AddToCondition(ConditionNode node)
    {
        node.AddParent(this);
        ConditionNodes.Add(node);
    }

    public override void AssignCard(Card card)
    {
        base.AssignCard(card);
        for(var i = 0; i < ConditionNodes.Count; i++)
        {
            var node = ConditionNodes[i];
            node.Reset(this as ConditionalAction, card);
            ConditionNodes[i] = node;
        }
    }

    public bool AreAllRequirementsMet()
    {
        return ConditionNodes.Where(n => n is IRequirementNode)
            .Where(m => !((IRequirementNode)m).IsRequirementMet()).Count() == 0;
    }

    public override void TriggerAction()
    {
        if (AreAllRequirementsMet())
        {
            base.TriggerAction();
        }
    }
    public void TriggerAction(PointerEventData data)
    {
        TriggerAction();
    }
    public override List<INode> AllNodes
    {
        get
        {
            var output = ConditionNodes.Select(x => (INode)x).ToList();
            output.AddRange(base.AllNodes);

            return output;
        }
    }

    public override bool HasOptionMenuItem(CardZones zone)
    {
        return zone == ActivationZones && ConditionNodes.Where(x => x.GetType().IsSubclassOf(typeof(ActivatedCondition))).Any();
    }

    public override RightClickItem GetRightClickItem()
    {
        return new RightClickItem
        {
            ClickAction = TriggerAction,
            Text = ToString(),
            Enabled = AreAllRequirementsMet()
        };
    }

    public override string ToString()
    {
        var output = new StringBuilder();
        foreach(var node in ConditionNodes)
        {
            output.Append(node + ", ");
        }

        var outputStr = (output.ToString().Length != 0) ? output.ToString().Substring(0, output.ToString().Length - 2) : "";
        return outputStr + " : " + base.ToString();
    }
}

public interface IRequirementNode
{
    bool IsRequirementMet();
}