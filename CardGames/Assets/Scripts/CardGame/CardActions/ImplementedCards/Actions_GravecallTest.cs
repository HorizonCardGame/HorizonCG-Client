﻿using UnityEngine;
using System.Collections;
using TCG.Utility;
using System.Collections.Generic;
using TCG.Enums;

namespace CardImplementations
{
    public class Actions_GravecallTest : ICardActionImplementation
    {
        public List<IAction> Construct(Card card)
        {
            var actions = new List<IAction>();

            var arriveAction = new ConditionalAction(card);
            arriveAction.AddToCondition(new SacrificeActivatedCondition(CardZones.Field));
            arriveAction.AddToEffect(new TargetingEffect("target",
                new TargetSelectionFilter(
                    "Target unit",
                    "Select a Unit",
                    1,
                    Vector3.zero,
                    Filters.UnitsOnTheField
                ),
                CardTextProperties.NoModification));
            arriveAction.AddToEffect(new GeneralActivateAnimation());
            arriveAction.AddToEffect(new CardChangeEffect("target", new CardChangeData { Attack = 1 }, 1, CardTextProperties.PeriodAtEnd | CardTextProperties.MiddleOf));
            actions.Add(arriveAction);

            return actions;
        }
    }
}
