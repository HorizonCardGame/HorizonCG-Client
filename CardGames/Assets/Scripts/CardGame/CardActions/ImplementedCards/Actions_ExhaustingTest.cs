﻿using UnityEngine;
using System.Collections;
using TCG.Utility;
using System.Collections.Generic;
using TCG.Enums;

namespace CardImplementations
{
    public class Actions_ExhaustingTest : ICardActionImplementation
    {
        public List<IAction> Construct(Card card)
        {
            var actions = new List<IAction>();

            var arriveAction = new ConditionalAction(card);
            arriveAction.AddToCondition(new ExhaustActivatedCondition(CardZones.Field));
            arriveAction.AddToEffect(new TargetingEffect("target", 
                new TargetSelectionFilter (
                    "Target unit",
                    "Select a Unit to Transform",
                    1,
                    Vector3.zero,
                    Filters.UnitsOnTheField
                ), 
                CardTextProperties.NoModification));
            arriveAction.AddToEffect(new GeneralActivateAnimation());
            arriveAction.AddToEffect(new TransformEffect("Paying Test", "TST", "target", CardTextProperties.PeriodAtEnd));
            actions.Add(arriveAction);

            return actions;
        }
    }
}
