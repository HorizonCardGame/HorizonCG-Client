﻿using System.Collections.Generic;
using TCG.Utility;

public interface ICardActionImplementation
{
    List<IAction> Construct(Card card);
}