﻿using UnityEngine;
using System.Collections;
using TCG.Utility;
using System.Collections.Generic;
using TCG.Enums;

namespace CardImplementations
{
    public class Actions_PayingTest : ICardActionImplementation
    {
        public List<IAction> Construct(Card card)
        {
            var actions = new List<IAction>();

            var arriveAction = new ConditionalAction(card);

            arriveAction.AddToCondition(new ArriveCondition());
            arriveAction.AddToEffect(new GeneralActivateAnimation(Vector3.zero));
            arriveAction.AddToEffect(new CreateTokenEffect("Gravecall Test", "TST", 2, CardTextProperties.PeriodAtEnd));

            actions.Add(arriveAction);

            return actions;
        }
    }
}
