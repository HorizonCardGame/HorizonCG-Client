﻿using UnityEngine;
using System.Collections;
using TCG.Utility;
using System.Collections.Generic;

namespace CardImplementations
{
    public class Actions_DepartTest : ICardActionImplementation
    {
        public List<IAction> Construct(Card card)
        {
            var actions = new List<IAction>();

            var arriveAction = new ConditionalAction(card);
            arriveAction.AddToCondition(new ClosureCondition());
            arriveAction.AddToEffect(new GeneralActivateAnimation());
            arriveAction.AddToEffect(new HealthGainEffect(1, CardTextProperties.PeriodAtEnd));
            actions.Add(arriveAction);

            return actions;
        }
    }
}
