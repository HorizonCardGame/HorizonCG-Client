﻿using UnityEngine;
using System.Collections;
using TCG.Utility;
using System.Collections.Generic;
using TCG.Enums;

namespace CardImplementations
{
    public class Actions_ArriveTest : ICardActionImplementation
    {
        public List<IAction> Construct(Card card)
        {
            var actions = new List<IAction>();

            var arriveAction = new ConditionalAction(card);
            arriveAction.AddToCondition(new ExhaustActivatedCondition(CardZones.Field));
            arriveAction.AddToEffect(new TargetingEffect("target",
                new TargetSelectionFilter(
                    "Target player",
                    "Select a Unit to Transform",
                    1,
                    Vector3.zero,
                    Filters.Players
                ),
                CardTextProperties.NoModification));
            arriveAction.AddToEffect(new GeneralActivateAnimation());
            arriveAction.AddToEffect(new CreateTokenEffect("Paying Test", "TST", 1, CardTextProperties.PeriodAtEnd, "target"));
            actions.Add(arriveAction);

            return actions;
        }
    }
}
