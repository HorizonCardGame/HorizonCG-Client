﻿using System;
using System.Collections.Generic;
using System.Linq;
using TCG.Enums;

public class DrawEffect : EffectNode
{
    public int Amount { get; protected set; }

    public DrawEffect(int a, CardTextProperties tp, string variable = null) : base(tp, variable)
    {
        Amount = a;
    }

    public override void Invoke()
    {
        if (!string.IsNullOrEmpty(_variable))
        {
            List<IBaseEventUser> targets = Parent.StoredVariables[_variable] as List<IBaseEventUser>;
            if (targets.Any())
            {
                for (int i = 0; i < targets.Count; ++i)
                {
                    if (targets[i].GetType().IsSubclassOf(typeof(BasePlayerController)))
                    {
                        ((BasePlayerController)targets[i]).DrawCards(Amount);
                    }
                }
            }
        }
        else
        {
            Card.Controller.DrawCards(Amount);
        }

        base.Invoke();
    }

    public override string ToString()
    {
        return Amount > 1 ? string.Format("Draw {0} cards", Amount) : "Draw a card";
    }
}

public static class DamageTypeExtensions
{
    public static string ToTextIcon(this DamageTypes t)
    {
        switch (t)
        {
            case DamageTypes.Acidic:
                return "<dA/>";
            case DamageTypes.Cold:
                return "<dC/>";
            case DamageTypes.Electric:
                return "<dE/>";
            case DamageTypes.Fire:
                return "<dF/>";
            case DamageTypes.Light:
                return "<dL/>";
            case DamageTypes.Melee:
                return "<dM/>";
            case DamageTypes.Ranged:
                return "<dR/>";
            case DamageTypes.Shadow:
                return "<dS/>";
            default:
                throw new IndexOutOfRangeException();
        }
    }
}
