﻿using System;
using System.Collections.Generic;
using System.Linq;

public class RevertEffect : EffectNode
{
    public RevertEffect(string variable, CardTextProperties tp) : base(tp, variable)
    {
        
    }

    public override void Setup(Card c)
    {
        base.Setup(c);
    }

    public override void Invoke()
    {
        if (!string.IsNullOrEmpty(_variable))
        {
            List<IBaseEventUser> targets = Parent.StoredVariables[_variable] as List<IBaseEventUser>;
            if (targets.Any())
            {
                for (int i = 0; i < targets.Count; ++i)
                {
                    if (targets[i].GetType().IsSubclassOf(typeof(Card)))
                    {
                        ((Card)targets[i]).Revert();
                    }
                    else if (targets[i] is ICardObject)
                    {
                        ((ICardObject)targets[i]).GetCard().Revert();
                    }
                }
            }
        }
        else
        {
            throw new ArgumentException();
        }

        base.Invoke();
    }

    public override string ToString()
    {
        return string.Format("Revert");
    }
}