﻿using UnityEngine;
using UnityEngine.Events;

public abstract class EffectNode : INode
{
    protected string _variable { get; set; }
    protected CardTextProperties _textProperties { get; set; }
    public EffectNode Previous { get; set; }
    public EffectNode Next { get; set; }
    public BaseAction Parent { get; set; }
    public Card Card { get; set; }

    public EffectNode(CardTextProperties tp = CardTextProperties.PeriodAtEnd, string variable = null)
    {
        if (!string.IsNullOrEmpty(variable))
        {
            _variable = variable;
        }
        _textProperties = tp;
    }

    public virtual void Invoke()
    {
        if(Next != null)
        {
            Next.Invoke();
        }
        else
        {
            Parent.CompleteActions();
        }
    }

    public virtual void Setup(Card c)
    {
        Card = c;
    }

    public virtual void Reset(BaseAction parent, Card c)
    {
        Parent = parent;
        Card = c;
    }

    public virtual void AddParent(BaseAction parent)
    {
        Parent = parent;
    }

    public virtual void BeforeTriggeringAction(UnityAction callback)
    {
        callback();
    }

    public virtual void AfterTriggeringAction(UnityAction callback)
    {
        callback();
    }

    public virtual CardTextProperties GetTextProperties()
    {
        return _textProperties;
    }

    public virtual int GetSortOrder()
    {
        return 0;
    }
}
