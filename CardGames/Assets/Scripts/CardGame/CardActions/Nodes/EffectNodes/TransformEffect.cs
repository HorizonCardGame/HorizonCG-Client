﻿using System;
using System.Collections.Generic;
using System.Linq;

public class TransformEffect : EffectNode
{
    public Card _cardToTransformTo;

    public TransformEffect(string transformTarget, string cardSetId, string variable, CardTextProperties tp) : base(tp, variable)
    {
        _cardToTransformTo = CardDatabase.Instance.GetCard(transformTarget, cardSetId);
    }

    public override void Setup(Card c)
    {
        Parent.AddToRelatedCards(_cardToTransformTo);
        base.Setup(c);
    }

    public override void Invoke()
    {
        if (!string.IsNullOrEmpty(_variable))
        {
            List<IBaseEventUser> targets = Parent.StoredVariables[_variable] as List<IBaseEventUser>;
            if (targets.Any())
            {
                for (int i = 0; i < targets.Count; ++i)
                {
                    if (targets[i].GetType().IsSubclassOf(typeof(Card)))
                    {
                        ((Card)targets[i]).TransformIntoAnotherCard(_cardToTransformTo);
                    }
                    else if(targets[i] is ICardObject)
                    {
                        ((ICardObject)targets[i]).GetCard().TransformIntoAnotherCard(_cardToTransformTo);
                    }
                }
            }
        }
        else
        {
            throw new ArgumentException();
        }

        base.Invoke();
    }

    public override string ToString()
    {
        return string.Format("Transform into <b>{0}</b>", _cardToTransformTo.Name);
    }
}
