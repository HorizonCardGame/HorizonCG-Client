﻿using System.Collections.Generic;
using System.Text;
using UnityEngine.Events;

public class TargetingEffect : EffectNode
{
    private TargetSelectionFilter _filter;
    private UnityAction _callback;

    public TargetingEffect(string variable, TargetSelectionFilter targets, CardTextProperties tp) : base(tp, variable)
    {
        _filter = targets;
    }

    public override void BeforeTriggeringAction(UnityAction callback)
    {
        _callback = callback;
        TargettingSystemController.Instance.BeginTargeting(_filter, false, Confirm, Cancel);
    }

    public override void AfterTriggeringAction(UnityAction callback)
    {
        Parent.StoredVariables.Remove(_variable);
        base.AfterTriggeringAction(callback);
    }

    public void Confirm(List<IBaseEventUser> targets, List<object> args)
    {
        Parent.StoredVariables.Add(_variable, targets);
        if (_callback != null)
        {
            _callback();
        }
    }

    public void Cancel() {  }

    public override string ToString()
    {
        if (_textProperties.Has(CardTextProperties.MiddleOf))
        {
            var output = _filter.ToString();
            var firstLetter = output[0];
            return firstLetter.ToString().ToLower() + output.Substring(1, output.Length - 1);
        }
        else
        {
            return _filter.ToString();
        }
    }

    public override int GetSortOrder()
    {
        return -1;
    }
}