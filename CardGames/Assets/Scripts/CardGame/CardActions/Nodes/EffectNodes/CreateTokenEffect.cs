﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CreateTokenEffect : EffectNode
{
    public int Amount;
    public Card _cardToCreate;

    public CreateTokenEffect(string tokenTarget, string cardSetId, int amount, CardTextProperties tp, string variable = null) : base(tp, variable)
    {
        Amount = amount;
        _cardToCreate = CardDatabase.Instance.GetCard(tokenTarget, cardSetId);
    }

    public override void Setup(Card c)
    {
        Parent.AddToRelatedCards(_cardToCreate);
        base.Setup(c);
    }

    public override void Invoke()
    {
        if (!string.IsNullOrEmpty(_variable))
        {
            List<IBaseEventUser> targets = Parent.StoredVariables[_variable] as List<IBaseEventUser>;
            if (targets.Any())
            {
                for (int i = 0; i < targets.Count; ++i)
                {
                    if (targets[i] is BasePlayerController)
                    {
                        CreateTokenForPlayer((BasePlayerController)targets[i], _cardToCreate, Amount);
                    }
                }
            }
        }
        else
        {
            CreateTokenForPlayer(Card.Controller, _cardToCreate, Amount);
        }

        base.Invoke();
    }

    private void CreateTokenForPlayer(BasePlayerController player, Card token, int amount)
    {
        for(var i = 0; i < amount;  ++i)
        {
            PawnController pawn = GameMaster.Instance.InstantiatePawn();
            var card = token.Clone();
            card.AssignOwner(player);
            pawn.Constructor(card, player);

            GameMaster.Instance.CardsInPlay.Add(new CardNode(card, pawn, player));
            player.AddCardToBoard(pawn);
        }
    }

    public override string ToString()
    {
        return string.Format("{0} {1} <b>{2}</b>", 
                                _textProperties.Has(CardTextProperties.MiddleOf) ? "Creates" : "Create",
                                    Amount > 1 ? Amount.ToString() : "a", 
                                    _cardToCreate.Name);
    }
}
