﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TCG.Enums;

public class CardChangeEffect : EffectNode
{
    public CardChangeData _data;
    public int _numberOfTurns;

    public CardChangeEffect(string variable, CardChangeData data, int numberOfTurns = -1, CardTextProperties tp = CardTextProperties.PeriodAtEnd) : base(tp, variable)
    {
        _data = data;
        _numberOfTurns = numberOfTurns;
    }

    public override void Invoke()
    {
        if (!string.IsNullOrEmpty(_variable))
        {
            List<IBaseEventUser> targets = Parent.StoredVariables[_variable] as List<IBaseEventUser>;
            if (targets.Any())
            {
                for (int i = 0; i < targets.Count; ++i)
                {
                    if (targets[i] is ICardObject)
                    {
                        ((ICardObject)targets[i]).GetCard().AddChangesToCard(_data, _numberOfTurns);
                    }
                }
            }
        }
        else
        {
            throw new ArgumentException();
        }

        base.Invoke();
    }

    public override string ToString()
    {
        return _data.ToString() + (_numberOfTurns > 0 ? " until the end of turn" : "");
    }
}

public class CardChangeData
{
    public int Cost { get; set; }
    public string SubType { get; set; }
    public int Attack { get; set; }
    public int Defense { get; set; }
    public List<IAction> Actions = new List<IAction>();

    public override string ToString()
    {
        var output = new List<string>();
        if (Cost > 0)
        {
            output.Add("Costs <" + Cost + "/> more");
        }
        else if(Cost < 0)
        {
            output.Add("Costs <" + -Cost + "/> less");
        }

        if(Attack != 0 && Defense != 0)
        {
            output.Add("Gets " + (Attack > 0 ? "+" : "") + Attack + "<Atk/>/" + (Defense > 0 ? "+" : "") + Defense + "<Def/>");
        }
        else if(Attack != 0)
        {
            output.Add("Gets " + (Attack > 0 ? "+" : "") + Attack + "<Atk/>");
        }
        else if(Defense != 0)
        {
            output.Add("Gets " + (Defense > 0 ? "+" : "") + Defense + "<Def/>");
        }

        if (!string.IsNullOrEmpty(SubType))
        {
            output.Add("Is " + SubType + " in addition to its other types");
        }

        if (Actions != null && Actions.Any())
        {
            output.AddRange(Actions.Select(x => string.Format("Gains \"{0}\"", x.ToString())));
        }

        var outputStr = new StringBuilder();

        for(var i = 0; i < output.Count; ++i)
        {
            outputStr.Append(i != 0 ? output[i][0].ToString().ToLower() + output[i].Substring(1) : output[i]);

            if(i == output.Count - 1 && output.Count > 1)
            {
                outputStr.Append(" and ");
            }
            else if(i < output.Count - 1)
            {
                outputStr.Append(", ");
            }
        }

        return outputStr.ToString();
    }
}