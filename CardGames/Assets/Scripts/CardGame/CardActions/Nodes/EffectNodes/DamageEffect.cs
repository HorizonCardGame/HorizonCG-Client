﻿using System.Collections.Generic;
using System.Linq;
using TCG.Enums;
using UnityEngine;

public class DamageEffect : EffectNode
{
    public int Amount { get; protected set; }
    public DamageTypes DamageType { get; set; }

    public DamageEffect(int a, DamageTypes dt, CardTextProperties tp, string variable = null) : base(tp, variable)
    {
        Amount = a;
        DamageType = dt;
    }

    public override void Invoke()
    {
        if (!string.IsNullOrEmpty(_variable) && Parent.StoredVariables.ContainsKey(_variable))
        {
            List<IBaseEventUser> targets = Parent.StoredVariables[_variable] as List<IBaseEventUser>;
            if (targets.Any())
            {
                for (int i = 0; i < targets.Count; ++i)
                {
                    if (targets[i] is IDamageable)
                    {
                        ((IDamageable)targets[i]).DealtDamage(Card, Amount, DamageType);
                    }
                }
            }
        }
        else
        {
            Card.Controller.DealtDamage(Card, Amount, DamageType);
        }

        base.Invoke();
    }

    public override string ToString()
    {
        return string.Format("Deal {0}{1} damage{2}", Amount, DamageType.ToTextIcon(), _textProperties.Missing(CardTextProperties.PeriodAtEnd) ? " to" : "");
    }

    public override int GetSortOrder()
    {
        return -5;
    }
}
