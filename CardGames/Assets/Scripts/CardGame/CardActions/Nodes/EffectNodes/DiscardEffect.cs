﻿using System.Collections.Generic;
using System.Linq;

public class DiscardEffect : EffectNode
{
    public int Amount { get; protected set; }

    public DiscardEffect(int a, CardTextProperties tp, string variable = null) : base(tp, variable)
    {
        Amount = a;
    }

    public override void Invoke()
    {
        if (!string.IsNullOrEmpty(_variable) && Parent.StoredVariables.ContainsKey(_variable))
        {
            List<IBaseEventUser> targets = Parent.StoredVariables[_variable] as List<IBaseEventUser>;
            if (targets.Any())
            {
                for (int i = 0; i < targets.Count; ++i)
                {
                    if (targets[i] is BasePlayerController)
                    {
                        ((BasePlayerController)targets[i]).DiscardCardsFromHand(Amount);
                    }
                }
            }
        }
        else
        {
            Card.Controller.DiscardCardsFromHand(Amount);
        }

        base.Invoke();
    }

    public override string ToString()
    {
        return string.Format("Discards {0}", Amount > 1 ? Amount + " cards" : "a card");
    }
}