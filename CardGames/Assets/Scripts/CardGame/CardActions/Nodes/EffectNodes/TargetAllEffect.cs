﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;

public class TargetAllEffect : EffectNode
{
    private TargetSelectionFilter _filter;
    private UnityAction _callback;

    public TargetAllEffect(string variable, TargetSelectionFilter targets, CardTextProperties tp) : base(tp, variable)
    {
        _filter = targets;
        _filter.NumberOfTargets = -1;
    }

    public override void BeforeTriggeringAction(UnityAction callback)
    {
        var targets = new List<IBaseEventUser>();

        var allPlayers = GameMaster.Instance.GetAllPlayers();
        targets.AddRange(allPlayers.Where(p => _filter.IsTarget(p)).Select(x => (IBaseEventUser)x));
        targets.AddRange(allPlayers.SelectMany(p => p.GetCardsOnBoard(_filter))
            .Where(x => _filter.IsTarget(x)).Select(y => (IBaseEventUser)y));
        targets.AddRange(allPlayers.SelectMany(p => p.GetCardsInHand(_filter).Select(x => (IBaseEventUser)x)));
        targets.AddRange(allPlayers.SelectMany(p => p.GetCardsInAether(_filter).Select(x => (IBaseEventUser)x)));

        Parent.StoredVariables.Add(_variable, targets);
        base.BeforeTriggeringAction(callback);
    }

    public override void AfterTriggeringAction(UnityAction callback)
    {
        Parent.StoredVariables.Remove(_variable);
        base.AfterTriggeringAction(callback);
    }

    public override string ToString()
    {
        if (_textProperties.Has(CardTextProperties.MiddleOf))
        {
            var output = _filter.ToString();
            var firstLetter = output[0];
            return firstLetter.ToString().ToLower() + output.Substring(1, output.Length - 1);
        }
        else
        {
            return _filter.ToString();
        }
    }
}