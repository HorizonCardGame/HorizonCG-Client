﻿using System.Collections.Generic;
using System.Linq;

public class ArmorGainEffect : EffectNode
{
    public int Amount { get; protected set; }
    private bool _doNotPropagate;

    public ArmorGainEffect(int a, CardTextProperties tp, string variable = null, bool doNotPropagate = false) : base(tp, variable)
    {
        Amount = a;
        _doNotPropagate = doNotPropagate;
    }

    public override void Invoke()
    {
        if (!string.IsNullOrEmpty(_variable))
        {
            List<IBaseEventUser> targets = Parent.StoredVariables[_variable] as List<IBaseEventUser>;
            if (targets.Any())
            {
                for (int i = 0; i < targets.Count; ++i)
                {
                    if (targets[i].GetType().IsSubclassOf(typeof(BasePlayerController)))
                    {
                        ((BasePlayerController)targets[i]).GainArmor(Card, Amount, !_doNotPropagate);
                    }
                }
            }
        }
        else
        {
            var cd = Card.GetCard();
            var controller = cd.Controller;
            controller.GainArmor(Card, Amount, !_doNotPropagate);
        }

        base.Invoke();
    }

    public override string ToString()
    {
        return string.Format("Gain {0} Armor.{1}", Amount, _doNotPropagate ? " <i>(This does not trigger actions.)</i>" : "");
    }
}