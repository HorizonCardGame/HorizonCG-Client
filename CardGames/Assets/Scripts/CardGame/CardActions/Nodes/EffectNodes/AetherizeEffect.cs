﻿using System;
using System.Collections.Generic;
using System.Linq;

public class AetherizeEffect : EffectNode
{
    public AetherizeEffect(string variable, CardTextProperties tp) : base(tp, variable)
    {
    }

    public override void Invoke()
    {
        if (!string.IsNullOrEmpty(_variable))
        {
            List<IBaseEventUser> targets = Parent.StoredVariables[_variable] as List<IBaseEventUser>;
            if (targets.Any())
            {
                for (int i = 0; i < targets.Count; ++i)
                {
                    if (targets[i] is ICardObject)
                    {
                        ((ICardObject)targets[i]).Aetherize();
                    }
                }
            }
        }
        else
        {
            throw new ArgumentException();
        }

        base.Invoke();
    }

    public override string ToString()
    {
        return "Aetherize";
    }

    public override int GetSortOrder()
    {
        return -3;
    }
}