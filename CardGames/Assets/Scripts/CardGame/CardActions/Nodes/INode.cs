﻿using UnityEngine.Events;

public interface INode
{
    int GetSortOrder();
    void AddParent(BaseAction Parent);
    void Setup(Card c);
    void BeforeTriggeringAction(UnityAction callback);
    void AfterTriggeringAction(UnityAction callback);
    void Reset(BaseAction Parent, Card c);
    CardTextProperties GetTextProperties();
}
