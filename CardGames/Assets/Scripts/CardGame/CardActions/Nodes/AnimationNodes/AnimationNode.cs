﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class AnimationNode : EffectNode
{
    public AnimationNode(string variable = null) : base(CardTextProperties.None, variable)
    {

    }

    public void MoveToNext()
    {
        base.Invoke();
    }
}

public class GeneralActivateAnimation : AnimationNode
{
    public Vector3 _setPosition = -100 * Vector3.one;

    public GeneralActivateAnimation(string variable = null) : base(variable)
    {

    }

    public GeneralActivateAnimation(Vector3 setPosition)
    {
        _setPosition = setPosition;
    }

    public override void Invoke()
    {
        GameMaster.Instance.RunCoroutine(RunAnimation(MoveToNext));
    }

    public IEnumerator RunAnimation(UnityAction callback)
    {
        //TODO: Fix Material Changing with animations
        var position = (!_setPosition.Equals(-100 * Vector3.one)) ? _setPosition : (!string.IsNullOrEmpty(_variable)) ? 
                        (Vector3)Parent.StoredVariables[_variable] : 
                        Card.Animatable != null ? Card.Animatable.GetTransform().position : Vector3.zero;
        //var material = Resources.Load<Material>("Materials/Glow");
        GameMaster.Instance.Instantiate("Particles/GeneralActivateParticles", position, 1f);
        //Card.Animatable.ChangeMaterial(material);
        yield return new WaitForSeconds(1);
        //Card.Animatable.ResetToDfaultMaterial();
        callback();
    }

    public override string ToString()
    {
        return "";
    }
}