﻿using TCG.Enums;
using UnityEngine.Events;

public class CostActivatedCondition : ActivatedCondition
{
    public int Cost { get; set; }
    public CostActivatedCondition(int cost, CardZones zones, string variable = null) : base(zones, variable)
    {
        Cost = cost;
    }

    public override void BeforeTriggeringAction(UnityAction callback)
    {
        Card.Controller.PayCost(Cost);
        base.BeforeTriggeringAction(callback);
    }

    public override bool IsRequirementMet()
    {
        var foo = Card.Controller.CanPayCost(Cost);
        return foo;
    }

    public override void Setup(Card c)
    {
        base.Setup(c);
        Parent.ActivationZones = ZonesActivated;
    }

    public override string ToString()
    {
        return string.Format("<{0}/>", Cost);
    }
}
