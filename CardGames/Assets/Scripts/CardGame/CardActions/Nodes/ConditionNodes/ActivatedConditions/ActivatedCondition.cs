﻿using TCG.Enums;
using UnityEngine;

public abstract class ActivatedCondition : ConditionNode, IRequirementNode
{
    public ActivatedCondition(CardZones zones, string variable = null) : base(variable) {
        ZonesActivated = zones;
    }

    public CardZones ZonesActivated { get; set; }

    public abstract bool IsRequirementMet();

    public override void Setup(Card c)
    {
        base.Setup(c);
    }

    public override string ToString()
    {
        return "<b>Activation Cost</b>";
    }
}
