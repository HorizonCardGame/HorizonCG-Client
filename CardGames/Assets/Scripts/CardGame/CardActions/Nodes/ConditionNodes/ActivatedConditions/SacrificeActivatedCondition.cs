﻿using TCG.Enums;
using UnityEngine.Events;

public class SacrificeActivatedCondition : ActivatedCondition
{
    public SacrificeActivatedCondition(CardZones zones, string variable = null) : base(zones, variable)
    {
    }

    public override void BeforeTriggeringAction(UnityAction callback)
    {
        ((PawnController)Card.Animatable).Destroy();
        base.BeforeTriggeringAction(callback);
    }

    public override bool IsRequirementMet()
    {
        return Card.Animatable.GetType().IsAssignableFrom(typeof(PawnController));
    }

    public override void Setup(Card c)
    {
        base.Setup(c);
        Parent.ActivationZones = ZonesActivated;
    }

    public override string ToString()
    {
        return "<b>Sacrifice</b>";
    }
}
