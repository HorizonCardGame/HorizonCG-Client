﻿using TCG.Enums;
using UnityEngine.Events;

public class ExhaustActivatedCondition : ActivatedCondition
{
    public ExhaustActivatedCondition(CardZones zones, string variable = null) : base(zones, variable)
    {
    }

    public override void BeforeTriggeringAction(UnityAction callback)
    {
        ((PawnController)Card.Animatable).Exhaust();
        base.BeforeTriggeringAction(callback);
    }

    public override bool IsRequirementMet()
    {
        return Card.Animatable.GetType().IsAssignableFrom(typeof(PawnController)) && ((PawnController)Card.Animatable).CanExhaust();
    }

    public override void Setup(Card c)
    {
        base.Setup(c);
        Parent.ActivationZones = ZonesActivated;
    }

    public override string ToString()
    {
        return "<b>Exhaust</b>";
    }
}