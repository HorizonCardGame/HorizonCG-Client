﻿using UnityEngine.Events;

public abstract class ConditionNode : INode
{
    protected string _variable { get; set; }
    public ConditionalAction Parent { get; set; }
    public Card Card { get; set; }

    public ConditionNode(string variable = null)
    {
        if (!string.IsNullOrEmpty(variable))
        {
            _variable = variable;
        }
    }

    public virtual void Setup(Card c)
    {
        Card = c;
    }

    public virtual void Stop()
    {
        
    }

    public void Reset(BaseAction parent, Card card)
    {
        Reset((ConditionalAction)parent, card);
    }

    public virtual void Reset(ConditionalAction parent, Card card)
    {
        Stop();
        Parent = parent;
        Card = card;
        Setup(card);
    }

    public void AddParent(BaseAction parent)
    {
        AddParent((ConditionalAction)parent);
    }

    public virtual void AddParent(ConditionalAction parent)
    {
        Parent = parent;
    }

    public virtual void BeforeTriggeringAction(UnityAction callback)
    {
        callback();
    }

    public virtual void AfterTriggeringAction(UnityAction callback)
    {
        callback();
    }

    public virtual CardTextProperties GetTextProperties()
    {
        return CardTextProperties.CommaAtEnd;
    }

    public virtual int GetSortOrder()
    {
        return 0;
    }
}
