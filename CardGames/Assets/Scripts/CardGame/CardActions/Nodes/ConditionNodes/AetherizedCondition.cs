﻿public class AetherizedCondition : ConditionNode
{
    public AetherizedCondition() : base() { }

    public override void Setup(Card c)
    {
        base.Setup(c);
        Card.Subscribe(EventNames.Aetherized, OnAetherize);
    }

    public override void Stop()
    {
        if (Card != null)
        {
            Card.Unsubscribe(EventNames.Aetherized);
        }
    }

    public void OnAetherize(IBaseEventUser sender, object args)
    {
        if (sender.GetId() == Card.BaseId)
        {
            Parent.TriggerAction();
        }
    }

    public override string ToString()
    {
        return "<b>Aetherized</b>";
    }
}