﻿using UnityEngine.Events;

public class GravecallCondition : ConditionNode
{
    public GravecallCondition(string variable = null) : base(variable) { }

    public override void Setup(Card c)
    {
        base.Setup(c);
        Card.Subscribe(EventNames.PutInDiscard, OnGravecall);
    }

    public void OnGravecall(IBaseEventUser sender, object args)
    {
        var gravecallData = (PutInGraveyardData)args;
        if (gravecallData.Card.BaseId == Card.BaseId)
        {
            if (!string.IsNullOrEmpty(_variable))
            {
                Parent.StoredVariables.Add(_variable, gravecallData.Position);
            }
            Parent.TriggerAction();
        }
    }

    public override void AfterTriggeringAction(UnityAction callback)
    {
        if (!string.IsNullOrEmpty(_variable))
            Parent.StoredVariables.Remove(_variable);
        base.AfterTriggeringAction(callback);
    }

    public override string ToString()
    {
        return "<b>Gravecall</b>";
    }
}
