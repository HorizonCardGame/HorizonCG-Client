﻿public class ArriveCondition : ConditionNode
{
    public ArriveCondition() : base() { }

    public override void Setup(Card c)
    {
        base.Setup(c);
        Card.Subscribe(EventNames.EnteredField, OnArrive);
    }

    public override void Stop()
    {
        if(Card != null)
        {
            Card.Unsubscribe(EventNames.EnteredField);
        }
    }

    public void OnArrive(IBaseEventUser sender, object args)
    {
        var arrivalData = (EnteredFieldEventData)args;
        if(arrivalData.Card.BaseId == Card.BaseId)
        {
            Parent.TriggerAction();
        }
    }

    public override string ToString()
    {
        return "<b>Arrive</b>";
    }
}
