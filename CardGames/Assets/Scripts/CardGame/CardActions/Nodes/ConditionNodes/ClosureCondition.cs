﻿using System.Collections.Generic;
using System.Text;
using TCG.Enums;

public class ClosureCondition : ConditionNode
{
    public ClosureCondition() : base() {}

    public override void Setup(Card c)
    {
        base.Setup(c);
        if(Card.Controller != null)
        {
            Card.Subscribe(EventNames.EndOfTurn, OnClosure, Card.Controller.BaseId.ToString());
        }
    }

    public override void Stop()
    {
        if (Card != null && Card.Controller != null)
        {
            Card.Unsubscribe(EventNames.EndOfTurn, Card.Controller.BaseId.ToString());
        }
    }

    public void OnClosure(IBaseEventUser sender, object args)
    {
        if(Card.IsOnField())
            Parent.TriggerAction();
    }

    public override string ToString()
    {
        return "<b>Closure</b>";
    }
}