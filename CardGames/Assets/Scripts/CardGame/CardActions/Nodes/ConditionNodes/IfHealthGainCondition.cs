﻿using UnityEngine;

public class IfHealthGainCondition : ConditionNode, IRequirementNode
{
    private bool MustBeController; private int healthGainedOnTurn = -1;
    public IfHealthGainCondition(bool mustBeController) : base()
    {
        MustBeController = mustBeController;
    }

    public override void Setup(Card c)
    {
        base.Setup(c);
        Card.Subscribe(EventNames.GainHealth, OnGainHealth, MustBeController && Card.Controller != null ? Card.Controller.BaseId.ToString() : null);
    }

    public override void Stop()
    {
        if (Card != null)
        {
            Card.Unsubscribe(EventNames.GainHealth, MustBeController && Card.Controller != null ? Card.Controller.BaseId.ToString() : null);
        }
    }

    public void OnGainHealth(IBaseEventUser sender, object args)
    {
        healthGainedOnTurn = GameMaster.Instance.CurrentTurn;
    }

    public override string ToString()
    {
        return MustBeController ? "<b>If you have gained health this turn</b>" : "<b>If a player has gained health this turn</b>";
    }

    public bool IsRequirementMet()
    {
        return GameMaster.Instance.CurrentTurn == healthGainedOnTurn;
    }
}