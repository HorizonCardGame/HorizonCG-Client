﻿public class OnAttackCondition : ConditionNode
{
    public OnAttackCondition() : base() { }

    public override void Setup(Card c)
    {
        base.Setup(c);
        Card.Subscribe(EventNames.OnAttack, OnAttack);
    }

    public override void Stop()
    {
        if(Card != null)
        {
            Card.Unsubscribe(EventNames.OnAttack);
        }
    }

    public void OnAttack(IBaseEventUser sender, object args)
    {
        if(((PawnController)sender).Card.GetId() == Card.BaseId)
        {
            Parent.TriggerAction();
        }
    }

    public override string ToString()
    {
        return "<b>When this attacks</b>";
    }
}
