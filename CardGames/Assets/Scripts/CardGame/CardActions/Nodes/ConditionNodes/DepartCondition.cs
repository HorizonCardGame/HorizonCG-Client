﻿using UnityEngine;
using UnityEngine.Events;

public class DepartCondition : ConditionNode
{
    public DepartCondition(string variable = null) : base(variable) { }
    
    public override void Setup(Card c)
    {
        base.Setup(c);
        Card.Subscribe(EventNames.LeftField, OnDepart);
    }

    public void OnDepart(IBaseEventUser sender, object args)
    {
        var departData = (LeftFieldEventData)args;
        if(departData.Card.BaseId == Card.BaseId)
        {
            if (!string.IsNullOrEmpty(_variable))
            {
                Parent.StoredVariables.Add(_variable, departData.LastPosition);
            }
            Parent.TriggerAction();
        }
    }

    public override void AfterTriggeringAction(UnityAction callback)
    {
        if(!string.IsNullOrEmpty(_variable))
            Parent.StoredVariables.Remove(_variable);
        base.AfterTriggeringAction(callback);
    }

    public override string ToString()
    {
        return "<b>Depart</b>";
    }
}
