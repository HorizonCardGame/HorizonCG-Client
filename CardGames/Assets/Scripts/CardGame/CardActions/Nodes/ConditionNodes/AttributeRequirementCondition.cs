﻿using System.Collections.Generic;
using System.Text;
using TCG.Enums;

public class AttributeRequirementCondition : ConditionNode, IRequirementNode
{
    public Dictionary<Attribute, int> AttributeRequirements { get; set; }
    public AttributeRequirementCondition(Dictionary<Attribute, int> attrs, string variable = null) : base(variable) {
        AttributeRequirements = attrs;
    }

    public override string ToString()
    {
        var sb = new StringBuilder();

        foreach(var attr in Constants.AttributesOrder)
        {

            if (AttributeRequirements.ContainsKey(attr))
            {
                var value = AttributeRequirements[attr];
                for (var i = 0; i < value; i++)
                {
                    switch (attr)
                    {
                        case Attribute.VIGOR: sb.Append("<aV/>"); break;
                        case Attribute.UNITY: sb.Append("<aU/>"); break;
                        case Attribute.WRATH: sb.Append("<aW/>"); break;
                        case Attribute.DECAY: sb.Append("<aD/>"); break;
                        case Attribute.PENANCE: sb.Append("<aP/>"); break;
                        case Attribute.AVARICE: sb.Append("<aA/>"); break;
                    }
                }
            }
        }

        return sb.ToString();
    }

    public bool IsRequirementMet()
    {
        return Card.Controller.HasAttributeRequirements(AttributeRequirements);
    }
}
