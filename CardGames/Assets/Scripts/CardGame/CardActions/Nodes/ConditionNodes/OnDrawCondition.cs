﻿public class OnDrawCondition : ConditionNode
{
    private bool MustBeController;
    public OnDrawCondition(bool mustBeController) : base() {
        MustBeController = mustBeController;
    }

    public override void Setup(Card c)
    {
        base.Setup(c);
        Card.Subscribe(EventNames.DrawsCard, OnDraw, MustBeController && Card.Controller != null ? Card.Controller.BaseId.ToString() : null);
    }

    public override void Stop()
    {
        if (Card != null)
        {
            Card.Unsubscribe(EventNames.DrawsCard, MustBeController && Card.Controller != null ? Card.Controller.BaseId.ToString() : null);
        }
    }

    public void OnDraw(IBaseEventUser sender, object args)
    {
        if (Card.IsOnField())
            Parent.TriggerAction();
    }

    public override string ToString()
    {
        return MustBeController ? "<b>When you draw a card</b>" : "<b>When a player draws a card</b>";
    }
}
