﻿using UnityEngine;

public class WhenGainingHealthCondition : ConditionNode
{
    private bool MustBeController;
    public WhenGainingHealthCondition(bool mustBeController) : base()
    {
        MustBeController = mustBeController;
    }

    public override void Setup(Card c)
    {
        base.Setup(c);
        Card.Subscribe(EventNames.GainHealth, OnGainHealth, MustBeController && Card.Controller != null ? Card.Controller.BaseId.ToString() : null);
    }

    public override void Stop()
    {
        if (Card != null)
        {
            Card.Unsubscribe(EventNames.GainHealth, MustBeController && Card.Controller != null ? Card.Controller.BaseId.ToString() : null);
        }
    }

    public void OnGainHealth(IBaseEventUser sender, object args)
    {
        if (Card.IsOnField())
            Parent.TriggerAction();
    }

    public override string ToString()
    {
        return MustBeController ? "<b>When you gain health</b>" : "<b>When a player gains health</b>";
    }
}