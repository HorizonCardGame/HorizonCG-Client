﻿using System.Collections.Generic;
using System.Text;
using TCG.Enums;

public class AdventCondition : ConditionNode
{
    public AdventCondition() : base() {}

    public override void Setup(Card c)
    {
        base.Setup(c);
        if(Card.Controller != null)
        {
            Card.Subscribe(EventNames.StartOfTurn, OnAdvent, Card.Controller.BaseId.ToString());
        }
    }

    public override void Stop()
    {
        if (Card != null && Card.Controller != null)
        {
            Card.Unsubscribe(EventNames.StartOfTurn, Card.Controller.BaseId.ToString());
        }
    }

    public void OnAdvent(IBaseEventUser sender, object args)
    {
        if(Card.IsOnField())
            Parent.TriggerAction();
    }

    public override string ToString()
    {
        return "<b>Advent</b>";
    }
}