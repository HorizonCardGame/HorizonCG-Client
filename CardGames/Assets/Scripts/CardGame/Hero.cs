﻿using UnityEngine;
using System.Collections;

public class Hero : MonoBehaviour
{
    public Card BaseCard { get; private set; }

    public Hero(Card card)
    {
        BaseCard = card;
    }
}
