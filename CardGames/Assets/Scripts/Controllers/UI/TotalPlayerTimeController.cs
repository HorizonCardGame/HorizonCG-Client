﻿using System.Collections;
using System.Collections.Generic;
using TCG.Enums;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

public class TotalPlayerTimeController : MonoBehaviour {

    public Text TimerText;
    public bool UseTimers = true;
    public float TickTime = 1;
    public UnityAction OnFinished;

    public TimerStates State { get; set; }

    private float _startTimerTime { get; set; }

    private float _pausedTimerTime = -1f;
    private float _totalPausedTime = 0f;
    private float _nextTimeTick = 0;

	public void Constructor(float startTime)
    {
        _startTimerTime = startTime;
    }

    public void Pause()
    {
        State = TimerStates.Pause;
        _pausedTimerTime = Time.time;
    }

    public void Run()
    {
        if(_pausedTimerTime > -1)
        {
            _totalPausedTime += Time.time - _pausedTimerTime;
            _pausedTimerTime = -1;
        }
        State = TimerStates.Run;
    }

    void Update()
    {
        if (State == TimerStates.Run && (Time.time + TickTime) - _nextTimeTick >= 0)
        {
            var difference = Time.time - _startTimerTime + _totalPausedTime;
            var timerRemaining = GetMaxTime() - difference;
            DisplayTimeRemaining(timerRemaining);
            if (timerRemaining <= 0)
            {
                if(OnFinished != null)
                {
                    OnFinished();
                }
            }
            _nextTimeTick = Time.time + TickTime;
        }
    }

    private void DisplayTimeRemaining(float timeToDisplay)
    {
        var minutes = Mathf.FloorToInt(timeToDisplay / (60));
        timeToDisplay -= minutes * (60);
        var seconds = Mathf.FloorToInt(timeToDisplay);
        var secondsStr = (seconds < 10) ? "0" + seconds : seconds.ToString();
        TimerText.text = string.Format("{0}:{1}", minutes, secondsStr);
    }

    private float GetMaxTime()
    {
        return Constants.TotalPlayerTimeInMinutes * 60;
    }
}
