﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandController : MonoBehaviour, IHandController {

    public BasePlayerController Owner { get; private set; }
    private List<Card> _cardsInHand { get; set; }

    protected float CardHeight { get; set; }

    public virtual void Constructor(BasePlayerController owner)
    {
        Owner = owner;
        _cardsInHand = new List<Card>();
    }

    public virtual void AddCardToHand(IEnumerable<Card> cards)
    {
        foreach (var card in cards)
        {
            _cardsInHand.Add(card);
        }
    }

    public virtual void RemoveCardFromHand(ICardController cc)
    {
        _cardsInHand.Remove(cc.GetCard());
    }

    public virtual int GetCardsInHand()
    {
        return _cardsInHand.Count;
    }

    public virtual List<Card> GetCards()
    {
        return _cardsInHand;
    }
}
