﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckController : MonoBehaviour {

    public Text DeckCount;

    public BasicDeck Deck { get; set; }
    public BasePlayerController Owner { get; private set; }

	// Use this for initialization
	public void Initialize (BasePlayerController owner) {
        Owner = owner;
        Deck = new BasicDeck(TEST_DECKLIST, owner);
        Deck.Shuffle();
        UpdateDeckCount();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void UpdateDeckCount()
    {
        DeckCount.text = Deck.GetNumberOfCardsInDeck().ToString();
    }

    public int GetCurrentDeckCount()
    {
        return Deck.GetNumberOfCardsInDeck();
    }

    public IEnumerable<Card> DrawCard()
    {
        var card = Deck.DrawCard();
        UpdateDeckCount();

        return card;
    }

    public const string TEST_DECKLIST = "{" +
        "\"MASTER HERO\": [" +
        "{\"Number\":1, \"Name\":\"Blobica, Heroine of Blob\", \"Set\":\"TST\"}" +
        "]," +
        "\"MAINDECK\": [" +
        "{\"Number\":60, \"Name\":\"Blibble, the Test Blob\", \"Set\":\"TST\"}" +
        "]," +
        "\"SIDEDECK\": []" +
        "}";
}
