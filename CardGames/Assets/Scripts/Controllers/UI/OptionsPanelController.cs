﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class OptionsPanelController : OverlayPanelController {

    [SerializeField]
    private RightClickItemController ItemPrefab;
    [SerializeField]
    private float MarginFromClick = 0f;

    private MouseActions Actions { get; set; }
    private string _customTextHolderName = "Option Panel Holder";

    public void Start()
    {
        Actions = GetComponent<MouseActions>();
        Actions.OnLeftClick += OnLeftClick;

        Subscribe(EventNames.ShowOptionsMenu, Show);
        Hide();
    }

    public void Construct(Vector3 clickPosition, IEnumerable<RightClickItem> items)
    {
        var currentPlacement = 0f;
        foreach(var item in items)
        {
            var menuItem = GameMaster.Instance.Instantiate(ItemPrefab, this.transform);
            if (!item.Enabled)
            {
                menuItem.Construct(item.Text, null, true);
            }
            else
            {
                menuItem.Construct(item.Text, (PointerEventData data) => { item.ClickAction(data); Hide(); });
            }
            menuItem.rectTransform.position = clickPosition + new Vector3(MarginFromClick, currentPlacement, 0);
            currentPlacement -= menuItem.rectTransform.GetHeight();
        }
    }

    public void Show(IBaseEventUser sender, object args)
    {
        var data = (OptionsMenuEventData)args;

        Construct(data.ClickPosition, data.Items);
        base.Show();
    }

    public override void Hide()
    {
        base.Hide();
        GameMaster.Instance.DestroyChildren(this.transform);
    }

    public void OnLeftClick(PointerEventData pointerEventData)
    {
        Hide();
    }

    public void OnRightClick(PointerEventData pointerEventData)
    {
        Hide();
    }
}

public struct RightClickItem
{
    public string Text { get; set; }
    public UnityAction<PointerEventData> ClickAction { get; set; }
    public bool Enabled { get; set; }
}
