﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlayPanelController : BaseBehaviour {

    protected override void OnEnable() {}
    protected override void OnDisable() {}

    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }
}
