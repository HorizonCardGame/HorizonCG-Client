﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RightClickItemController : BaseUIController {

    public CText Text;
    public MouseActions mouseActions;
    public Image Background;

    private bool _disabled { get; set; }

    public void Construct(string text, UnityAction<PointerEventData> clickAction, bool disabled = false)
    {
        SetRectTransform();
        _disabled = disabled;

        Background.color = Colors.HexToColor((_disabled) ? Constants.DisabledColor : Constants.DefaultColor);
        Text.UpdateText(text, 14f);
        var preferredSize = Text.PreferredSize;

        Text.Resize(preferredSize);
        rectTransform.SetHeight(preferredSize.Height);

        mouseActions.OnLeftClick += clickAction;
        mouseActions.OnMouseEnter += OnMouseEnterItem;
        mouseActions.OnMouseExit += OnMouseExitItem;
    }

    private void OnMouseEnterItem(PointerEventData pointerEventData)
    {
        if(!_disabled)
            Background.color = Colors.HexToColor(Constants.DefaultMouseOverColor);
    }

    public void OnMouseExitItem(PointerEventData pointerEventData)
    {
        if (!_disabled)
            Background.color = Colors.HexToColor(Constants.DefaultColor);
    }
}
