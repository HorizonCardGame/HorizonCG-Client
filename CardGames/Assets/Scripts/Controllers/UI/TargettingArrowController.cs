﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargettingArrowController : MonoBehaviour {

    public RectTransform Rect { get; set; }
    public Image Arrow;
    public Vector3 StartingPosition { get; set; }
    private bool _shouldAnimateArrow = false;

	// Use this for initialization
	void Start () {
        Rect = GetComponent<RectTransform>();
        SetStartingPosition(transform.position);
	}
	
    public void SetStartingPosition(Vector3 start)
    {
        StartingPosition = start;
    }

    public void SetAnimationAndShow(bool animateAndShow)
    {
        _shouldAnimateArrow = animateAndShow;
        Arrow.enabled = animateAndShow;
    }

    public void SetArrowColor(Color color)
    {
        Arrow.color = color;
    }

	// Update is called once per frame
	void Update () {
        if(_shouldAnimateArrow)
        {
            var finalPosition = Input.mousePosition;
            Vector3 centerPos = (StartingPosition + finalPosition) / 2f;
            transform.position = centerPos;
            Vector3 direction = finalPosition - StartingPosition;
            direction = Vector3.Normalize(direction);
            transform.right = direction;
            Rect.SetWidth(Vector3.Distance(StartingPosition, finalPosition - (2 * direction)));
        }
    }
}
