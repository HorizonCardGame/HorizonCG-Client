﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalHandController : HandController
{
    public float SpacingOnCards = 1.0f;
    public float CardWidth = 150f;
    public float CardHeightNudge = 2f;
    public int MaxCardsOnScreen = 7;

    private List<ICardController> _cardsControllersInHand { get; set; }

    public override void Constructor(BasePlayerController owner)
    {
        base.Constructor(owner);
        _cardsControllersInHand = new List<ICardController>();
        CardHeight = CardWidth * (1 / Constants.CardWidthHeightRatio);
    }

    private void ArrangeCards()
    {
        var parentPos = transform.position;
        var scrunch = (_cardsControllersInHand.Count > MaxCardsOnScreen) ? (CardWidth * (_cardsControllersInHand.Count - MaxCardsOnScreen)) / _cardsControllersInHand.Count : 0f;
        var spacing = (_cardsControllersInHand.Count > MaxCardsOnScreen) ? 0 : SpacingOnCards;
        for (var i = 0; i < _cardsControllersInHand.Count; i++)
        {
            var currentCard = _cardsControllersInHand[i];
            currentCard.SetSize(CardWidth, CardHeight);
            //There is a tiny error in this math and I don't know what it is. TODO: FIX THE MATH ERROR
            currentCard.GetTransform().position = new Vector3((parentPos.x + (spacing * (1 + i)) + (CardWidth - scrunch) * i),
                                                                parentPos.y + CardHeightNudge - (CardHeight / 2), 1f * i);
            currentCard.AddedToAHand();
        }
    }

    public override void AddCardToHand(IEnumerable<Card> cards)
    {
        foreach (var card in cards)
        {
            var cardController = GameMaster.Instance.CreateCardController(card, true);

            cardController.transform.parent = transform;
            _cardsControllersInHand.Add(cardController);
        }
        ArrangeCards();
    }

    public override void RemoveCardFromHand(ICardController cc)
    {
        _cardsControllersInHand.Remove(cc);
        GameMaster.Instance.Destroy(cc.GetGameObject());
        ArrangeCards();
    }

    public override int GetCardsInHand()
    {
        return _cardsControllersInHand.Count;
    }
}
