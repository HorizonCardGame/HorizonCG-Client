﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using TCG.Enums;

public abstract class BasePlayerHeaderController : MonoBehaviour
{
    public Image HeroImage;
    public Image ImageBorder;
    public Text Health;
    public Text Armor;
    public Text CardsInDeck;
    public Text CardsInDiscard;
    public Text CardsInBanishment;
    public Text CardsInHand;
    public Text CurrentAether;
    public Text MaxAether;
    public Text Essense;
    public AttributePanelController AttributePanel;
    public GameObject HealthChangePrefab;

    public ReactionTimerController ReactionTimer;
    public TotalPlayerTimeController TotalPlayerTimer;

    public MouseActions HeroActions;
    public MouseActions DiscardActions;
    public MouseActions BanishedActions;

    public void ChangeInHealth(int amount)
    {
        var factor = GameMaster.Instance.Instantiate(HealthChangePrefab, this.transform);
        var healthChange = factor.GetComponent<HealthChangeNumber>();
        healthChange.Constructor(amount, Health.transform.position);
    }

    public void SetParameters(int health, int armor, int cardsInDeck, int cardsInDiscard, int cardsInBanishment, int cardsInHand, int currentAether, int maxAether, int charges, Dictionary<Attribute, int> dict)
    {
        this.Health.text = health.ToString();
        this.Armor.text = armor.ToString();
        this.CardsInDeck.text = cardsInDeck.ToString();
        this.CardsInDiscard.text = cardsInDiscard.ToString();
        this.CardsInBanishment.text = cardsInBanishment.ToString();
        this.CardsInHand.text = cardsInHand.ToString();
        this.CurrentAether.text = currentAether.ToString();
        this.MaxAether.text = maxAether.ToString();
        this.Essense.text = charges.ToString();
        this.AttributePanel.Construct(dict);
    }

    public void SetImageUI(Sprite heroImage, Color borderColor)
    {
        HeroImage.sprite = heroImage;
        ImageBorder.color = borderColor;
    }
}
