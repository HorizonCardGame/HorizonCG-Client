﻿using System.Collections;
using System.Collections.Generic;
using TCG.Enums;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainPlayerHeaderController : BasePlayerHeaderController {


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (ReactionTimer.State == TimerStates.Run)
            {
                ReactionTimer.PauseTimer();
            }
            else if(ReactionTimer.State == TimerStates.Pause)
            {
                ReactionTimer.UnpauseTimer();
            }
        }
    }
}
