﻿using System.Collections;
using System.Collections.Generic;
using TCG.Enums;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(MouseActions))]
public class OtherZonesPanelController : OverlayPanelController
{
    public ScrollRect ScrollArea;

    private ZoneTypes _zone;
    private MouseActions _mouseActions { get; set; }

    public void Start()
    {
        _mouseActions = GetComponent<MouseActions>();
        _mouseActions.OnLeftClick += OnClick;

        Subscribe(EventNames.ShowOtherZone, ShowCards);
        Hide();
    }

    public void ShowCards(IBaseEventUser sender, object args)
    {
        var data = (OtherCardZoneEventData)args;
        _zone = data.Zone;
        foreach(var card in data.Cards)
        {
            var c = GameMaster.Instance.CreateCardController(card);
            c.transform.parent = ScrollArea.content.transform;
        }

        base.Show();
    }

    public override void Hide()
    {
        if(GameMaster.Instance != null)
            GameMaster.Instance.DestroyChildren(ScrollArea.content);

        base.Hide();
    }

    private void OnClick(PointerEventData pointerEventData)
    {
        Hide();
    }
}