﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiscardController : MonoBehaviour {

    public Image TopCardDisplay;

    private List<Card> _cardsInDiscard { get; set; }

	// Use this for initialization
	void Start () {
        _cardsInDiscard = new List<Card>();
        UpdateTopDisplay();
	}
	
	// Update is called once per frame
	private void UpdateTopDisplay ()
    {
		if(_cardsInDiscard.Count <= 0)
        {
            TopCardDisplay.enabled = false;
        }
        else
        {
            TopCardDisplay.enabled = true;
            //TopCardDisplay.sprite = _cardsInDiscard[0].CardImage; //TODO This will need to be changed when the card rework happens
        }
	}

    public void AddCard(ICardController cc)
    {
        _cardsInDiscard.Insert(0, cc.GetCard());
        UpdateTopDisplay();
    }

    public IEnumerable<Card> GetCards()
    {
        return _cardsInDiscard;
    }
}
