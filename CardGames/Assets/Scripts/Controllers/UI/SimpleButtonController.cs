﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TCG.Utility;

public class SimpleButtonController : MonoBehaviour {

    public Card CardToPlay { get; set; }

	// Use this for initialization
	void Start () {
        CardToPlay = new Card();

        var button = GetComponent<Button>();
        button.onClick.AddListener(PlayCard);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayCard()
    {
        Debug.Log("Played card " + CardToPlay.Name);
        //GameMaster.Instance.PlayCard(CardToPlay);
    }
}
