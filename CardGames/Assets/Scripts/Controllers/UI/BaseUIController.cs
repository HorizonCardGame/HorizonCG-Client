﻿using UnityEngine;
using System.Collections;

public class BaseUIController : MonoBehaviour
{
    public RectTransform rectTransform { get; set; }

    // Use this for initialization
    void Awake()
    {
        SetRectTransform();
    }

    public void SetRectTransform()
    {
        if(rectTransform == null)
        {
            rectTransform = GetComponent<RectTransform>();
        }
    }
}
