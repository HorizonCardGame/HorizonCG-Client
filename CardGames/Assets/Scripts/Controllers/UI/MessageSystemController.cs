﻿using UnityEngine;
using System.Collections;

public class MessageSystemController : OverlayPanelController
{
    public CText Text;
    private float _timeToHide = 0f;

    public void Start()
    {
        Subscribe(EventNames.DisplayMessage, DisplayMessage);
        Subscribe(EventNames.DisplayError, DisplayError);
    }

    public void Show(string text, float fadeTime)
    {
        Text.UpdateText(text);
        _timeToHide = Time.time + fadeTime;
        Show();
    }

    private void Update()
    {
        if(Time.time > _timeToHide && gameObject.activeSelf)
        {
            Hide();
        }
    }

    public void DisplayMessage(IBaseEventUser sender, object textArg)
    {
        var text = (string)textArg;
        Show(text, Constants.StandardMessageTime);
    }

    public void DisplayError(IBaseEventUser sender, object textArg)
    {
        var text = (string)textArg;
        var errorRed = Colors.HexToColor(Constants.ErrorRed);
        Show(string.Format("<color=\"{0},{1},{2}\">{3}</color>", errorRed.r, errorRed.g, errorRed.b, text), Constants.StandardMessageTime);
    }
}
