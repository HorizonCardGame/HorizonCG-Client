﻿using System.Collections;
using System.Collections.Generic;
using TCG.Enums;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ReactionTimerController : MonoBehaviour {

    public Slider Bar;
    public bool PassOnReacting = false;
    public UnityAction OnFinished;

    public TimerStates State { get; set; }

    private float _startingTimerTime { get; set; }

    // Use this for initialization
    void Start () {
        State = TimerStates.Stop;
        Bar.maxValue = Constants.ReactTimerAmount;
        Bar.minValue = 0;
        Hide();
    }
	
	// Update is called once per frame
	void Update () {
        if (PassOnReacting)
        {
            if (gameObject.activeSelf)
                Hide();
            return;
        }

        if(State == TimerStates.Run)
        {
            var difference = Time.time - _startingTimerTime;
            var timeRemaining = Constants.ReactTimerAmount - difference;
            Bar.value = timeRemaining;
            if(timeRemaining <= 0)
            {
                TimerFinished();
            }
        }
	}

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void StartTimer()
    {
        if (PassOnReacting)
        {
            TimerFinished();
            return;
        }
        _startingTimerTime = Time.time;
        State = TimerStates.Run;
        Show();
    }

    public void PauseTimer()
    {
        State = TimerStates.Pause;
    }

    public void UnpauseTimer()
    {
        StartTimer();
    }

    private void TimerFinished()
    {
        State = TimerStates.Stop;
        Hide();
        if(OnFinished != null)
        {
            OnFinished();
        }
    }
}
