﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardPeekController : OverlayPanelController {

    public CardController PreviewCard;
    public CardController OriginalCard;
    public GameObject OriginalCardPanel;
    public CardController RelatedCards1;
    public CardController RelatedCards2;
    public CardController RelatedCards3;
    public GameObject RelatedCardsPanel;

    private MouseActions _clickable { get; set; }

    public void Start()
    {
        _clickable = GetComponent<MouseActions>();
        _clickable.OnLeftClick += OnClick;
        _clickable.OnMouseScrollDown += OnScrollDown;

        Subscribe(EventNames.PreviewCard, Show);
        Hide();
    }

    public void Show(IBaseEventUser sender, object cardArg)
    {
        Card card = (Card)cardArg;

        PreviewCard.Constructor(card, true, true);
        if (card.RelatedCards.Any())
        {
            //Todo, do this properly
            RelatedCards1.Constructor(card.RelatedCards[0], true, true);
            RelatedCardsPanel.SetActive(true);
        }
        if(card.BaseCardNameAndSet.First != card.Name || card.BaseCardNameAndSet.Second != card.CardSetId)
        {
            OriginalCard.Constructor(CardDatabase.Instance.GetCard(card.BaseCardNameAndSet.First, card.BaseCardNameAndSet.Second), true, true);
            OriginalCardPanel.SetActive(true);
        }
        //Keyword Displays

        base.Show();
    }

    public override void Hide()
    {
        RelatedCardsPanel.SetActive(false);
        RelatedCards2.gameObject.SetActive(false);
        RelatedCards3.gameObject.SetActive(false);
        OriginalCardPanel.SetActive(false);
        base.Hide();
    }

    private void OnClick(PointerEventData pointerEventData)
    {
        Hide();
    }

    private void OnScrollDown(Single value)
    {
        Hide();
    }
}
