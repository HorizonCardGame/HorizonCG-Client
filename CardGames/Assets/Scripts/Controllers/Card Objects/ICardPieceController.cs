﻿using UnityEngine;

public interface ICardPieceController {

    void UpdateFromCard(Card card);
    void UpdateVisibility(bool visible);
    void Rescale(Vector3 newScale);
}
