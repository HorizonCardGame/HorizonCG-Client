﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using TCG.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using TCG.Enums;

public class CardController : BaseCardVisual, ICardController, ICardObject, ICard
{
    public Sprite CardBack;
    public BottomBarController BottomSection;

    public float TimeToMouseOver = 1f;
    public float AmountToMouseUp = 20f;
    public bool DisableHandMouseOverBehavior = false;
    public bool AlwaysShowFaceUp = false;

    public Card Card { get; set; }
    public bool IsFaceUp { get; set; }
    public bool IsExhausted { get; set; }
    public bool IsAttacking { get; set; }

    private MouseActions _clickable { get; set; }
    private Sprite _currentCardFace { get; set; }
    private bool _isMousedOver { get; set; }
    private bool _isNotInOriginalSpot { get; set; }

    private bool _isInAHand { get; set; }
    private Vector3 _targetMouseOverLocation;
    private Vector3 _originalLocation;
    private bool _isDragging { get; set; }
    public Vector3 _draggingClickOffset { get; set; }

    public BasePlayerController Controller { get { return Card.Controller; } }
    public BasePlayerController Owner { get { return Card.Owner; } }

    private Material _defaultMaterial;
    public Material DefaultMaterial { get { return _defaultMaterial; } }
    private CanvasRenderer _renderer { get; set; }

    private TargettingSystemInfo _targetingSystemInfo { get; set; }

    void Start()
    {
        _clickable = GetComponent<MouseActions>();
        _clickable.OnDoubleClick += OnDoubleClick;
        _clickable.OnMouseEnter += OnPointerEnter;
        _clickable.OnMouseExit += OnPointerExit;
        _clickable.OnMouseScrollUp += OnScrollUp;
        _clickable.OnRightClick += OnRightClick;
        _clickable.OnLeftClickDown += OnPointerDown;
        _clickable.OnLeftClickUp += OnPointerUp;
        _isMousedOver = false;
        _renderer = GetComponent<CanvasRenderer>();
        _defaultMaterial = _renderer.GetMaterial();
    }

    public void Constructor(Card card)
    {
        card.AssignAnimatable(this);
        Card = card;
        UpdateCardState();

        _originalLocation = transform.position;
    }
    public void Constructor(Card card, bool isFaceUp, bool disableMouseOverBehaviour)
    {
        IsFaceUp = isFaceUp;
        DisableHandMouseOverBehavior = disableMouseOverBehaviour;

        Constructor(card);
    }

    public void Update()
    {
        if (!DisableHandMouseOverBehavior)
        {
            if (_isMousedOver && !_isDragging && transform.position != _targetMouseOverLocation)
            {
                transform.position = Vector3.Lerp(transform.position, _targetMouseOverLocation, Time.deltaTime * TimeToMouseOver);
            }
            else if (_isDragging)
            {
                transform.position = Vector3.Lerp(transform.position, Input.mousePosition + _draggingClickOffset, Time.deltaTime * 20f);
            }
            else if (!_isMousedOver && _isNotInOriginalSpot && transform.position != _originalLocation)
            {
                transform.position = Vector3.Lerp(transform.position, _originalLocation, Time.deltaTime * TimeToMouseOver);
                if (transform.position == _originalLocation)
                {
                    _isNotInOriginalSpot = false;
                }
            }
        }
    }

    #region Card Image and Face

    public void UpdateCardState()
    {
        if (Card == null)
            return;

        if (!string.IsNullOrEmpty(Card.CardSetId))
        {
            _currentCardFace = CardDatabase.Instance.GetCardImage(Card.CardSetId);
        }

        UpdateCardFace();
    }
    public void UpdateCardState(Card c)
    {
        Card = c;
        UpdateCardState();
    }

    public void UpdateCardFace()
    {
        if (IsFaceUp || AlwaysShowFaceUp)
        {
            UpdateVisualState();
        }
        else
        {
            UpdateBackOfCard();
        }
    }

    public void UpdateVisualState()
    {
        CardFace.sprite = _currentCardFace;

        MiddleSection.UpdateWith(Card.Attributes, Card.Attack, Card.DamageType, Card.Defense);
        BottomSection.UpdateWith(Card.Attributes, Card.Rarity, Card.CollectorsInfo, Card.Artist);
        TopBar.UpdateWith(Card.Cost, Card.Name, Card.Attributes);
        TextArea.UpdateWith(Card.TypeLine(), Card.Text, Card.Attributes);
        UpdateCardSectionVisiblities(true);

        if (DisableHandMouseOverBehavior)
            return;

        if (_isMousedOver)
        {
            if (ShouldBeTarget)
            {
                SelectionFrame.SetSelected();
            }
            else
            {
                SelectionFrame.SetSelectable();
            }
        }
        else
        {
            if (Card.HasSpecialActivation())
            {
                SelectionFrame.SetSpecialActivated();
            }
            else if (ShouldBeTarget)
            {
                SelectionFrame.SetSelectable();
            }
            else
            {
                SelectionFrame.SetOff();
            }
        }
    }

    public void UpdateBackOfCard()
    {
        CardFace.sprite = CardBack;
        UpdateCardSectionVisiblities(false);
    }

    public void UpdateCardSectionVisiblities(bool visible, bool andCardImage = false)
    {
        MiddleSection.UpdateVisibility(visible);
        BottomSection.UpdateVisibility(visible);
        TopBar.UpdateVisibility(visible);
        TextArea.UpdateVisibility(visible);

        CardFrame.enabled = visible;
        CardFace.enabled = (!andCardImage) ? true : visible;
    }
    #endregion

    public void PlayCard()
    {
        if (DisableHandMouseOverBehavior)
            return;

        if (!Controller.CanPlayCard(Card))
        {
            Publish(EventNames.DisplayError, Localization.CanNotPlayCard);
            return;
        }

        //OnPlayTrigger

        Controller.PlayCard(this);
        _isInAHand = false;
    }

    public void AetherizeCard()
    {
        if (DisableHandMouseOverBehavior)
            return;

        if (!Controller.CanAetherizeCards())
        {
            Publish(EventNames.DisplayError, Localization.CanNotAetherizeMore);
            return;
        }

        //On Aetherize Trigger
        Aetherize();
    }

    public void Aetherize()
    {
        Controller.AetherizeCard(this);
        RemoveGameObject();
        _isInAHand = false;
    }

    public void Destroy()
    {
        //OnDestroyed

        MoveToDiscard();
    }

    public void MoveToDiscard()
    {
        //OnMovedToDiscard

        _isInAHand = false;
        Owner.MoveCardToDiscard(this);
        RemoveGameObject();
    }

    public void Banish()
    {
        _isInAHand = false;
        Owner.MoveCardToBanished(Card);
        RemoveGameObject();
    }

    public void AddedToAHand()
    {
        _isInAHand = true;
        _originalLocation = transform.position;
        _targetMouseOverLocation = transform.position + new Vector3(0, AmountToMouseUp, 0);
    }

    public void Transform(Card target)
    {
        //TODO Add Transform mechanics
        UpdateCardState();
    }

    public void Revert()
    {

    }

    public void AdjustStatistics(JSONNode statistics)
    {

    }

    public void Attack()
    {
        if (!IsExhausted)
        {
            //Attacking Trigger
            IsAttacking = true;
            var targets = Controller.GetListOfValidDefenders();
            //Controller.SetAttacking(this, targets[0]);
            UpdateCardFace();
        }
    }

    public void RemoveFromAttacking()
    {
        IsAttacking = false;
        //Controller.RemovingAttacking(this);
        UpdateCardFace();
    }

    public void AttackComplete()
    {
        IsAttacking = false;
        Exhaust();
    }

    public void Exhaust()
    {
        IsExhausted = true;
        UpdateCardFace();
    }

    public void Recover()
    {
        IsExhausted = false;
        UpdateCardFace();
    }

    public void DealDamage(IDamageable target)
    {
        //deal damage trigger
        target.DealtDamage(this, Card.Attack, Card.DamageType);
    }

    public void DealtDamage(ICardController target, int damage)
    {
        throw new System.NotImplementedException();
    }

    public void ShowCard()
    {
        Publish(EventNames.PreviewCard, this.Card);
    }

    #region User Interactions

    private void OnDoubleClick(PointerEventData pointEventData)
    {
        PlayCard();
    }

    private void OnPointerEnter(PointerEventData pointerEventData)
    {
        _isMousedOver = true;
        _isNotInOriginalSpot = true;

        if (ShouldBeTarget)
        {
            _targetingSystemInfo.TargetingCallback(this, TargettingSystemCallbacks.Entered);
        }
        UpdateVisualState();
    }

    private void OnPointerExit(PointerEventData pointerEventData)
    {
        _isMousedOver = false;

        if (ShouldBeTarget)
        {
            _targetingSystemInfo.TargetingCallback(this, TargettingSystemCallbacks.Exited);
        }
        UpdateVisualState();
    }

    private void OnScrollUp(Single value)
    {
        ShowCard();
    }

    private void OnPointerDown(PointerEventData pointerEventData)
    {
        if (DisableHandMouseOverBehavior)
            return;

        _isDragging = true;
        _draggingClickOffset = transform.position - new Vector3(pointerEventData.position.x, pointerEventData.position.y, 0);
        GameMaster.Instance.ShowDraggableUI();
    }

    private void OnPointerUp(PointerEventData pointerEventData)
    {
        if (DisableHandMouseOverBehavior)
            return;

        _isDragging = false;
        if (GameMaster.Instance.GetAetherizedAreaBounds().Contains(pointerEventData.position))
        {
            AetherizeCard();
        }
        else if(GameMaster.Instance.GetPlayAreaBounds().Contains(pointerEventData.position))
        {
            PlayCard();
        }
        GameMaster.Instance.HideDraggableUI();
    }

    private void OnRightClick(PointerEventData pointerEventData)
    {
        if (DisableHandMouseOverBehavior)
            return;

        var items = new List<RightClickItem>();
        items.Add(new RightClickItem { Text = "View Card", ClickAction = (PointerEventData data) => { ShowCard(); }, Enabled = true });

        if (IsLocallyControlled())
        {
            items.Add(new RightClickItem {
                Text = "Play Card",
                ClickAction = (PointerEventData data) => { PlayCard(); },
                Enabled = Controller.CanPlayCard(Card)
            });
            items.Add(new RightClickItem {
                Text = string.Format("Aetherize Card ({0})", Controller.NumberOfCardsCanBeAetherized()),
                ClickAction = (PointerEventData data) => { AetherizeCard(); },
                Enabled = Controller.CanAetherizeCards()
            });

            var options = Card.Actions.Where(x => x.HasOptionMenuItem(CardZones.Hand));
            if (options.Any())
            {
                foreach(var opt in options)
                {
                    items.Add(opt.GetRightClickItem());
                }
            }
        }

        Publish(EventNames.ShowOptionsMenu, new OptionsMenuEventData
        {
            ClickPosition = pointerEventData.position,
            Items = items
        });
    }
    #endregion

    #region Accessors
    public override Card GetCard()
    {
        return Card;
    }

    public RectTransform GetRect()
    {
        return GetComponent<RectTransform>();
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public void RemoveGameObject()
    {
        //UpdateCardSectionVisiblities(false, true);
        //Card.RemoveAnimatable();
        GameMaster.Instance.Destroy(this.gameObject);
    }

    public void SetSizeByHeight(float height)
    {
        var width = height * Constants.CardWidthHeightRatio;
        SetSize(width, height);
    }
    public void SetSizeByWidth(float width)
    {
        var height = width * (1 / Constants.CardWidthHeightRatio);
        SetSize(width, height);
    }
    public void SetSize(float width, float height)
    {
        var rect = GetRect();
        var w = rect.GetWidth();
        var h = rect.GetHeight();
        var newW = width/w;
        var newH = height/h;
        transform.localScale = new Vector3(newW, newH);
    }

    public bool IsLocallyControlled()
    {
        return Controller == GameMaster.Instance.LocalPlayer;
    }
    #endregion

    #region ICardObject

    public void OnStartOfTurn()
    {

    }

    public void ChangeMaterial(Material material)
    {
        _renderer.SetMaterial(material,0);
    }

    public void ResetToDefaultMaterial()
    {
        _renderer.SetMaterial(_defaultMaterial,0);
    }
    #endregion

    public override void SetTargetingInfo(IBaseEventUser sender, TargettingSystemInfo targettingInfo)
    {
        if (!targettingInfo.Filter.IsTarget(this))
            return;

        ShouldBeTarget = true;
        _targetingSystemInfo = targettingInfo;
    }
}