﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TCG.Enums;
using UnityEngine;
using UnityEngine.UI;

public class TextAreaController : MonoBehaviour
{
    public CText TextArea;
    public Image Textbox;
    public Image Typebox;
    public CText Typeline;
    public HeroActionBoxController HeroActionPrefab;
    public float PaddingAroundTextBox = 4f;
    public float MaximumTextboxHeight = 100f;
    public float HeightOfTypeLine = 26f;
    public int DefaultTextSize = 10;

    private List<HeroActionBoxController> HeroActions { get; set; }

    void Awake()
    {
        HeroActions = new List<HeroActionBoxController>();
    }

    public void UpdateWith(string typeline, string text, List<Attribute> attributes)
    {
        var holderName = "Generated Hero Abilities";
        if (transform.Find(holderName))
        {
            DestroyImmediate(transform.Find(holderName).gameObject);
        }

        var actionHolder = new GameObject(holderName).transform;
        actionHolder.parent = transform;
        actionHolder.localPosition = Vector3.zero;

        Typeline.UpdateText(typeline);

        var mainText = ""; var heroActionLines = new List<string>(); HeroActions = new List<HeroActionBoxController>();
        using (StringReader reader = new StringReader(text))
        {
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (line.Contains(Constants.HeroActionBreak))
                {
                    heroActionLines.Add(line);
                }
                else
                {
                    mainText += line;
                }
            }
        }

        Typebox.sprite = GetTypelineGraphic(attributes);
        Textbox.sprite = GetTextboxGraphic(attributes);

        if (string.IsNullOrEmpty(mainText))
        {
            Textbox.rectTransform.SetHeight(HeightOfTypeLine);
            TextArea.rectTransform.SetHeight(0);
            return;
        }

        TextArea.UpdateText(mainText, DefaultTextSize);
        var heightOfText = TextArea.PreferredSize.Height;
        var previousHeroActionHeight = 0f;

        if(heroActionLines.Any())
        {
            for(var i = heroActionLines.Count; i > 0; i--)
            {
                var obj = GameMaster.Instance.Instantiate(HeroActionPrefab.gameObject, actionHolder);
                var newHA = obj.GetComponent<HeroActionBoxController>();
                var split = heroActionLines[i-1].Split(new string[] { Constants.HeroActionBreak }, System.StringSplitOptions.None);
                newHA.UpdateFromSource(attributes, int.Parse(split[0]), split[1]);
                newHA.transform.localPosition = new Vector3(0, previousHeroActionHeight, 0);
                previousHeroActionHeight += (newHA.GetHeight() + PaddingAroundTextBox * 2);
                if(i == heroActionLines.Count || i == 0)
                {
                    previousHeroActionHeight -= PaddingAroundTextBox;
                }
                HeroActions.Add(newHA);
            }
        }

        var maxHeight = MaximumTextboxHeight - previousHeroActionHeight;
        if(maxHeight < heightOfText)
        {
            heightOfText = maxHeight;
            TextArea.ResizeToFit = true;
        }

        Textbox.rectTransform.SetHeight((3 * PaddingAroundTextBox) + heightOfText + HeightOfTypeLine);
        Textbox.rectTransform.localPosition = new Vector3(0, previousHeroActionHeight, 0);

        TextArea.rectTransform.SetHeight(heightOfText);
        TextArea.rectTransform.SetWidth(Textbox.rectTransform.GetWidth() - (2 * PaddingAroundTextBox));
        TextArea.rectTransform.localPosition = new Vector3(PaddingAroundTextBox, PaddingAroundTextBox, 0);
        TextArea.Construct(false);
    }

    public void UpdateVisibility(bool visible)
    {
        TextArea.enabled = visible;
        Textbox.enabled = visible;
        Typebox.enabled = visible;
        Typeline.enabled = visible;

        foreach(var ha in HeroActions)
        {
            ha.UpdateVisibility(visible);
        }
    }

    private Sprite GetTextboxGraphic(List<Attribute> attributes)
    {
        var stringAT = ""; var numberOfAttr = 0;

        if (attributes.Contains(Attribute.VIGOR))
        {
            stringAT = "Textbox_vigor-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.UNITY))
        {
            stringAT = "Textbox_unity-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.WRATH))
        {
            stringAT = "Textbox_wrath-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.DECAY))
        {
            stringAT = "Textbox_decay-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.PENANCE))
        {
            stringAT = "Textbox_penance-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.AVARICE))
        {
            stringAT = "Textbox_avarice-02";
            numberOfAttr++;
        }

        if(numberOfAttr > 1)
        {
            stringAT = "Textbox_gold-02";
        }
        else if(string.IsNullOrEmpty(stringAT))
        {
            stringAT = "Textbox_neutral-02";
        }

        return Resources.Load<Sprite>(string.Format("Images/CardComponents/{0}", stringAT));
    }

    private Sprite GetTypelineGraphic(List<Attribute> attributes)
    {
        var stringAT = ""; var numberOfAttr = 0;

        if (attributes.Contains(Attribute.VIGOR))
        {
            stringAT = "TypeLine_vigor-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.UNITY))
        {
            stringAT = "TypeLine_unity-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.WRATH))
        {
            stringAT = "TypeLine_wrath-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.DECAY))
        {
            stringAT = "TypeLine_decay-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.PENANCE))
        {
            stringAT = "TypeLine_penance-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.AVARICE))
        {
            stringAT = "TypeLine_avarice-02";
            numberOfAttr++;
        }

        if (numberOfAttr > 1)
        {
            stringAT = "TypeLine_gold-02";
        }
        else if (string.IsNullOrEmpty(stringAT))
        {
            stringAT = "TypeLine_neutral-02";
        }

        return Resources.Load<Sprite>(string.Format("Images/CardComponents/{0}", stringAT));
    }

    public void Rescale(Vector3 newScale)
    {
        Typeline.Resize(newScale);
        TextArea.Resize(newScale);
    }
}
