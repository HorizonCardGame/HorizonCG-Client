﻿using System.Collections;
using System.Collections.Generic;
using TCG.Enums;
using UnityEngine;
using UnityEngine.UI;

public class HeroActionBoxController : MonoBehaviour {

    public Image MainBoxBackground;
    public Text MainBoxText;
    public Image EssenseRequirementBoxBackground;
    public Text EssenseRequirementText;

    public float MinimumOfHeroAbility = 15f;
    public float MaximumOfHeroAbility = 32f;

    public void UpdateFromSource(List<Attribute> attributes, int essense, string text)
    {
        MainBoxText.text = text;
        EssenseRequirementText.text = essense.ToString();
        MainBoxBackground.sprite = GetTextboxGraphic(attributes);
        EssenseRequirementBoxBackground.sprite = GetHeroBoxGraphic(attributes);

        var heightOfText = MainBoxText.preferredHeight;
        if (MaximumOfHeroAbility < heightOfText)
        {
            heightOfText = MaximumOfHeroAbility;
            MainBoxText.resizeTextForBestFit = true;
        }
        else if(MinimumOfHeroAbility > heightOfText)
        {
            heightOfText = MinimumOfHeroAbility;
        }

        MainBoxBackground.rectTransform.SetHeight(heightOfText);
        MainBoxText.rectTransform.SetHeight(heightOfText);
        EssenseRequirementBoxBackground.rectTransform.SetHeight(heightOfText);
    }

    public void UpdateVisibility(bool visible)
    {
        MainBoxBackground.enabled = visible;
        MainBoxText.enabled = visible;
        EssenseRequirementBoxBackground.enabled = visible;
        EssenseRequirementText.enabled = visible;
    }

    private Sprite GetTextboxGraphic(List<Attribute> attributes)
    {
        var stringAT = ""; var numberOfAttr = 0;

        if (attributes.Contains(Attribute.VIGOR))
        {
            stringAT = "Textbox_vigor-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.UNITY))
        {
            stringAT = "Textbox_unity-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.WRATH))
        {
            stringAT = "Textbox_wrath-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.DECAY))
        {
            stringAT = "Textbox_decay-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.PENANCE))
        {
            stringAT = "Textbox_penance-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.AVARICE))
        {
            stringAT = "Textbox_avarice-02";
            numberOfAttr++;
        }

        if (numberOfAttr > 1)
        {
            stringAT = "Textbox_gold-02";
        }
        else if (stringAT.Contains(""))
        {
            stringAT = "Textbox_neutral-02";
        }

        return Resources.Load<Sprite>(string.Format("Images/CardComponents/{0}", stringAT));
    }

    private Sprite GetHeroBoxGraphic(List<Attribute> attributes)
    {
        var stringAT = ""; var numberOfAttr = 0;

        if (attributes.Contains(Attribute.VIGOR))
        {
            stringAT = "HeroBox_Vigor-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.UNITY))
        {
            stringAT = "HeroBox_Unity-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.WRATH))
        {
            stringAT = "HeroBox_Wrath-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.DECAY))
        {
            stringAT = "HeroBox_Decay-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.PENANCE))
        {
            stringAT = "HeroBox_Penance-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.AVARICE))
        {
            stringAT = "HeroBox_Avarice-02";
            numberOfAttr++;
        }

        if (numberOfAttr > 1)
        {
            stringAT = "HeroBox_Gold-02";
        }
        else if (stringAT == "")
        {
            stringAT = "HeroBox_Neutral-02";
        }

        return Resources.Load<Sprite>(string.Format("Images/CardComponents/{0}", stringAT));
    }

    public float GetHeight()
    {
        return MainBoxBackground.rectTransform.GetHeight();
    }
}
