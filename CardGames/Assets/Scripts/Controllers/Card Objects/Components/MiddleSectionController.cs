﻿using System.Collections;
using System.Collections.Generic;
using TCG.Enums;
using UnityEngine;

public class MiddleSectionController : MonoBehaviour
{
    public AttributeController Attributes;
    public AtkDefController AttackBubble;
    public AtkDefController DefenseBubble;

    public void Rescale(Vector3 newScale)
    {
        AttackBubble.Rescale(newScale);
        DefenseBubble.Rescale(newScale);
    }

    public void UpdateWith(List<Attribute> attributes, int attack, DamageTypes dt, int defense)
    {
        Attributes.UpdateWith(attributes);
        AttackBubble.UpdateWith(attack, dt);
        DefenseBubble.UpdateWith(defense);
    }

    public void UpdateVisibility(bool visible)
    {
        Attributes.UpdateVisibility(visible);
        AttackBubble.UpdateVisibility(visible);
        DefenseBubble.UpdateVisibility(visible);
    }
}
