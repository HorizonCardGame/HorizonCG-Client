﻿using System;
using System.Collections.Generic;
using System.Linq;
using TCG.Enums;
using UnityEngine;
using UnityEngine.UI;
using Attribute = TCG.Enums.Attribute;

public class AttributeController : MonoBehaviour
{
    public Image Attribute0;
    public Image Attribute1;
    public Image Attribute2;
    public Image Attribute3;
    public Image Attribute4;
    public Image Attribute5;

    private Image _CurrentAttribute;
    private int _current = 0;
    private List<Attribute> _attributes { get; set; }

    public void UpdateWith(List<Attribute> attributes)
    {
        _attributes = attributes;
        _current = 0;
        foreach(var attribute in _attributes)
        {
            if (attribute == Attribute.VIGOR)
            {
                SetCurrentToNext(GetSprite(Attribute.VIGOR));
            }
            else if (attribute == Attribute.UNITY)
            {
                SetCurrentToNext(GetSprite(Attribute.UNITY));
            }
            else if (attribute == Attribute.WRATH)
            {
                SetCurrentToNext(GetSprite(Attribute.WRATH));
            }
            else if (attribute == Attribute.DECAY)
            {
                SetCurrentToNext(GetSprite(Attribute.DECAY));
            }
            else if (attribute == Attribute.PENANCE)
            {
                SetCurrentToNext(GetSprite(Attribute.PENANCE));
            }
            else if (attribute == Attribute.AVARICE)
            {
                SetCurrentToNext(GetSprite(Attribute.AVARICE));
            }
        }
    }

    public void UpdateVisibility(bool visible)
    {
        SetUpAttributes();
        if(_attributes != null && _attributes.Any())
        {
            if (_attributes.Count > 0)
            {
                Attribute0.enabled = visible;
            }
            if (_attributes.Count > 1)
            {
                Attribute1.enabled = visible;
            }
            if (_attributes.Count > 2)
            {
                Attribute2.enabled = visible;
            }
            if (_attributes.Count > 3)
            {
                Attribute3.enabled = visible;
            }
            if (_attributes.Count > 4)
            {
                Attribute4.enabled = visible;
            }
            if (_attributes.Count > 5)
            {
                Attribute5.enabled = visible;
            }
        }
    }

    private void SetUpAttributes()
    {
        Attribute0.enabled = false;
        Attribute1.enabled = false;
        Attribute2.enabled = false;
        Attribute3.enabled = false;
        Attribute4.enabled = false;
        Attribute5.enabled = false;
    }

    private void SetCurrentToNext(Sprite sprite)
    {
        if(_current == 0 && Attribute0 != null)
        {
            Attribute0.sprite = sprite;
            Attribute0.enabled = true;
        }
        else if(_current == 1 && Attribute1 != null)
        {
            Attribute1.sprite = sprite;
            Attribute1.enabled = true;
        }
        else if (_current == 2 && Attribute2 != null)
        {
            Attribute2.sprite = sprite;
            Attribute2.enabled = true;
        }
        else if (_current == 3 && Attribute3 != null)
        {
            Attribute3.sprite = sprite;
            Attribute3.enabled = true;
        }
        else if (_current == 4 && Attribute4 != null)
        {
            Attribute4.sprite = sprite;
            Attribute4.enabled = true;
        }
        else if (Attribute5 != null)
        {
            Attribute5.sprite = sprite;
            Attribute5.enabled = true;
        }
        _current++;
    }

    private void SetCurrentVisibleToNext(bool visible)
    {
        if (_current == 0)
        {
            Attribute0.enabled = visible;
        }
        else if (_current == 1)
        {
            Attribute1.enabled = visible;
        }
        else if (_current == 2)
        {
            Attribute2.enabled = visible;
        }
        else if (_current == 3)
        {
            Attribute3.enabled = visible;
        }
        else if (_current == 4)
        {
            Attribute4.enabled = visible;
        }
        else
        {
            Attribute5.enabled = visible;
        }
        _current++;
    }

    public static Sprite GetSprite(Attribute at)
    {
        string stringAT;

        switch (at)
        {
            case Attribute.VIGOR:
                stringAT = "Vigor Symbol alternate";
                break;
            case Attribute.UNITY:
                stringAT = "Unity Symbol Alternate";
                break;
            case Attribute.WRATH:
                stringAT = "Wrath Symbol Alternate";
                break;
            case Attribute.DECAY:
                stringAT = "Decay Symbol Alternate";
                break;
            case Attribute.PENANCE:
                stringAT = "Pentance Symbol Alternate";
                break;
            case Attribute.AVARICE:
                stringAT = "Avarice Symbol Alternate";
                break;
            default:
                throw new ArgumentException();
        }

        return Resources.Load<Sprite>(string.Format("Images/CardComponents/Icons/{0}", stringAT));
    }

    public void Rescale(Vector3 newScale)
    {
        throw new NotImplementedException();
    }
}