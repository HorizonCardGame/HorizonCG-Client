﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TCG.Enums;
using System.Linq;
using System.Collections.Generic;

public class CardTopBarController : MonoBehaviour {

    public CircleBubbleController CardCost;
    public CText CardText;
    public Image CardFrameBorder;

    public void Rescale(Vector3 newScale)
    {
        CardText.Resize(newScale);
        CardCost.Rescale(newScale);
    }

    public void UpdateWith(int cost, string name, List<Attribute> attributes)
    {
        CardCost.UpdateNumber(cost);
        
        CardText.UpdateText(name);
        CardFrameBorder.sprite = GetBorderGraphic(attributes);
    }

    public void UpdateVisibility(bool visible)
    {
        CardCost.UpdateVisibility(visible);
        CardText.enabled = visible;
        CardFrameBorder.enabled = visible;
    }

    private Sprite GetBorderGraphic(List<Attribute> attributes)
    {
        var stringAT = ""; var numberOfAttr = 0;

        if (attributes.Contains(Attribute.VIGOR))
        {
            stringAT = "TypeLine_vigor-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.UNITY))
        {
            stringAT = "TypeLine_unity-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.WRATH))
        {
            stringAT = "TypeLine_wrath-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.DECAY))
        {
            stringAT = "TypeLine_decay-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.PENANCE))
        {
            stringAT = "TypeLine_penance-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.AVARICE))
        {
            stringAT = "TypeLine_avarice-02";
            numberOfAttr++;
        }

        if (numberOfAttr > 1)
        {
            stringAT = "TypeLine_gold-02";
        }
        else if (string.IsNullOrEmpty(stringAT))
        {
            stringAT = "TypeLine_neutral-02";
        }

        return Resources.Load<Sprite>(string.Format("Images/CardComponents/{0}", stringAT));
    }
}
