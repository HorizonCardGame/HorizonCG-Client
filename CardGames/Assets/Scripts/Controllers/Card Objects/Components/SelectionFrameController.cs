﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectionFrameController : MonoBehaviour
{
    public Image Frame;

    // Use this for initialization
    void Start()
    {
        SetOff();
    }

    public void SetAttacking()
    {
        SetColor(Colors.HexToColor(Constants.AttackColor));
    }

    public void SetSelected()
    {
        SetColor(Colors.HexToColor(Constants.SelectedArrowColor));
    }

    public void SetSelectable()
    {
        SetColor(Colors.HexToColor(Constants.SelectableColor));
    }

    public void SetMouseOver()
    {
        SetColor(Colors.HexToColor(Constants.DefaultMouseOverColor));
    }

    public void SetSpecialActivated()
    {
        SetColor(Colors.HexToColor(Constants.SpecialActivatedColor));
    }

    public void SetOff()
    {
        SetColor(new Color(0, 0, 0, 0));
    }

    private void SetColor(Color color)
    {
        Frame.color = color;
    }
}
