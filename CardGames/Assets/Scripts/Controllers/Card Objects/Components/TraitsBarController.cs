﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraitsBarController : MonoBehaviour {

    public TraitController TraitPrefab;
    
    public void UpdateTraitList(List<Traits> traits)
    {
        foreach(var trait in traits)
        {
            TraitController traitController = GameMaster.Instance.Instantiate(TraitPrefab.gameObject, this.transform).GetComponent<TraitController>();
            traitController.UpdateSpriteIcon(trait);
        }
    }
}
