﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TraitController : MonoBehaviour {

    public Image Sprite;

	public void UpdateSpriteIcon(Traits trait)
    {
        Sprite.sprite = Resources.Load<Sprite>(string.Format("Images/CardComponents/Icons/Traits/{0}", GetSpriteString(trait)));
    }

    private string GetSpriteString(Traits trait)
    {
        switch (trait)
        {
            case Traits.Rush:
            case Traits.Flying:
                return "Icons - Flying-02.png";
            default:
                throw new System.Exception("Can't add a non-existent Trait");
        }
    }
}

public enum Traits
{
    Flying,
    Rush
}
