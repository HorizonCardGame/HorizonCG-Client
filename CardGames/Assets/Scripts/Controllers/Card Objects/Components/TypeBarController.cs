﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TCG.Enums;

public class TypeBarController : MonoBehaviour, ICardPieceController {

    public Text Typeline;
    public Image Rarity;

    public void UpdateFromCard(Card card)
    {
        Typeline.text = card.TypeLine();
        Rarity.color = ResolveRarity(card.Rarity);
    }

    public Color ResolveRarity(CardRarity rarity)
    {
        switch (rarity)
        {
            case CardRarity.White:
                return new Color(255, 255, 255);
            case CardRarity.Green:
                return new Color(61, 210, 11);
            case CardRarity.Blue:
                return new Color(47, 120, 255);
            case CardRarity.Purple:
                return new Color(145, 50, 200);
            case CardRarity.Orange:
                return new Color(255, 150, 0);
            case CardRarity.Violet:
                return new Color(255, 0, 255);
            case CardRarity.Pink:
                return new Color(255, 105, 180);
            case CardRarity.Pearlescent:
                return new Color(0, 255, 255);
            default:
                return Color.black;
        }
    }

    public void UpdateVisibility(bool visible)
    {
        Rarity.enabled = visible;
        Typeline.enabled = visible;
    }

    public void Rescale(Vector3 newScale)
    {
        throw new System.NotImplementedException();
    }
}
