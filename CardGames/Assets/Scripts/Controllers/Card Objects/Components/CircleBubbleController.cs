﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CircleBubbleController : MonoBehaviour {

    public Image Image;
    public CText Text;

    public void Rescale(Vector3 newScale)
    {
        Text.Resize(newScale);
    }

    public void UpdateNumber(int number)
    {
        if(number < 0)
        {
            UpdateVisibility(false);
            return;
        }

        Text.UpdateText(number.ToString());
    }

    public void UpdateImage(Image img, Color clr)
    {
        Image = img;
        Image.color = clr;
    }
    public void UpdateImage(Image img)
    {
        UpdateImage(img, Image.color);
    }

    public void UpdateVisibility(bool visible)
    {
        Image.enabled = visible;
        Text.enabled = visible;
    }
}
