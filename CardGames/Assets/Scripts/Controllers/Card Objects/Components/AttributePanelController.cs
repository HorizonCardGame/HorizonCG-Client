﻿using System.Collections;
using System.Collections.Generic;
using TCG.Enums;
using UnityEngine;

public class AttributePanelController : BaseUIController
{
    public AttributeNodeController AttributeNode;
    public float OffsetAmount = 31f;
    public bool ExpandFromTheRight = false;
    private string _customTextHolderName = "AttributeNodeHolder";

    public void Construct(Dictionary<Attribute, int> dict)
    {
        GameMaster.Instance.DestroyChildren(transform);

        var offset = (ExpandFromTheRight) ? rectTransform.GetWidth() - OffsetAmount : 0f;
        var attributeList = Constants.AttributesOrder;
        if (ExpandFromTheRight) attributeList.Reverse();
        foreach(var key in attributeList)
        {
            var amount = dict[key];
            if(amount > 0)
            {
                var obj = GameMaster.Instance.Instantiate(AttributeNode.gameObject, transform);
                var node = obj.GetComponent<AttributeNodeController>();
                node.transform.localPosition = new Vector3(offset, 0);
                node.Contructor(key, amount);
                offset += (ExpandFromTheRight) ? -OffsetAmount : OffsetAmount;
            }
        }
    }
}
