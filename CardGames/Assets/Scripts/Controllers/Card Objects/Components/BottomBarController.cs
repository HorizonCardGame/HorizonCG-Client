﻿using System.Collections;
using System.Collections.Generic;
using TCG.Enums;
using UnityEngine;
using UnityEngine.UI;

public class BottomBarController : MonoBehaviour {

    public Image BorderBar;
    public CText CollectorsInformation;
    public CText Artist;
    public Image Rarity;

    public void UpdateWith(List<Attribute> attributes, CardRarity rarity, string collectorsInfo, string artist)
    {
        BorderBar.sprite = GetBorderGraphic(attributes);
        Rarity.color = ResolveRarity(rarity);
        CollectorsInformation.UpdateText(collectorsInfo);
        Artist.UpdateText(artist);
    }

    public void UpdateVisibility(bool visible)
    {
        BorderBar.enabled = visible;
        CollectorsInformation.enabled = visible;
        Artist.enabled = visible;
        Rarity.enabled = visible;
    }

    public Color ResolveRarity(CardRarity rarity)
    {
        switch (rarity)
        {
            case CardRarity.White:
                return Colors.HexToColor(Constants.RarityCommon);
            case CardRarity.Green:
                return Colors.HexToColor(Constants.RarityUncommon);
            case CardRarity.Blue:
                return Colors.HexToColor(Constants.RarityRare);
            case CardRarity.Purple:
                return Colors.HexToColor(Constants.RarityPromo);
            case CardRarity.Orange:
                return Colors.HexToColor(Constants.RaritySuperRare);
            case CardRarity.Violet:
                return Colors.HexToColor(Constants.RarityViolet);
            case CardRarity.Pink:
                return Colors.HexToColor(Constants.RarityPink);
            case CardRarity.Pearlescent:
                return Colors.HexToColor(Constants.RarityPearlescent);
            default:
                return Color.black;
        }
    }

    private Sprite GetBorderGraphic(List<Attribute> attributes)
    {
        var stringAT = ""; var numberOfAttr = 0;

        if (attributes.Contains(Attribute.VIGOR))
        {
            stringAT = "TypeLine_vigor-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.UNITY))
        {
            stringAT = "TypeLine_unity-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.WRATH))
        {
            stringAT = "TypeLine_wrath-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.DECAY))
        {
            stringAT = "TypeLine_decay-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.PENANCE))
        {
            stringAT = "TypeLine_penance-02";
            numberOfAttr++;
        }
        if (attributes.Contains(Attribute.AVARICE))
        {
            stringAT = "TypeLine_avarice-02";
            numberOfAttr++;
        }

        if (numberOfAttr > 1)
        {
            stringAT = "TypeLine_gold-02";
        }
        else if (string.IsNullOrEmpty(stringAT))
        {
            stringAT = "TypeLine_neutral-02";
        }

        return Resources.Load<Sprite>(string.Format("Images/CardComponents/{0}", stringAT));
    }

    public void Rescale(Vector3 newScale)
    {
        CollectorsInformation.Resize(newScale);
        Artist.Resize(newScale);
    }
}
