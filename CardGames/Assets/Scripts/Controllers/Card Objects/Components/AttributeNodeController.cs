﻿using System.Collections;
using System.Collections.Generic;
using TCG.Enums;
using UnityEngine;
using UnityEngine.UI;

public class AttributeNodeController : MonoBehaviour {

    public CText Number;
    public Image Image;

    public void Contructor(Attribute attribute, int amount)
    {
        Image.sprite = AttributeController.GetSprite(attribute);
        Number.UpdateText(amount.ToString());
    }
}
