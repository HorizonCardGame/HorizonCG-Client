﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TCG.Enums;
using Attribute = TCG.Enums.Attribute;

public class ActionVisualController : BaseCardVisual
{
    private MouseActions _clickable { get; set; }
    private Sprite _currentCardFace { get; set; }
    private bool _isMousedOver { get; set; }

    public BaseAction Action { get; private set; }
    public string Name { get; private set; }
    public string Text { get; private set; }
    public List<Attribute> Attributes { get; private set; }

    private TargettingSystemInfo _targetingSystemInfo { get; set; }

    void Start()
    {
        _clickable = GetComponent<MouseActions>();
        _clickable.OnMouseEnter += OnPointerEnter;
        _clickable.OnMouseExit += OnPointerExit;
        _clickable.OnRightClick += OnRightClick;
        _isMousedOver = false;
    }

    public void Constructor(BaseAction action, string name, string text, List<Attribute> attrs)
    {
        this.Action = action;
        this.Name = name;
        this.Text = text;
        this.Attributes = attrs;

        UpdateCardState();
    }

    public void UpdateCardState()
    {
        if (!string.IsNullOrEmpty(Action.Card.GetCard().CardSetId))
        {
            _currentCardFace = CardDatabase.Instance.GetCardImage(Action.Card.GetCard().CardSetId);
        }

        UpdateVisualState();
    }

    public void UpdateVisualState()
    {
        CardFace.sprite = _currentCardFace;

        MiddleSection.UpdateWith(Attributes, -1, DamageTypes.Shadow, -1);
        TopBar.UpdateWith(-1, Name, Attributes);
        TextArea.UpdateWith("Action", Text, Attributes);
        UpdateCardSectionVisiblities(true);

        if (_isMousedOver)
        {
            if (ShouldBeTarget)
            {
                SelectionFrame.SetSelected();
            }
            else
            {
                SelectionFrame.SetSelectable();
            }
        }
        else
        {
            if (ShouldBeTarget)
            {
                SelectionFrame.SetSelectable();
            }
            else
            {
                SelectionFrame.SetOff();
            }
        }
    }

    public void UpdateCardSectionVisiblities(bool visible, bool andCardImage = false)
    {
        MiddleSection.UpdateVisibility(visible);
        TopBar.UpdateVisibility(visible);
        TextArea.UpdateVisibility(visible);

        CardFrame.enabled = visible;
        CardFace.enabled = (!andCardImage) ? true : visible;
    }

    #region User Interactions

    private void OnPointerEnter(PointerEventData pointerEventData)
    {
        _isMousedOver = true;

        if (ShouldBeTarget)
        {
            _targetingSystemInfo.TargetingCallback(this, TargettingSystemCallbacks.Entered);
        }
        UpdateVisualState();
    }

    private void OnPointerExit(PointerEventData pointerEventData)
    {
        _isMousedOver = false;

        if (ShouldBeTarget)
        {
            _targetingSystemInfo.TargetingCallback(this, TargettingSystemCallbacks.Exited);
        }
        UpdateVisualState();
    }

    private void OnRightClick(PointerEventData pointerEventData)
    {
        //var items = new List<RightClickItem>();
        //items.Add(new RightClickItem { Text = "View Originating Card", ClickAction = (PointerEventData data) => { ShowCard(); }, Enabled = true });

        //Publish(EventNames.ShowOptionsMenu, new OptionsMenuEventData
        //{
        //    ClickPosition = pointerEventData.position,
        //    Items = items
        //});
    }
    #endregion

    public override Card GetCard()
    {
        return Action.Card.GetCard();
    }
}
