﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BaseCardVisual : BaseBehaviour, ICard
{
    public Image CardFrame;
    public Image CardFace;
    public MiddleSectionController MiddleSection;
    public TextAreaController TextArea;
    public CardTopBarController TopBar;
    public SelectionFrameController SelectionFrame;

    public virtual Card GetCard()
    {
        throw new System.NotImplementedException();
    }
}
