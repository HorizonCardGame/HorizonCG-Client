﻿using System.Collections;
using System.Collections.Generic;
using TCG.Utility;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(MouseActions))]
public class BasicCardController : MonoBehaviour, ICardController {

    public Image CardFace;
    public Sprite CardBack;

	public Card Card { get; set; }
    public bool IsFaceUp { get; set; }
    public bool IsInPlay { get; set; }
    public bool IsExhausted { get; set; }
    public bool IsAttacking { get; set; }
    public BasePlayerController Owner { get; private set; }
    public BasePlayerController Controller { get; set; }
    
    private MouseActions _clickable { get; set; }

    void Start()
    {
        _clickable = GetComponent<MouseActions>();
        _clickable.OnLeftClick += OnClick;
        _clickable.OnDoubleClick += OnDoubleClick;
    }


    public void Constructor(Card card)
    {
        Card = card;
        UpdateCardFace();
    }
    public void Constructor(Card card, BasePlayerController owner, bool isFaceUp)
    {
        Owner = owner;
        Controller = owner;
        IsFaceUp = isFaceUp;
        Constructor(card);
    }

    public void PlayCard()
    {
        //OnPlayTrigger

        Owner.PlayCard(this);
        IsInPlay = true;
    }

    public void Destroy()
    {
        //OnDestroyed

        MoveToDiscard();
    }

    public void MoveToDiscard()
    {
        //OnMovedToDiscard

        IsInPlay = false;
        Owner.MoveCardToDiscard(this);
        RemoveGameObject();
    }

    public void UpdateCardFace()
    {
        if (IsFaceUp)
        {
            //CardFace.sprite = Card.CardImage;

            if (IsExhausted)
            {
                CardFace.color = Color.gray;
            }
            else if (IsAttacking)
            {
                CardFace.color = Color.red;
            }
            else
            {
                CardFace.color = Color.white;
            }
        }
        else
        {
            CardFace.sprite = CardBack;
        }
    }

    private void OnClick(PointerEventData pointerEventData)
    {
        if (IsInPlay && !IsExhausted && !IsAttacking)
        {
            Attack();
        }
        else
        {
            RemoveFromAttacking();
        }
    }

    private void OnDoubleClick(PointerEventData pointEventData)
    {
        PlayCard();
    }

    public void Attack()
    {
        if (!IsExhausted)
        {
            //Attacking Trigger
            IsAttacking = true;
            var targets = Controller.GetListOfValidDefenders();
            //Controller.SetAttacking(this, targets[0]);
            UpdateCardFace();
        }
    }

    public void RemoveFromAttacking()
    {
        IsAttacking = false;
        //Controller.RemovingAttacking(this);
        UpdateCardFace();
    }

    public void AttackComplete()
    {
        IsAttacking = false;
        Exhaust();
    }

    public void Exhaust()
    {
        IsExhausted = true;
        UpdateCardFace();
    }

    public void Recover()
    {
        IsExhausted = false;
        UpdateCardFace();
    }

    public void DealDamage(IDamageable target)
    {
        //deal damage trigger
        target.DealtDamage(this, Card.Attack, Card.DamageType);
    }

    public void DealtDamage(ICardController target, int damage)
    {
        throw new System.NotImplementedException();
    }


    public Card GetCard()
    {
        return Card;
    }

    public RectTransform GetRect()
    {
        return GetComponent<RectTransform>();
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public void RemoveGameObject()
    {
        Destroy(this.gameObject);
    }

    public void UpdateCardState()
    {
        throw new System.NotImplementedException();
    }

    public void Transform(Card target)
    {
        throw new System.NotImplementedException();
    }

    public void Revert()
    {
        throw new System.NotImplementedException();
    }

    public void AdjustStatistics(JSONNode statistics)
    {
        throw new System.NotImplementedException();
    }

    public void SetSizeByHeight(float height)
    {
        throw new System.NotImplementedException();
    }

    public void SetSizeByWidth(float width)
    {
        throw new System.NotImplementedException();
    }

    public void SetSize(float width, float height)
    {
        throw new System.NotImplementedException();
    }

    public void AddedToAHand()
    {
        throw new System.NotImplementedException();
    }
}
