﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum GameBoardStartingOwnships
{
    EqualSplit
}

public class GameBoardController : MonoBehaviour {

    public Transform[][] BoardPositions { get; set; }
    public int[,] CurrentOwnership { get; set; }

    public Vector2 MaxBoardPositions = new Vector2(8, 3);
    public GameBoardStartingOwnships GameStartingOwnership = GameBoardStartingOwnships.EqualSplit;
    public Vector2[] GameBoardStartingPositions = new Vector2[] { new Vector2(1, 1), new Vector2(6, 1) };
    public GameObject GamePawn;

    public Color[] PlayerColors;

	// Use this for initialization
	void Start () {
        var allBoardPositions = new List<Transform[]>();
        CurrentOwnership = new int[(int)MaxBoardPositions.x, (int)MaxBoardPositions.y];
        var columnOwnerSwitch = ChangingOwnershipStartingColumn();
        var currentOwner = 0;

        for(var i = 0; i < MaxBoardPositions.x; i++)
        {
            if(i == columnOwnerSwitch)
            {
                currentOwner++;
            }

            var depthPositions = new List<Transform>();
            for(var j = 0; j < MaxBoardPositions.y; j++)
            {
                CurrentOwnership[i, j] = currentOwner;
                var obj = GameObject.Find(i + "-" + j);

                if(obj != null)
                    depthPositions.Add(obj.transform);  
            }
            allBoardPositions.Add(depthPositions.ToArray());
        }

        BoardPositions = allBoardPositions.ToArray();

        foreach(var pos in GameBoardStartingPositions)
        {
            var position = BoardPositions[(int)pos.x][(int)pos.y].position;
            var obj = GameMaster.Instance.Instantiate(GamePawn.gameObject) as GameObject;
            obj.transform.rotation = Quaternion.identity;
            obj.transform.position = position;
        }
    }
	
    public int ChangingOwnershipStartingColumn()
    {
        switch (GameStartingOwnership)
        {
            case GameBoardStartingOwnships.EqualSplit:
                return ((int)MaxBoardPositions.x) / 2;
            default:
                return 0;
        }

    }
}
