﻿using System.Collections.Generic;
using UnityEngine;

public class BasicPlayerBoardController : MonoBehaviour
{
    public BasePlayerController Owner { get; private set; }

    private List<ICardController> _cardsOnBoard { get; set; }
    private bool _boardListNeedsUpdate { get; set; }

    public void Constructor(BasePlayerController owner)
    {
        Owner = owner;
        _cardsOnBoard = new List<ICardController>();
        _boardListNeedsUpdate = false;
    }

    public void Add(ICardController cc)
    {
        cc.GetTransform().parent = this.transform;
        _boardListNeedsUpdate = true;
    }

    public void Remove(ICardController cc)
    {
        var ccTransform = cc.GetTransform().transform;
        if (ccTransform.parent == this.transform)
        {
            ccTransform.parent = null;
            _boardListNeedsUpdate = true;
        }
    }

    public IEnumerable<ICardController> GetCardsOnBoard()
    {
        if (!_boardListNeedsUpdate)
        {
            return _cardsOnBoard;
        }

        var output = new List<ICardController>();

        foreach (Transform child in this.transform)
        {
            if (child == null)
            {
                continue;
            }

            var cardController = child.GetComponent<ICardController>();
            if(cardController != null)
            {
                output.Add(cardController);
            }
        }

        _cardsOnBoard = output;
        _boardListNeedsUpdate = false;

        return output;
    }

    public RectTransform GetRectTransfrom()
    {
        return GetComponent<RectTransform>();
    }
}