﻿using UnityEngine;
using System.Collections;
using TCG.Enums;
using System.Linq;
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using TCG.Utility;

public class ComputerPlayerController : BasePlayerController
{
    [SerializeField]
    private bool DebugPlayer = false;

    public float ComputerPlaySpeed = 1.5f;
    public MonobehaviorMouseActions MouseActions { get; set; }

    private ComputerStates _state { get; set; }
    private float _nextActionTime = 0f;
    private int _currentTurn = -1;

    public void Start()
    {
        MouseActions = GetComponent<MonobehaviorMouseActions>();
        PlayerHeader.HeroActions.OnRightClick += ShowOptionsMenu;
        PlayerHeader.HeroActions.OnMouseEnter += OnPointerEnter;
        PlayerHeader.HeroActions.OnMouseExit += OnPointerExit;
        MouseActions.OnRightClick += ShowOptionsMenu;
        MouseActions.OnMousePointerEnter += OnPointerEnter;
        MouseActions.OnMousePointerExit += OnPointerExit;
        Subscribe(EventNames.OnMainPhase, OnMainPhase, BaseId.ToString());
    }

    public override void OnStartOfTurn()
    {
        base.OnStartOfTurn();
        _state = ComputerStates.StartOfTurn;
    }

    public void FixedUpdate()
    {
        if (Time.time < _nextActionTime)
        {
            return;
        }

        if(DebugPlayer) Debug.LogFormat("{0} - {1}", PlayerName, _state);
        switch (_state)
        {
            case ComputerStates.StartOfTurn:
                //_state = ComputerStates.AetherizeCards;
                break;
            case ComputerStates.AetherizeCards:
                if (Hand.GetCardsInHand() > 0)
                {
                    if(CanAetherizeCards())
                    {
                        var card = Hand.GetCards().OrderByDescending(x => x.Cost).FirstOrDefault();
                        var cController = GameMaster.Instance.CreateCardController(card, this);
                        AetherizeCard(cController);
                    }
                }
                _state = ComputerStates.PlayCards;
                break;
            case ComputerStates.PlayCards:
                if(Hand.GetCardsInHand() > 0)
                {
                    var card = Hand.GetCards().OrderByDescending(x => x.Cost).Where(y => CanPlayCard(y)).FirstOrDefault();
                    if (card != null)
                    {
                        PlayCard(card);
                    }
                    else
                    {
                        MoveToCombat();
                    }
                }
                else
                {
                    MoveToCombat();
                }
                break;
            case ComputerStates.FinishedAttack:
                _state = ComputerStates.EndOfTurn;
                break;
            case ComputerStates.EndOfTurn:
                _state = ComputerStates.Idle;
                GameMaster.Instance.GetState<MainBattleState>().DoCurrentMainStateAction();
                break;
            default:
                break;
        }

        _nextActionTime = Time.time + ComputerPlaySpeed;
    }

    public void PlayCard(Card cc)
    {
        _state = ComputerStates.Idle;
        var cController = GameMaster.Instance.CreateCardController(cc, this);
        PlayCard(cController);
    }

    private void OnMainPhase(IBaseEventUser arg0, object arg1)
    {
        _currentTurn = GameMaster.Instance.CurrentTurn;
        _state = ComputerStates.AetherizeCards;
    }

    private void ShowOptionsMenu(PointerEventData data)
    {
        ShowOptionsMenu(data.position);
    }
    private void ShowOptionsMenu(Vector3 mousePosition)
    {
        var items = new List<RightClickItem>();
        items.Add(new RightClickItem { Text = "View Card", ClickAction = (PointerEventData data) => { ShowHero(); }, Enabled = true });

        Publish(EventNames.ShowOptionsMenu, new OptionsMenuEventData
        {
            ClickPosition = mousePosition,
            Items = items
        });
    }

    private void ShowHero()
    {
        Publish(EventNames.PreviewCard, _hero);
    }

    private void MoveToCombat()
    {
        _state = ComputerStates.Idle;

        GameMaster.Instance.GetState<MainBattleState>().DoCurrentMainStateAction();
    }

    #region DeclareAttacks
    private UnityAction<List<Tuple<IPawn, IDamageable>>> _attackingAction;
    public override void GetAttacking(UnityAction<List<Tuple<IPawn, IDamageable>>> callback)
    {
        var attackers = GetAllUnitsThatCanExhaust();
        
        //TODO: Battle Visuals
        callback(attackers.Select(x => new Tuple<IPawn, IDamageable>(x, GetListOfValidDefenders().FirstOrDefault())).ToList());
    }

    #endregion

    #region DeclareBlocks
    public override void AssignBlockers(UnityAction<List<Tuple<IPawn, IDamageable>>> callback)
    {
        _propestiveAttacks = GameMaster.Instance.CurrentPlayer.Attackers.Select(x => new Tuple<IPawn, IDamageable>(x.First, x.Second)).ToList();
        var availableBlockers = GetCardsOnBoard().Where(x => x.Card.Type.Contains(CardType.UNIT) && !x.IsExhausted).ToList();
        for(var i = 0; i < availableBlockers.Count(); i++)
        {
            _propestiveAttacks[i].Second = availableBlockers[i];
        }

        //TODO: Battle Visuals

        callback(_propestiveAttacks);
    }
    #endregion
}
