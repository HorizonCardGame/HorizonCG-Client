﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using TCG.Utility;
using UnityEngine.Events;
using TCG.Enums;
using UnityEngine.EventSystems;
using System;
using Attribute = TCG.Enums.Attribute;

public abstract class BasePlayerController : BaseBehaviour, IDamageable, IPlayerController, ICard
{
    public const int MASTER_HERO_INCREASE_IN_HEALTH = 15;

    public PlayerBoard Board;
    public HandController Hand;
    public BasePlayerHeaderController PlayerHeader;
    public CardOrbController CardOrbController;
    public TargetCircle TargetCircle;
    public PawnModelController Model;

    public string PlayerName = "Base Name";

    public List<Triple<IPawn, IDamageable, bool>> Attackers { get; set; }
    protected List<Tuple<IPawn, IDamageable>> _propestiveAttacks { get; set; }

    protected int _health { get; set; }
    protected int _armor { get; set; }
    protected int _aether { get; set; }
    protected int _maxAether { get { return Card.GetTotalAetherizationContributions(_cardsAetherized).First; } }
    protected List<Card> _cardsAetherized { get; set; }
    protected int _essense { get; set; }
    protected List<Card> _discard { get; set; }
    protected List<Card> _banishment { get; set; }
    protected BasicDeck _deck { get; set; }
    protected Card _hero { get; set; }
    protected Color _playerColor { get; set; }

    private bool _reinitializeUI = true;
    private bool _reactionTimerIsActive = false;
    private bool _isMousedOver = false;
    private bool _isAttacking = false;
    private bool _isBlocking = false;
    private int _numberOfAetherizedCardsPerTurn = Constants.DefaultNumberOfAetherizes;
    private int _cardsAetherizedThisTurn = 0;

    private TargettingSystemInfo _targetingSystemInfo { get; set; }

    public virtual void Constructor(Color color) {
        _cardsAetherized = new List<Card>();
        _discard = new List<Card>();
        _banishment = new List<Card>();

        _playerColor = color;
        Board.Constructor(this.transform.position, this.transform.rotation);

        InitializeDeck();
        InitializeMasterHero(_deck.MasterHero);

        Hand.Constructor(this);
        PlayerHeader.TotalPlayerTimer.Constructor(Time.time);

        UpdatePlayerUI();
        ClearAttackers();
        Subscribe(EventNames.UpdateUI, UpdateUI);
	}

    public void InitializeMasterHero(Card mh)
    {
        InitializeHero(mh);
        _health = MASTER_HERO_INCREASE_IN_HEALTH;
        _armor = mh.Defense;
        _essense = 0;
        _aether = 0;
    }

    #region Play Cards
    public void PlayCard(ICardController cc)
    {
        if (CanPlayCard(cc.GetCard()))
        {
            PayCost(cc.GetCard().Cost);
            Hand.RemoveCardFromHand(cc);
            GameMaster.Instance.AddToTheStack(new NonAbilityStackData { Card = cc.GetCard(), Caster = this });
            Publish(EventNames.PlayedCard, new PlayCardEventData { Card = cc.GetCard() });
            UpdatePlayerUI();
        }
        else
        {
            Publish(EventNames.DisplayError, Localization.CanNotPlayCard);
        }
    }

    public bool CanPlayCard(Card card)
    {
        if((_reactionTimerIsActive && !card.Type.Contains(CardType.FAST)) 
            || !(GameMaster.Instance.IsCurrentState<MainBattleState>() && GameMaster.Instance.CurrentPlayer == this))
        {
            return false;
        }

        //get if legal targets
        var validTargets = true;
        var additionalCosts = card.GetAdditionalCosts().Count == 0;

        return CanPayCost(card.Cost) && HasAttributeRequirements(Card.GetAttributesOfCard(card)) && validTargets && additionalCosts;
    }

    public bool HasAttributeRequirements(Dictionary<Attribute, int> requirements)
    {
        var aetherDict = Card.GetTotalAetherizationContributions(_cardsAetherized).Second;
        return !requirements.Any(attr => attr.Value > aetherDict[attr.Key]);
    }

    public bool CanPayCost(int cost)
    {
        return cost <= _aether;
    }

    public void PayCost(int cost)
    {
        _aether -= cost;
    }
    #endregion

    #region PlayerBoard
    public void AddCardToBoard(PawnController pawn)
    {
        Board.Add(pawn);
        pawn.EnterField();
    }

    public void RemoveCardFromBoard(PawnController pawn)
    {
        Board.Remove(pawn);
    }

    public List<PawnController> GetCardsOnBoard(TargetSelectionFilter filter = null)
    {
        if(filter == null)
        {
            return Board.GetAll().Select(x => (PawnController)x).ToList();
        }
        else
        {
            return Board.GetAll().Select(x => (PawnController)x)
                .Where(p => p.Card.IsApartOfTheFilter(filter)).ToList();
        }
    }

    public IEnumerable<PawnController> GetAllUnitsThatCanExhaust()
    {
        return GetCardsOnBoard().Where(x => x.CanExhaust() && x.Card.Type.Contains(CardType.UNIT));
    }
    #endregion

    #region Hand
    public List<Card> GetCardsInHand(TargetSelectionFilter filter = null)
    {
        if(filter == null)
        {
            return Hand.GetCards();
        }
        else
        {
            return Hand.GetCards()
                .Where(p => p.IsApartOfTheFilter(filter)).ToList();
        }
    }
    #endregion

    #region Aetherization
    public void AetherizeCard(ICardController cc)
    {
        if (CanAetherizeCards())
        {
            Hand.RemoveCardFromHand(cc);
            MoveToAetherize(cc.GetCard());
        }
        else
        {
            Publish(EventNames.DisplayError, Localization.CanNotAetherizeMore);
        }
    }

    public void MoveToAetherize(Card c)
    {
        _cardsAetherized.Add(c);
        var contributions = c.GetAetherizedContributions();
        _aether += contributions.First;
        _essense += contributions.Third;
        _cardsAetherizedThisTurn++;
        UpdatePlayerUI();
        EventManager.Publish(c, EventNames.Aetherized, null);
    }

    public bool CanAetherizeCards()
    {
        //TODO have a way of getting all the ways to increase how many cards you can aetherize each turn.
        return NumberOfCardsCanBeAetherized() > 0 && !_reactionTimerIsActive  
            && GameMaster.Instance.IsCurrentState<MainBattleState>() && GameMaster.Instance.CurrentPlayer == this;
    }

    public int NumberOfCardsCanBeAetherized()
    {
        return _numberOfAetherizedCardsPerTurn - _cardsAetherizedThisTurn;
    }

    public List<Card> GetCardsInAether(TargetSelectionFilter filter = null)
    {
        if (filter == null)
        {
            return _cardsAetherized;
        }
        else
        {
            return _cardsAetherized.Where(p => p.IsApartOfTheFilter(filter)).ToList();
        }
    }
    #endregion

    #region Combat
    public List<IDamageable> GetListOfValidDefenders()
    {
        return GameMaster.Instance.GetEnemyPlayers(this).ConvertAll<IDamageable>((input) => { return input as IDamageable; });
    }

    public virtual void GetAttacking(UnityAction<List<Tuple<IPawn, IDamageable>>> callback)
    {
        callback.Invoke(new List<Tuple<IPawn, IDamageable>>());
    }

    public void SetAttacking(IPawn attacker, IDamageable defender)
    {
        Attackers.Add(new Triple<IPawn, IDamageable, bool>(attacker, defender, false));
        //TargetCircle.SetAttacking();
    }

    public void RemovingAttacking(IPawn cc)
    {
        Triple<IPawn, IDamageable, bool> attacker = null;
        foreach(var triple in Attackers)
        {
            if(triple.First == cc)
            {
                attacker = triple;
                continue;
            }
        }

        if(attacker != null)
        {
            Attackers.Remove(attacker);
        }
    }

    public void EngageAttacks()
    {
        foreach(var attacks in Attackers)
        {
            attacks.First.DealDamage(attacks.Second);
            attacks.Third = true;
        }

        ClearAttackers();
    }

    public void ClearAttackers()
    {
        if (Attackers != null)
        {
            foreach (var attacks in Attackers)
            {
                if (attacks.Third)
                {
                    attacks.First.AttackComplete();
                }
            }
        }

        Attackers = new List<Triple<IPawn, IDamageable, bool>>();
    }

    public virtual void AssignBlockers(UnityAction<List<Tuple<IPawn, IDamageable>>> callback)
    {
        callback(new List<Tuple<IPawn, IDamageable>>());
    }

    public void SetBlocking(IDamageable defender, IPawn attacker)
    {
        Attackers.First(x => x.First.Equals(attacker)).Second = defender;
        UpdateVisualState();
    }

    #endregion

    public void UpdateUI(IBaseEventUser baseEventUser, object data)
    {
        UpdatePlayerUI();
    }

    protected void UpdatePlayerUI()
    {
        PlayerHeader.SetParameters(_health, _armor, _deck.GetNumberOfCardsInDeck(), _discard.Count, 
                                    _banishment.Count, Hand.GetCardsInHand(), _aether, _maxAether, 
                                    _essense, Card.GetTotalAetherizationContributions(_cardsAetherized).Second);
        CardOrbController.UpdateCardNumbers(Hand.GetCardsInHand());

        if(_reinitializeUI)
        {
            var sprite = CardDatabase.Instance.GetCardImage(_hero.BaseCardNameAndSet.Second);
            PlayerHeader.SetImageUI(sprite, _playerColor);
            _reinitializeUI = false;
        }
    }

    public void UpdateVisualState()
    {
        if (_isAttacking)
        {
            Model.SetAttacking();
            TargetCircle.SetAttacking();
        }
        else if (_isBlocking)
        {
            Model.SetBlocking();
            TargetCircle.SetBlocking();
        }
        else if (_isMousedOver)
        {
            if (ShouldBeTarget)
            {
                Model.SetSelected();
                TargetCircle.SetSelected();
            }
            else
            {
                Model.SetSelectable();
                TargetCircle.SetSelectable();
            }
        }
        else
        {
            if (_hero.HasSpecialActivation())
            {
                Model.SetSpecialActivated();
                TargetCircle.SetSpecialActivated();
            }
            else if (ShouldBeTarget)
            {
                Model.SetSelectable();
                TargetCircle.SetSelectable();
            }
            else
            {
                Model.SetOff();
                TargetCircle.SetOff();
            }
        }
    }

    #region Events

    public void DealtDamage(object source, int amount, DamageTypes dt)
    {
        //Damage reduction calculation
        PlayerHeader.ChangeInHealth(-amount);

        var damageAmount = amount - _armor;
        if (damageAmount < 0) _armor = -damageAmount;

        UpdatePlayerUI();
        if (damageAmount > 0)
        {
            LoseHealth(source, damageAmount, true);
        }

        Publish(EventNames.DealtDamage, new DealtDamageEventData { Source = source, Amount = amount }, BaseId.ToString());
    }

    public void LoseHealth(object source, int amount, bool fromDealtDamage = false)
    {
        _health -= amount;

        if(!fromDealtDamage)
            PlayerHeader.ChangeInHealth(-amount);

        UpdatePlayerUI();
        if(_health <= 0)
        {
            TriggerGameLoss();
        }

        Publish(EventNames.LostHealth, new LostHealthEventData { Source = source, Amount = amount }, BaseId.ToString());
    }

    public void GainHealth(object source, int amount, bool propagateTriggers = true)
    {
        _health += amount;

        PlayerHeader.ChangeInHealth(amount);

        UpdatePlayerUI();
        if(propagateTriggers)
            Publish(EventNames.GainHealth, new GainHealthEventData { Source = source, Amount = amount }, BaseId.ToString());
    }

    public void GainArmor(object source, int amount, bool propagateTriggers = true)
    {
        _armor += amount;
        UpdatePlayerUI();
        if(propagateTriggers)
            Publish(EventNames.GainArmor, new GainArmorEventData { Source = source, Amount = amount }, BaseId.ToString());
    }

    public void TriggerGameLoss()
    {
        Debug.Log("Should lose the game");
    }

    public void OnPointerEnter()
    {
        _isMousedOver = true;
        UpdateVisualState();
        if (ShouldBeTarget)
        {
            _targetingSystemInfo.TargetingCallback(this, TargettingSystemCallbacks.Entered);
        }
    }
    public void OnPointerEnter(PointerEventData data)
    {
        OnPointerEnter();
    }

    public void OnPointerExit()
    {
        _isMousedOver = false;
        UpdateVisualState();
        if (ShouldBeTarget)
        {
            _targetingSystemInfo.TargetingCallback(this, TargettingSystemCallbacks.Exited);
        }
    }
    public void OnPointerExit(PointerEventData data)
    {
        OnPointerExit();
    }

    public virtual void OnBeginningOfGame()
    {
        DrawCards(7);
    }

    public virtual void OnStartOfTurn()
    {
        _cardsAetherizedThisTurn = 0;
        _aether = _maxAether;
        var playboard = Board.GetAll();
        foreach (PawnController cc in playboard)
        {
            cc.OnStartOfYourTurn();
        }

        TargetCircle.SetOff();
        DrawCard();
        PlayerHeader.TotalPlayerTimer.Run();
    }

    public virtual void OnEndOfTurn()
    {
        PlayerHeader.TotalPlayerTimer.Pause();
    }

    #endregion

    public virtual void OnPausedReactionTimer()
    {
       PlayerHeader.TotalPlayerTimer.Run();
    }

    public virtual void StartReactionTimer(UnityAction callback)
    {
        _reactionTimerIsActive = true;
        PlayerHeader.ReactionTimer.OnFinished = () => { _reactionTimerIsActive = false; callback(); };
        PlayerHeader.ReactionTimer.StartTimer();
    }

    public virtual bool IsCurrentPlayersTurn()
    {
        return GameMaster.Instance.CurrentPlayer != null && BaseId == GameMaster.Instance.CurrentPlayer.BaseId;
    }

    #region Hero Actions
    public void InitializeHero(Card card)
    {
        _hero = card;
    }
    #endregion

    #region Deck Actions
    public void InitializeDeck()
    {
        _deck = new BasicDeck(TEST_DECKLIST, this);
        _deck.Shuffle();
    }

    public void DrawCard()
    {
        Publish(EventNames.DrawsCard, null, BaseId.ToString());

        var card = _deck.DrawCard();
        Hand.AddCardToHand(card);
        UpdatePlayerUI();
    }

    public void DrawCards(int numberOfCards)
    {
        for(var i = 0; i < numberOfCards; i++)
        {
            DrawCard();
        }
    }
    #endregion

    #region Discard Actions
    public void InitializeDiscard()
    {
        _discard = new List<Card>();
    }

    public void MoveCardToDiscard(ICardController cc)
    {
        Hand.RemoveCardFromHand(cc);
        MoveCardToDiscard(cc.GetCard());
    }

    public void MoveCardToDiscard(Card c)
    {
        _discard.Insert(0, c);
        Publish(EventNames.PutInDiscard, new PutInGraveyardData { Card = c, Position = Vector3.zero });
        UpdatePlayerUI();
    }

    public IEnumerable<Card> GetCardsInDiscard()
    {
        return _discard;
    }

    public void DiscardCardsFromHand(int amount)
    {
        var targetArgs = new TargetSelectionFilter(
            null,
            "Choose a card in your hand to discard.",
            amount,
            new Vector3(0, 0, 0),
            Filters.CardsInHand
        );

        TargetSystem.BeginTargeting(targetArgs, false,
            (List<IBaseEventUser> bases, List<object> args) => {
                foreach (var i in bases)
                {
                    var card = (CardController)i;
                    MoveCardToDiscard(card);
                }
            },
            () => { });
    }

    public void DiscardACardFromHand()
    {
        DiscardCardsFromHand(1);
    }
    #endregion

    #region Banish Actions
    public void MoveCardToBanished(Card c)
    {
        _banishment.Insert(0, c);
        Publish(EventNames.Banished, new BanishedData { Card = c });
        UpdatePlayerUI();
    }

    #endregion

    #region DebugPanelActions

    public void DestroyAllUnits()
    {
        var playboard = Board.GetAll().Select(x => (PawnController)x).ToList();
        for (var i = 0; i < playboard.Count(); i++)
        {
            var controller = playboard[i];
            if (controller.GetCard().Type.Contains(CardType.UNIT))
            {
                controller.Destroy();
            }
        }
    }

    public void GainOneHealth()
    {
        GainHealth(this, 1);
    }

    public void LoseOneHealth()
    {
        LoseHealth(this, 1);
    }

    #endregion

    #region Accessors

    public override void SetTargetingInfo(IBaseEventUser sender, TargettingSystemInfo targettingInfo)
    {
        if (!targettingInfo.Filter.IsTarget(this))
            return;

        ShouldBeTarget = true;
        _targetingSystemInfo = targettingInfo;
    }

    public Card GetCard()
    {
        return _hero;
    }

    public Transform GetTransform()
    {
        return transform;
    }

    #endregion

    public const string TEST_DECKLIST = "{" +
    "\"MASTER HERO\": [" +
    "{\"Number\":1, \"Name\":\"Blobica, Heroine of Blob\", \"Set\":\"TST\"}" +
    "]," +
    "\"MAINDECK\": [" +
    "{\"Number\":30, \"Name\":\"Arrive Test\", \"Set\":\"TST\"}," +
    "{\"Number\":30, \"Name\":\"Gravecall Test\", \"Set\":\"TST\"}," +
    "]," +
    "\"SIDEDECK\": []" +
    "}";
}
