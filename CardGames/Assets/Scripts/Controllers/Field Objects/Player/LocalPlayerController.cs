﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using TCG.Enums;
using System;
using System.Collections.Generic;
using TCG.Utility;
using UnityEngine.Events;
using System.Linq;

public class LocalPlayerController : BasePlayerController
{
    public MonobehaviorMouseActions MouseActions { get; set; }

    public void Start()
    {
        MouseActions = GetComponent<MonobehaviorMouseActions>();
        PlayerHeader.DiscardActions.OnLeftClick += DiscardClicked;
        PlayerHeader.BanishedActions.OnLeftClick += BanishedClicked;
        //PlayerHeader.HeroActions.OnLeftClick += MasterHeroClicked;
        PlayerHeader.HeroActions.OnRightClick += ShowOptionsMenu;
        PlayerHeader.HeroActions.OnMouseEnter += OnPointerEnter;
        PlayerHeader.HeroActions.OnMouseExit += OnPointerExit;
        MouseActions.OnRightClick += ShowOptionsMenu;
        MouseActions.OnMousePointerEnter += OnPointerEnter;
        MouseActions.OnMousePointerExit += OnPointerExit;
    }

    public void DiscardClicked(PointerEventData pointerEventData)
    {
        Publish(EventNames.ShowOtherZone, new OtherCardZoneEventData
        {
            Zone = ZoneTypes.DISCARD,
            Cards = _discard
        });
    }

    public void BanishedClicked(PointerEventData pointerEventData)
    {
        Publish(EventNames.ShowOtherZone, new OtherCardZoneEventData
        {
            Zone = ZoneTypes.BANISHED,
            Cards = _banishment
        });
    }

    public void MasterHeroClicked(PointerEventData pointerEventData)
    {
        if(!GameMaster.Instance.IsCurrentState<TargettingState>())
        {
            ShowHero();
        }
    }

    public void AetherizedCardsClicked(PointerEventData pointerEventData)
    {

    }

    private void ShowHero()
    {
        Publish(EventNames.PreviewCard, _hero);
    }

    private void ShowOptionsMenu(PointerEventData data)
    {
        ShowOptionsMenu(data.position);
    }
    private void ShowOptionsMenu(Vector3 mousePosition)
    {
        var items = new List<RightClickItem>();
        items.Add(new RightClickItem { Text = "View Card", ClickAction = (PointerEventData data) => { ShowHero(); }, Enabled = true });

        //var options = Card.Actions.Where(x => x.HasOptionMenuItem(CardZones.Field));
        //if (options.Any())
        //{
        //    foreach (var opt in options)
        //    {
        //        items.Add(opt.GetRightClickItem());
        //    }
        //}

        Publish(EventNames.ShowOptionsMenu, new OptionsMenuEventData
        {
            ClickPosition = mousePosition,
            Items = items
        });
    }

    #region DeclareAttacks
    private UnityAction<List<Tuple<IPawn, IDamageable>>> _attackingAction;
    public override void GetAttacking(UnityAction<List<Tuple<IPawn, IDamageable>>> callback)
    {
        _attackingAction = callback;
        _propestiveAttacks = new List<Tuple<IPawn, IDamageable>>();
        var attackers = GetAllUnitsThatCanExhaust();
        foreach(var unit in attackers)
        {
            unit.SetVisualState(VisualStates.Selectable);
            //TODO : Make this work for multiplayer
            unit.ChangeOnClick((IPawn pawn) => {
                if(_propestiveAttacks.Any(x => x.First == pawn))
                {
                    _propestiveAttacks.Remove(_propestiveAttacks.Find(y => y.First == pawn));
                }
                else {
                    pawn.SetVisualState(VisualStates.Selected);
                    _propestiveAttacks.Add(new Tuple<IPawn, IDamageable>(pawn, GetListOfValidDefenders().FirstOrDefault()));
                }

                if (!_propestiveAttacks.Any())
                {
                    GameMaster.Instance.ChangeButtonLook("Skip Attacks",
                        Colors.HexToColor("#704040FF"),
                        Colors.HexToColor("#A04040FF"),
                        Colors.HexToColor("#404040FF"),
                        ConfirmAttacks);
                }
                else
                {
                    GameMaster.Instance.ChangeButtonLook(string.Format("Declare {0} attacks",_propestiveAttacks.Count),
                        Colors.HexToColor("#904040FF"),
                        Colors.HexToColor("#A04040FF"),
                        Colors.HexToColor("#404040FF"),
                        ConfirmAttacks);
                }

                //TODO: Battle Visuals
            });
        }
        GameMaster.Instance.ChangeButtonLook("Skip Attacks",
            Colors.HexToColor("#704040FF"),
            Colors.HexToColor("#A04040FF"),
            Colors.HexToColor("#404040FF"),
            ConfirmAttacks);
    }

    private void ConfirmAttacks()
    {
        _attackingAction.Invoke(_propestiveAttacks);
        _attackingAction = null;
    }
    #endregion

    #region DeclareBlocks
    private UnityAction<List<Tuple<IPawn, IDamageable>>> _blockingAction;
    public override void AssignBlockers(UnityAction<List<Tuple<IPawn, IDamageable>>> callback)
    {
        _blockingAction = callback;
        _propestiveAttacks = GameMaster.Instance.CurrentPlayer.Attackers.Where(w => ((IBaseEventUser)w.Second).GetId() == GetId())
                                    .Select(x => new Tuple<IPawn, IDamageable>(x.First, x.Second)).ToList();
        var availableBlockers = GetCardsOnBoard().Where(x => x.Card.Type.Contains(CardType.UNIT) && !x.IsExhausted);
        foreach (var unit in availableBlockers)
        {
            unit.SetVisualState(VisualStates.Selectable);
            //TODO : Make this work for multiplayer
            unit.ChangeOnClick((IPawn pawn) => {
                if (_propestiveAttacks.Any(x => x.First == pawn))
                {
                    _propestiveAttacks.Remove(_propestiveAttacks.Find(y => y.First == pawn));
                    var newList = GameMaster.Instance.CurrentPlayer.Attackers.Select(x => new Tuple<IPawn, IDamageable>(x.First, x.Second)).ToList();
                    _propestiveAttacks.Add(newList.Find(y => y.First == pawn));
                    pawn.SetVisualState(VisualStates.Selectable);

                    if (!_propestiveAttacks.Where(w => ((IBaseEventUser)w.Second).GetId() != GetId()).Any())
                    {
                        GameMaster.Instance.ChangeButtonLook("No Blocks",
                            Colors.HexToColor("#704040FF"),
                            Colors.HexToColor("#A04040FF"),
                            Colors.HexToColor("#404040FF"),
                            ConfirmBlocks);
                    }
                    else
                    {
                        GameMaster.Instance.ChangeButtonLook("Declare Blocks",
                                                        Colors.HexToColor("#904040FF"),
                                                        Colors.HexToColor("#A04040FF"),
                                                        Colors.HexToColor("#404040FF"),
                                                        ConfirmBlocks);
                    }

                    GameMaster.Instance.ConstructBattleArcs(_propestiveAttacks);
                }
                else
                {
                    pawn.SetVisualState(VisualStates.Selected);
                    TargetSystem.BeginTargeting(new TargetSelectionFilter((ICard c) => {
                                                    return _propestiveAttacks.Any(x => x.First.GetCard().GetId() == c.GetCard().GetId());
                                                }, new List<object> { pawn }),
                                                false, 
                                                (List<IBaseEventUser> list, List<object> arguments) => {

                                                    var p = (IDamageable)arguments.FirstOrDefault();
                                                    if(p != null)
                                                    {
                                                        foreach (var eventUser in list)
                                                        {
                                                            var found = _propestiveAttacks.FirstOrDefault(x => ((IBaseEventUser)x.First).GetId() == eventUser.GetId());
                                                            found.Second = p;
                                                        }
                                                    }
                                                    unit.SetVisualState(VisualStates.Selected);
                                                    GameMaster.Instance.ChangeButtonLook("Declare Blocks",
                                                        Colors.HexToColor("#904040FF"),
                                                        Colors.HexToColor("#A04040FF"),
                                                        Colors.HexToColor("#404040FF"),
                                                        ConfirmBlocks);

                                                    GameMaster.Instance.ConstructBattleArcs(_propestiveAttacks);
                                                }, () => {
                                                    unit.SetVisualState(VisualStates.Selectable);
                                                    if (!_propestiveAttacks.Where(w => ((IBaseEventUser)w.Second).GetId() != GetId()).Any())
                                                    {
                                                        GameMaster.Instance.ChangeButtonLook("No Blocks",
                                                            Colors.HexToColor("#704040FF"),
                                                            Colors.HexToColor("#A04040FF"),
                                                            Colors.HexToColor("#404040FF"),
                                                            ConfirmBlocks);
                                                    }
                                                    else
                                                    {
                                                        GameMaster.Instance.ChangeButtonLook("Declare Blocks",
                                                                                        Colors.HexToColor("#904040FF"),
                                                                                        Colors.HexToColor("#A04040FF"),
                                                                                        Colors.HexToColor("#404040FF"),
                                                                                        ConfirmBlocks);
                                                    }

                                                    GameMaster.Instance.ConstructBattleArcs(_propestiveAttacks);
                                                });
                }
            });
        }
        GameMaster.Instance.ChangeButtonLook("No Blocks",
            Colors.HexToColor("#704040FF"),
            Colors.HexToColor("#A04040FF"),
            Colors.HexToColor("#404040FF"),
            ConfirmBlocks);
    }

    private void ConfirmBlocks()
    {
        _blockingAction(_propestiveAttacks);
        _blockingAction = null;
    }
    #endregion
}
