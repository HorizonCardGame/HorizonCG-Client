﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroController : MonoBehaviour {

    public Image CardFace;
    public Sprite CardBack;
    public Hero Hero { get; set; }

    public void Constructor(Card card)
    {
        Hero = new Hero(card);

        UpdateCardFace();
    }

    public void PlayCard()
    {
        throw new System.NotImplementedException();
    }

    public void UpdateCardFace()
    {
        //CardFace.sprite = Hero.BaseCard.CardImage; //TODO
    }
}
