﻿using System;
using TCG.Enums;
using UnityEngine;
using UnityEngine.UI;

public class PawnAtkDefController : MonoBehaviour, ICardPieceController
{
    public Text Number;
    public Image Icon;
    public Image BackSquare;

    public bool IsDef = false;

    public void UpdateFromCard(Card card)
    {
        if (IsDef)
        {
            Number.text = card.Defense.ToString();
        }
        else
        {
            Number.text = card.Attack.ToString();
            Icon.sprite = GetIconSprite(card.DamageType);
        }
    }

    public void UpdateVisibility(bool visible)
    {
        Number.enabled = visible;
        Icon.enabled = visible;
        BackSquare.enabled = visible;
    }

    public Sprite GetIconSprite(DamageTypes dt)
    {
        string stringDamageType;

        switch (dt)
        {
            case DamageTypes.Melee:
                stringDamageType = "icon_melee-02";
                break;
            case DamageTypes.Ranged:
                stringDamageType = "icon_ranged-02";
                break;
            case DamageTypes.Fire:
                stringDamageType = "icon_fire-02";
                break;
            case DamageTypes.Cold:
                stringDamageType = "icon_frost-02";
                break;
            case DamageTypes.Shadow:
                stringDamageType = "icon_shadow-02";
                break;
            case DamageTypes.Light:
                stringDamageType = "icon_light-02";
                break;
            case DamageTypes.Acidic:
                stringDamageType = "icon_acidic-02";
                break;
            case DamageTypes.Electric:
                stringDamageType = "icon_electric-02";
                break;
            default:
                throw new ArgumentException();
        }

        return Resources.Load<Sprite>(string.Format("Images/CardComponents/Icons/{0}", stringDamageType));
    }

    public void Rescale(Vector3 newScale)
    {
        
    }
}