﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnModelController : MonoBehaviour {

    public Renderer Model;

    private const float OUTLINE_WIDTH_FOR_PAWNS = 0.05f;

	// Use this for initialization
	void Start () {
        var materials = Model.materials;

        for(var i = 0; i < materials.Length; ++i)
        {
            var shader = Shader.Find("Outlined/UltimateOutline");
            var mat = new Material(shader);
            mat.mainTexture = materials[i].mainTexture;
            mat.mainTextureOffset = materials[i].mainTextureOffset;
            mat.mainTextureScale = materials[i].mainTextureScale;
            mat.SetFloat("_FirstOutlineWidth", OUTLINE_WIDTH_FOR_PAWNS);
            mat.SetFloat("_SecondOutlineWidth", OUTLINE_WIDTH_FOR_PAWNS);

            materials[i] = mat;
        }

        Model.materials = materials;
        SetOff();
	}

    public void SetAttacking()
    {
        SetColor(Colors.HexToColor(Constants.AttackColor));
    }

    public void SetBlocking()
    {
        SetColor(Colors.HexToColor(Constants.BlockingColor));
    }

    public void SetSelected()
    {
        SetColor(Colors.HexToColor(Constants.SelectedArrowColor));
    }

    public void SetSelectable()
    {
        SetColor(Colors.HexToColor(Constants.SelectableColor));
    }

    public void SetMouseOver()
    {
        SetColor(Colors.HexToColor(Constants.DefaultMouseOverColor));
    }

    public void SetSpecialActivated()
    {
        SetColor(Colors.HexToColor(Constants.SpecialActivatedColor));
    }

    public void SetOff()
    {
        SetColor( new Color(0, 0, 0, 0));
    }

    private void SetColor(Color color)
    {
        var materials = Model.materials;

        for (var i = 0; i < materials.Length; ++i)
        {
            materials[i].SetColor("_FirstOutlineColor", color);
            materials[i].SetColor("_SecondOutlineColor", color);
        }
    }
}
