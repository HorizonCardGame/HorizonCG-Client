﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PawnUIController : MonoBehaviour {

    public PawnAtkDefController AtkController;
    public PawnAtkDefController DefController;
    public Text PawnText;
    public TraitsBarController TraitsBar;

    public void UpdateFields(Card card)
    {
        PawnText.text = card.Name;
        AtkController.UpdateFromCard(card);
        DefController.UpdateFromCard(card);
        TraitsBar.UpdateTraitList(card.Traits);
    }
}
