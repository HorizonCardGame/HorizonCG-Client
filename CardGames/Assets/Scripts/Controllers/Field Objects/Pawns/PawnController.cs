﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TCG.Enums;
using TCG.Utility;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(MonobehaviorMouseActions))]
public class PawnController : BaseBehaviour, IPlacable, IMovable, IPawn, ICardObject, ICard, IDamageable {

    public PawnModelController Visual;
    public Image CardImage;
    public PawnUIController PawnUI;
    public TargetCircle TargetCircle;

    public bool IsExhausted { get; set; }
    public bool IsAttacking { get; set; }
    public bool IsBlocking { get; set; }
    public bool IsSelected { get; set; }
    public bool IsSummonSick { get; private set; }
    private int _turnEntered { get; set; }
    private int _damageTaken = 0;
    private bool _isRemoved { get; set; }
    public Card Card { get; set; }
    public MonobehaviorMouseActions MouseActions { get; set; }

    public BasePlayerController Owner { get { return Card.Owner; } }
    public BasePlayerController Controller { get { return Card.Controller; } }
    public BasePlayerController GetController() { return Controller; }

    private string _currentCardSetId = "";
    private Sprite _currentCardImage;
    private CharacterController CharacterController { get; set; }
    private Animator Anim { get; set; }
    private Material _defaultMaterial { get; set; }
    public Material DefaultMaterial { get { return _defaultMaterial; } }
    private Renderer _renderer { get; set; }

    private UnityAction<IPawn> _OnClick = null;

    private TargettingSystemInfo _targetingSystemInfo { get; set; }
    private bool _isMousedOver { get; set; }

    // Use this for initialization
    void Start()
    {
        CharacterController = GetComponent<CharacterController>();
        Anim = GetComponent<Animator>();
        MouseActions = GetComponent<MonobehaviorMouseActions>();
        MouseActions.OnLeftClick += OnLeftClick;
        MouseActions.OnRightClick += OnRightClick;
        MouseActions.OnMousePointerEnter += OnPointerEnter;
        MouseActions.OnMousePointerExit += OnPointerExit;
        //_renderer = GetComponent<Renderer>();
        //_defaultMaterial = _renderer.material;
    }

    public virtual void Constructor(Card c, BasePlayerController owner)
    {
        c.AssignAnimatable(this);
        _isRemoved = false;
        this.Card = c;
        UpdateCardState();
    }

    public void ChangeOnClick(UnityAction<IPawn> onClick)
    {
        _OnClick = onClick;
    }

    public void CancelOnClick()
    {
        _OnClick = null;
    }

    public void Transform(Card target)
    {
        //TODO Add Transform mechanics
        UpdateVisualState();
    }

    public void Revert()
    {

    }

    public void Attack()
    {
        if (!IsExhausted)
        {
            Publish(EventNames.OnAttack, null);
            IsAttacking = true;
            var targets = Controller.GetListOfValidDefenders();
            Controller.SetAttacking(this, targets[0]);
            UpdateVisualState();
        }
    }

    public void AssignAttacking(IDamageable defender)
    {
        Publish(EventNames.OnAttack, null);
        IsAttacking = true; IsSelected = false;
        Controller.SetAttacking(this, defender);
        UpdateVisualState();
    }

    public void AssignBlocking(IPawn attacker)
    {
        Publish(EventNames.OnBlock, null);
        IsBlocking = true; IsSelected = false;
        attacker.GetController().SetBlocking(this, attacker);
        UpdateVisualState();
    }

    public void RemoveFromAttacking()
    {
        IsAttacking = false; IsSelected = false;
        Controller.RemovingAttacking(this);
        UpdateVisualState();
    }

    public void AttackComplete()
    {
        IsAttacking = false;
        Exhaust();
    }

    public void BlockingComplete()
    {
        IsBlocking = false;
    }

    public bool CanExhaust()
    {
        return !IsExhausted && !IsSummonSick;
    }

    public void Exhaust()
    {
        IsExhausted = true; IsSelected = false;
        TargetCircle.SetExhaust();
        UpdateVisualState();
    }

    public void Recover()
    {
        IsExhausted = false; IsSelected = false;
        TargetCircle.SetOff();
        UpdateVisualState();
    }

    public void OnStartOfYourTurn()
    {
        IsSummonSick = false;
        Recover();
    }

    public void OnStartOfTurn()
    {
        _damageTaken = 0;
    }

    public void DealDamage(IDamageable target)
    {
        target.DealtDamage(this, Card.Attack, Card.DamageType);
    }

    public void DealtDamage(object source, int amount, DamageTypes dt)
    {
        Publish(EventNames.DealtDamage, new DealtDamageEventData { Source = source, Amount = amount, DamageType = dt });
        _damageTaken += amount;

        if(Card.Defense - _damageTaken <= 0)
        {
            Remove();
        }
    }

    public void ShowCard()
    {
        Publish(EventNames.PreviewCard, this.Card);
    }

    #region Animations & Astetics

    public void Move(Vector2 movePos)
    {
        CharacterController.Move(movePos);
    }

    public void UpdateCardState()
    {
        if (Card.Defense - _damageTaken <= 0 || Card.Defense <= 0)
        {
            Remove();
        }

        UpdateVisualState();
        PawnUI.UpdateFields(Card);
        if (_currentCardImage == null || _currentCardSetId != Card.CardSetId)
        {
            ResolveCardImage();
        }
        else
        {
            UpdateCardFace();
        }
    }
    public void UpdateCardState(Card c)
    {
        Card = c;
        UpdateCardState();
    }

    public void UpdateVisualState()
    {
        if (IsAttacking)
        {
            Visual.SetAttacking();
            TargetCircle.SetAttacking();
        }
        else if (IsBlocking)
        {
            Visual.SetBlocking();
            TargetCircle.SetBlocking();
        }
        else if (IsSelected)
        {
            Visual.SetSelected();
            TargetCircle.SetOff();
        }
        else if (_isMousedOver)
        {
            if(ShouldBeTarget)
            {
                Visual.SetSelected();
                TargetCircle.SetSelected();
            }
            else
            {
                Visual.SetSelectable();
                TargetCircle.SetSelectable();
            }
        }
        else
        {
            if (Card.HasSpecialActivation())
            {
                Visual.SetSpecialActivated();
                TargetCircle.SetSpecialActivated();
            }
            else if(ShouldBeTarget)
            {
                Visual.SetSelectable();
                TargetCircle.SetSelectable();
            }
            else
            {
                Visual.SetOff();
                TargetCircle.SetOff();
            }
        }
    }

    public void SetVisualState(VisualStates state)
    {
        switch (state)
        {
            case VisualStates.Selectable:
                Visual.SetSelectable();
                IsSelected = true;
                break;
            case VisualStates.Selected:
                Visual.SetSelected();
                IsSelected = true;
                break;
            case VisualStates.Exhausted:
            case VisualStates.Off:
            default:
                Visual.SetOff();
                IsSelected = false;
                break;
        }
    }
    #endregion

    #region UserInteractions
    public void OnLeftClick(Vector3 mousePosition)
    {
        if (_OnClick != null)
            _OnClick(this);
    }

    public void OnRightClick(Vector3 mousePosition)
    {
        var items = new List<RightClickItem>();

        items.Add(new RightClickItem { Text = "View Card", ClickAction = (PointerEventData data) => { ShowCard(); }, Enabled = true });

        if(IsLocallyControlled())
        {
            var options = Card.Actions.Where(x => x.HasOptionMenuItem(CardZones.Field));
            if (options.Any())
            {
                foreach (var opt in options)
                {
                    items.Add(opt.GetRightClickItem());
                }
            }
        }

        Publish(EventNames.ShowOptionsMenu, new OptionsMenuEventData
        {
            ClickPosition = mousePosition,
            Items = items
        });
    }

    public void OnPointerEnter()
    {
        _isMousedOver = true;
        UpdateVisualState();
        if (ShouldBeTarget)
        {
            _targetingSystemInfo.TargetingCallback(this, TargettingSystemCallbacks.Entered);
        }
    }

    public void OnPointerExit()
    {
        _isMousedOver = false;
        UpdateVisualState();
        if (ShouldBeTarget)
        {
            _targetingSystemInfo.TargetingCallback(this, TargettingSystemCallbacks.Exited);
        }
    }
    #endregion

    #region Placable
    public void ChangePosition(Vector3 position)
    {
        ChangePosition(position, this.transform.localRotation);
    }

    public virtual void ChangePosition(Vector3 position, Quaternion rotation)
    {
        transform.localPosition = position;
        transform.localRotation = rotation;
    }

    public void Remove()
    {
        //Visual.gameObject.SetActive(false);
        //PawnUI.gameObject.SetActive(false);

        Publish(EventNames.LeftField, new LeftFieldEventData { Card = Card, LastPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z)  });
        Card.RemoveAnimatable();
        _isRemoved = true;
        Owner.RemoveCardFromBoard(this);

        GameMaster.Instance.Destroy(this.gameObject);
    }

    public bool IsRemoved()
    {
        return _isRemoved;
    }

    public void Destroy()
    {
        Remove();
        Owner.MoveCardToDiscard(Card);
    }

    public void Banish()
    {
        Remove();
        Owner.MoveCardToBanished(Card);
    }

    public void Aetherize()
    {
        Remove();
        Owner.MoveToAetherize(Card);
    }
    #endregion

    #region Accessors
    public Card GetCard()
    {
        return Card;
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public bool IsLocallyControlled()
    {
        return Controller == GameMaster.Instance.LocalPlayer;
    }

    public IDamageable GetDamageable()
    {
        return this;
    }
    #endregion

    public void ResolveCardImage()
    {
        if (!string.IsNullOrEmpty(Card.CardSetId))
        {
            _currentCardSetId = Card.CardSetId;
            _currentCardImage = CardDatabase.Instance.GetCardImage(Card.CardSetId);
        }

        UpdateCardFace();
    }

    private void UpdateCardFace()
    {
        CardImage.sprite = _currentCardImage;
    }

    public void EnterField()
    {
        _turnEntered = GameMaster.Instance.CurrentTurn;
        IsSummonSick = (Card.Type.Contains(CardType.UNIT) && !Card.Traits.Contains(Traits.Rush));
        Publish(EventNames.EnteredField, new EnteredFieldEventData { Card = Card });
    }

    #region IAnimatable

    public void ChangeMaterial(Material material)
    {
        //_renderer.material = material;
    }

    public void ResetToDefaultMaterial()
    {
        //_renderer.material = _defaultMaterial;
    }
    #endregion

    public override void SetTargetingInfo(IBaseEventUser sender, TargettingSystemInfo targettingInfo)
    {
        if (!targettingInfo.Filter.IsTarget(this))
            return;

        ShouldBeTarget = true;
        _targetingSystemInfo = targettingInfo;
    }

    public void AdjustStatistics(JSONNode statistics)
    {
        throw new NotImplementedException();
    }
}
