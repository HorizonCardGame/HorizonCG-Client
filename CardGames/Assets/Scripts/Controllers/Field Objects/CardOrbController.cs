﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardOrbController : MonoBehaviour {

    public Transform Ring;
    public OrbController CardOrbPrefab;
    public float OrbMovementSpeed = 3f;
    public float OrbitingDistance = 1.5f;
    public float MovementStep = 0.1f;
    public int MaximumSpacingNumber = 100;

    private List<OrbController> _cardOrbs = new List<OrbController>();
    private bool _orbsAreEbbing = true;
    private int _currentTiding = 0;
	
	// Update is called once per frame
	void Update () {
		for(var i = 0; i < _cardOrbs.Count; i++)
        {
            _cardOrbs[i].UpdateMovementPattern(((_orbsAreEbbing) ? MovementStep : -MovementStep) * i);

            if (_orbsAreEbbing)
            {
                if (++_currentTiding > MaximumSpacingNumber) _orbsAreEbbing = false;
            }
            else
            {
                if (--_currentTiding < 0) _orbsAreEbbing = true;
            }
        }
        var newRotation = transform.localRotation.eulerAngles + (new Vector3(180, 0, 0));
        transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(newRotation), 0.1f * Time.deltaTime);
	}

    public void UpdateCardNumbers(int numberOfCards)
    {
        if(numberOfCards > _cardOrbs.Count)
        {
            var difference = numberOfCards - _cardOrbs.Count;
            for (var i = 0; i < difference; i++)
            {
                var newNumber = GameMaster.Instance.Instantiate(CardOrbPrefab.gameObject, Ring.transform).GetComponent<OrbController>();
                newNumber.Radius = OrbitingDistance;
                newNumber.LeaderMovementAmount = OrbMovementSpeed;
                _cardOrbs.Add(newNumber);
            }
        }
        else if(numberOfCards < _cardOrbs.Count)
        {
            var difference = _cardOrbs.Count - numberOfCards;
            for (var i = 0; i < difference; i++)
            {
                var cardOrb = _cardOrbs[_cardOrbs.Count - 1];
                _cardOrbs.Remove(cardOrb);
                GameMaster.Instance.Destroy(cardOrb.gameObject);
            }
        }
    }
}
