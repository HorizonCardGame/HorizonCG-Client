﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbController : MonoBehaviour {

    public GameObject Visual;
    public float LeaderMovementAmount = 3f;
    public float Radius = 1.5f;

    private float _angle = 0;
    private float _currentSpeed = 3f;

    public void UpdateMovementPattern(float speedStep, float radiusStep = 0f)
    {
        Radius += radiusStep;
        if (Radius < 0) Radius = 0;
        _currentSpeed += speedStep;
        if (_currentSpeed < 0.05f) _currentSpeed = 0.05f;

        _angle += ((2 * Mathf.PI) / _currentSpeed) * Time.deltaTime; //if you want to switch direction, use -= instead of +=
        var x = Mathf.Cos(_angle) * Radius;
        var y = Mathf.Sin(_angle) * Radius;
        transform.localPosition = new Vector3(x, 0, y);
    }
}
