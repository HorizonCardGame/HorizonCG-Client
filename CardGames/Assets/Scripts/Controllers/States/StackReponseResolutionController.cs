﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;

public class StackReponseResolutionController : BaseBehaviour
{
    private Stack<IStackData> _stack = new Stack<IStackData>();
    private Stack<State> _previousStates = new Stack<State>();
    [SerializeField]
    private Transform _StackVisual;
    [SerializeField]
    private CardController _BaseCard;

    private GameMaster GM;
    private const float MOVE_DURATION = 0.5f;
    private const float STACK_SCALE = 0.7f;
    private const float STACK_SPACING = 35f;

    public int Count { get { return _stack.Count; } }

    protected override void Awake()
    {
        base.Awake();
        GM = GetComponent<GameMaster>();
    }

    public void ChangeToPreviousState()
    {
        if(_previousStates.Count > 0)
        {
            var previousState = _previousStates.Pop();
            GM.ChangeState(previousState);
        }
    }

    public void Add(IStackData cardData)
    {
        _stack.Push(cardData);
        _previousStates.Push(GM.CurrentState);
        StartCoroutine(AddVisual(Peek()));
    }

    public IStackData Peek()
    {
        return _stack.Peek();
    }

    public IStackData Pop()
    {
        var stack = _stack.Pop();
        StartCoroutine(RemoveVisual(stack));

        return stack;
    }

    public void Clear()
    {
        _stack.Clear();
    }

    private IEnumerator AddVisual(IStackData data)
    {
        var instatiatedCard = data.GetVisual(_StackVisual.transform);

        var origin = (data.Origin != null) ? data.Origin : Vector3.zero;
        instatiatedCard.transform.position = GM.MainCamera.WorldToScreenPoint(origin);
        instatiatedCard.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

        var t1 = instatiatedCard.transform.MoveToLocal(Vector3.zero, MOVE_DURATION, EasingEquations.EaseInOutQuad);
        var t2 = instatiatedCard.transform.ScaleTo(new Vector3(STACK_SCALE, STACK_SCALE, STACK_SCALE), MOVE_DURATION, EasingEquations.EaseInOutQuad);

        while (t1 != null && t2 != null)
            yield return null;

        //instatiatedCard.UpdateCardState();
        StartCoroutine(UpdateVisualState(GM.ChangeState<CheckStackState>));
    }

    private IEnumerator RemoveVisual(IStackData data)
    {
        var controller = GetChildCardFromCard(data.Card);
        Destroy(controller.gameObject);

        yield return null;
        StartCoroutine(UpdateVisualState());
    }

    private IEnumerator UpdateVisualState(UnityAction onFinish = null)
    {
        var tasks = new List<Tweener>();

        for(var i = 0; i < _stack.Count; ++i)
        {
            var controller = GetChildCardFromCard(_stack.ElementAt(i).Card);
            controller.transform.SetSiblingIndex(i);
            tasks.Add(controller.transform.MoveToLocal(new Vector3(0, i * STACK_SPACING), MOVE_DURATION, EasingEquations.EaseInOutQuad));
        }

        while (tasks.Any(x => x != null))
            yield return null;

        if (onFinish != null)
            onFinish();
    }

    private Transform GetChildCardFromCard(Card card)
    {
        foreach(Transform c in _StackVisual.transform)
        {
            var controller = c.GetComponent<BaseCardVisual>(); //TODO to be more general like with BaseBehavior or something similar
            if(controller.GetCard().BaseId == card.BaseId)
            {
                return controller.transform;
            }
        }

        return null;
    }
}
