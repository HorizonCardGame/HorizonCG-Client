﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Linq;

public interface IStackData
{
    Card Card { get; }
    BasePlayerController Controller { get; }
    Vector3 Origin { get; }

    void Run();
    Transform GetVisual(Transform parent);
}

public class ActionData : IStackData
{
    public Card Card {
        get {return Action.Card.GetCard();} 
    }
    public BasePlayerController Controller { get { return Card.Controller; } }
    public BaseAction Action { get; set; }
    public Vector3 Origin { get; set; }

    public void Run()
    {
        Action.Invoke();
    }

    public Transform GetVisual(Transform parent)
    {
        var instantiatedCard = GameMaster.Instance.InstantiateActionVisual(parent);
        instantiatedCard.Constructor(Action, Card.Name, Action.ToString(), Card.Attributes);

        return instantiatedCard.transform;
    }
}

public class NonAbilityStackData : IStackData
{
    public Card Card { get; set; }
    public BasePlayerController Controller { get { return Card.Controller; } }
    public BasePlayerController Caster { get; set; }
    public UnityAction Callback { get; set; }
    public Vector3 Origin { get; set; }

    public void Run()
    {
        var pawn = GameMaster.Instance.InstantiatePawn(Caster.Board.transform);
        pawn.Constructor(Card, Caster);

        GameMaster.Instance.CardsInPlay.Add(new CardNode(Card, pawn, Caster));
        Caster.AddCardToBoard(pawn);
    }

    public Transform GetVisual(Transform parent)
    {
        var instantiatedCard = GameMaster.Instance.InstantiateCardController(parent);
        instantiatedCard.Constructor(Card, true, true);

        return instantiatedCard.transform;
    }
}

public class AbilityStackData : IStackData
{
    public Card Card { get; set; }
    public BasePlayerController Controller { get { return Card.Controller; } }
    public Vector3 Origin { get; set; }

    public void Run()
    {
        Card.Actions.Where(x => x is AbilityAction)
                    .Select(y => (AbilityAction)y).ToList().ForEach(z => z.TriggerAction());
        Card.Owner.MoveCardToDiscard(Card);
    }

    public Transform GetVisual(Transform parent)
    {
        var instantiatedCard = GameMaster.Instance.InstantiateCardController(parent);
        instantiatedCard.Constructor(Card, true, true);

        return instantiatedCard.transform;
    }
}
