﻿using UnityEngine;
using System.Collections;

public class CheckStackState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        GM.StateBaseActions();
        StartCoroutine(EvaluateStack());
    }

    IEnumerator EvaluateStack()
    {
        yield return null;

        if (Stack.Count == 0)
        {
            Stack.ChangeToPreviousState();
        }
        else
        {
            GM.ChangeState<ResponseState>();
        }
    }
}
