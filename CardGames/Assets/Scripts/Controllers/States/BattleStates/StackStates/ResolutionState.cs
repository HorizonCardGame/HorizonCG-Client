﻿using UnityEngine;
using System.Collections;
using System.Linq;
using TCG.Enums;

public class ResolutionState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        StartCoroutine(Resolve());
    }

    IEnumerator Resolve()
    {
        yield return null;
        if(Stack.Count > 0)
        {
            IStackData data = Stack.Pop();

            data.Run();
        }

        GM.ChangeState<CheckStackState>();
    }
}
