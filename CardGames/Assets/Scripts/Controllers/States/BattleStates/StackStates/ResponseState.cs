﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class ResponseState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        GM.StartReactionSequence(Stack.Peek().Controller, Finish);
    }

    public void Finish()
    {
        GM.ChangeState<ResolutionState>();
    }
}
