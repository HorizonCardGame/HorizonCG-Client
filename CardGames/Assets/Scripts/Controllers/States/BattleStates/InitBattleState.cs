﻿using UnityEngine;
using System.Collections;

public class InitBattleState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        StartCoroutine(DownloadDatabase());
    }

    IEnumerator DownloadDatabase()
    {
        var downloader = GetComponent<CardDatabaseDownloader>();
        if (!downloader.IsDatabaseReady())
        {
            downloader.DownloadDatabase();

            while (!downloader.CompletelyFinished)
                yield return null;
        }

        var database = GetComponent<CardDatabase>();
        database.Constructor();

        while (!database.CompletelyFinished)
            yield return null;

        PlayerList.Add(new PlayerNode(GM.LocalPlayer));
        PlayerList.Add(new PlayerNode(GM.ComputerPlayer));

        for (int i = 0; i < PlayerList.Count; ++i)
        {
            PlayerList[i].Player.Constructor(Constants.ColorsForPlayers[i]);
            PlayerList[i].Player.OnBeginningOfGame();
        }

        //_currentPlayerIndex = Randomizer.Instance.Random.Next(0, _playerList.Count);
        GM.CurrentTurn = 0;
        GM.CurrentPlayerIndex = 0;

        GM.ChangeState<StartOfTurnState>();
    }
}
