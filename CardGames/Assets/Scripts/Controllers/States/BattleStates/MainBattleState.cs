﻿using System.Collections;
using UnityEngine.Events;

public class MainBattleState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        if(GM.LocalPlayer == CurrentPlayer)
        {
            if (GM.ShouldGoToCombat())
            {
                GM.ChangeButtonLook("Go to Attacks", Colors.HexToColor("#803030"), Colors.HexToColor("#903030"), Colors.HexToColor("#502020"), GoToCombat);
            }
            else
            {
                GM.ChangeButtonLook("End Turn", Colors.HexToColor("#303080"), Colors.HexToColor("#303080"), Colors.HexToColor("#502020"), GoToEndOfTurn);
            }
        }
        else
        {
            GM.DisableButton();
        }
        StartCoroutine(StartOfMainState());
    }

    IEnumerator StartOfMainState()
    {
        yield return null;
        EventManager.Publish(null, EventNames.OnMainPhase, null, CurrentPlayer.BaseId.ToString());
    }

    private void GoToCombat()
    {
        GM.CurrentNumberOfCombatPhases++;
        GM.PhaseChange<DeclareAttacksBattleState>();
    }

    private void GoToEndOfTurn()
    {
        GM.CurrentNumberOfCombatPhases = 0;
        GM.ChangeState<EndOfTurnState>();
    }

    public void DoCurrentMainStateAction()
    {
        if (GM.ShouldGoToCombat())
        {
            GoToCombat();
        }
        else
        {
            GoToEndOfTurn();
        }
    }
}