﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System;

public class BattleState : State
{
    protected GameMaster GM;
    protected StackReponseResolutionController Stack { get { return GM.Stack; } }  
    protected BasePlayerController CurrentPlayer { get { return GM.CurrentPlayer; } }
    protected List<PlayerNode> PlayerList { get { return GM.PlayerList; } }

    private void Awake()
    {
        GM = GetComponent<GameMaster>();
    }
}
