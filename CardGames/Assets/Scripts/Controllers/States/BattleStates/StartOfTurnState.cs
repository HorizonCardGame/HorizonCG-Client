﻿using UnityEngine;
using System.Collections;

public class StartOfTurnState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        GM.DisableButton();
        //Trigger at the start of turn.

        EventManager.Publish(null,
            EventNames.DisplayMessage,
            (CurrentPlayer.GetType() == typeof(LocalPlayerController)) ?
                        Localization.CurrentPlayersTurn :
                        string.Format(Localization.OtherPlayersTurn, CurrentPlayer.PlayerName));

        StartCoroutine(GoToNextPhase());
    }

    IEnumerator GoToNextPhase()
    {
        yield return null;
        EventManager.Publish(null, EventNames.StartOfTurn, null, CurrentPlayer.BaseId.ToString());
        CurrentPlayer.OnStartOfTurn();
        GM.PhaseChange<MainBattleState>();
    }
}
