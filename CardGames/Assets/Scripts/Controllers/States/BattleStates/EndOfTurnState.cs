﻿using UnityEngine;
using System.Collections;

public class EndOfTurnState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        GM.DisableButton();
        //ResponseTimer
        StartCoroutine(MoveToNextTurn());
    }

    IEnumerator MoveToNextTurn()
    {
        yield return null;
        CurrentPlayer.OnEndOfTurn();
        EventManager.Publish(null, EventNames.EndOfTurn, null, CurrentPlayer.BaseId.ToString());

        GM.StartReactionSequence(CurrentPlayer, Finish_MoveToNextTurn);
    }

    private void Finish_MoveToNextTurn()
    {
        GM.SetCurrentPlayer(++GM.CurrentPlayerIndex);
        GM.CurrentTurn++;
        GM.ChangeState<StartOfTurnState>();
    }
}
