﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TCG.Utility;
using UnityEngine;

public class DeclareAttacksBattleState : BattleState
{
    public override void Enter()
    {
        base.Enter();

        StartCoroutine(DeclareAttacks());
    }

    private IEnumerator DeclareAttacks()
    {
        yield return null;

        CurrentPlayer.GetAttacking(ConfirmAttacks);
    }

    private void ConfirmAttacks(List<Tuple<IPawn, IDamageable>> attackers)
    {
        attackers.ForEach(x => x.First.AssignAttacking(x.Second));

        GM.ConstructBattleArcs(attackers);

        if (attackers.Any())
        {
            StartCoroutine(MoveToBlocks());
        }
        else
        {
            StartCoroutine(MoveToSecondMain());
        }
    }

    private IEnumerator MoveToBlocks()
    {
        yield return null;

        GM.PhaseChange<DeclareBlocksBattleState>();
    }

    private IEnumerator MoveToSecondMain()
    {
        yield return null;

        GM.ChangeState<MainBattleState>();
    }
}
