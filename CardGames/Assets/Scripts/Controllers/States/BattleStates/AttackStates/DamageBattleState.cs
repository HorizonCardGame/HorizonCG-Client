﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageBattleState : BattleState
{
    public const float DISTANCE_FROM_TARGET = 1f;
    public const float MOVEMENT_DURATION = 1.2f;
    public override void Enter()
    {
        base.Enter();

        StartCoroutine(GoThroughAttacks());
    }

    private IEnumerator GoThroughAttacks()
    {
        GM.ClearBattleArcs();
        var attacks = CurrentPlayer.Attackers;

        foreach(var attack in attacks)
        {
            var attacker = attack.First;
            var startingPosition = attacker.GetTransform().position;
            var originalRotation = attacker.GetTransform().rotation;
            var inFrontOfTarget = attack.Second.GetTransform().position + (DISTANCE_FROM_TARGET * attack.Second.GetTransform().forward);
            attacker.GetTransform().LookAt(inFrontOfTarget);
            Tweener movement = attacker.GetTransform().MoveTo(inFrontOfTarget, MOVEMENT_DURATION);

            while (movement != null)
                yield return null;

            attacker.GetTransform().LookAt(attack.Second.GetTransform());

            //attack animation

            attacker.DealDamage(attack.Second);
            attacker.AttackComplete();

            if(attack.Second is IPawn)
            {
                ((IPawn)attack.Second).DealDamage((IDamageable)attacker);
                ((IPawn)attack.Second).BlockingComplete();
            }

            if (!attacker.IsRemoved())
            {
                attacker.GetTransform().LookAt(startingPosition);
                movement = attacker.GetTransform().MoveTo(startingPosition, MOVEMENT_DURATION);

                while (movement != null)
                    yield return null;

                attacker.GetTransform().rotation = originalRotation;
            }
        }

        StartCoroutine(StartBackToMainPhase());
    } 

    private IEnumerator StartBackToMainPhase()
    {
        yield return null;

        CurrentPlayer.Attackers.Clear();

        GM.StartReactionSequence(CurrentPlayer, ToMainPhaseTwo);
    }

    public void ToMainPhaseTwo()
    {
        GM.ChangeState<MainBattleState>();
    }
}
