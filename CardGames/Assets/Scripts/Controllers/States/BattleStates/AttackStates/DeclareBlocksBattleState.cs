﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TCG.Utility;
using UnityEngine;

public class DeclareBlocksBattleState : BattleState
{
    public override void Enter()
    {
        base.Enter();

        StartCoroutine(StartDeclareBlocks());
    }

    private IEnumerator StartDeclareBlocks()
    {
        yield return null;

        var targets = CurrentPlayer.Attackers.Select(x => x.Second);

        var players = new List<BasePlayerController>();

        foreach(var target in targets)
        {
            if(target is BasePlayerController)
            {
                if (!players.Contains((BasePlayerController)target))
                    players.Add((BasePlayerController)target);
            }
            else if(target is IPawn)
            {
                var controller = ((IPawn)target).GetCard().Controller;
                if (!players.Contains(controller))
                {
                    players.Add(controller);
                }
            }
        }

        StartAssignBlocksSequence(players);
    }

    private int _startingIndex = 1;
    private int _currentIndex = -1;
    private List<BasePlayerController> _blockerList;
    public void StartAssignBlocksSequence(List<BasePlayerController> blockerList)
    {
        _startingIndex = 0;
        _currentIndex = -1;
        _blockerList = blockerList;
        StartCoroutine(Begin());
    }

    IEnumerator Begin()
    {
        yield return null;
        Continue();
    }

    public void Continue()
    {
        if (_currentIndex == _startingIndex)
        {
            Finish();
            return;
        }
        else if (_currentIndex < 0)
        {
            _currentIndex = _startingIndex;
        }

        var currentPlayer = _blockerList[_currentIndex];

        if (++_currentIndex >= _blockerList.Count)
        {
            _currentIndex = 0;
        }

        currentPlayer.AssignBlockers(AssignBlocks);
    }

    private void AssignBlocks(List<Tuple<IPawn, IDamageable>> assignedList)
    {
        assignedList.Where(w => w.Second is IPawn).ToList().ForEach(x => ((IPawn)x.Second).AssignBlocking(x.First));
        GM.ConstructBattleArcs(assignedList);
        Continue();
    }

    public void Break()
    {

    }

    public void Finish()
    {
        _startingIndex = -1;
        _currentIndex = -1;
        _blockerList = null;

        GM.PhaseChange<DamageBattleState>();
    }
}
