﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TCG.Enums;
using UnityEngine;
using UnityEngine.Events;

public class TargettingSystemController : MonoBehaviour {

    public TargettingArrowController Arrow;

    private List<IBaseEventUser> _targets { get; set; }
    private TargetSelectionFilter _currentFilter { get; set; }
    private IBaseEventUser _currentHover { get; set; }
    private bool _slideToCast = false;

    private State _previousState { get; set; }

    private UnityAction<List<IBaseEventUser>, List<object>> Callback { get; set; }
    private UnityAction Cancel { get; set; }

    public static TargettingSystemController _instance;
    public static TargettingSystemController Instance
    {
        get
        {
            return _instance;
        }
    }
    void GetInstance()
    {
        _instance = this;
    }

    // Use this for initialization
    void Start () {
        GetInstance();
        _targets = new List<IBaseEventUser>();
        Arrow.SetAnimationAndShow(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(_currentFilter != null)
        {
            if((Input.GetMouseButtonUp(0) && _slideToCast) || (Input.GetMouseButton(0) && !_slideToCast))
            {
                if(_currentHover == null)
                {
                    CancelTargeting();
                }
                else
                {
                    if(!_targets.Contains(_currentHover))
                    {
                        _targets.Add(_currentHover);
                    }
                    
                    if(_targets.Count >= _currentFilter.NumberOfTargets)
                    {
                        FinishedTargeting();
                    }
                }
            }

            if (Input.GetMouseButton(1))
            {
                CancelTargeting();
            }
        }
	}

    public void BeginTargeting(TargetSelectionFilter targetingArguments, bool slideToCast, UnityAction<List<IBaseEventUser>, List<object>> callback, UnityAction cancel)
    {
        //ToDo: Show message on screen until finished or cancelled
        _previousState = GameMaster.Instance.CurrentState;
        //GameMaster.Instance.ChangeState<TargettingState>();

        Arrow.SetStartingPosition(targetingArguments.Position);
        Arrow.SetArrowColor(Colors.HexToColor(Constants.DefaultArrowColor));
        Arrow.SetAnimationAndShow(true);
        _currentFilter = targetingArguments;
        _slideToCast = slideToCast;
        Callback = callback;
        Cancel = cancel;
        EventManager.Publish(null, EventNames.SetTargettingInfo, new TargettingSystemInfo
        {
            TargetingCallback = Targeting_Callback,
            Filter = targetingArguments
        });
    }

    private void FinishedTargeting()
    {
        EventManager.Publish(null, EventNames.StopTargettingInfo, null);
        Arrow.SetAnimationAndShow(false);
        _currentHover = null;
        Cancel = null;
        Callback(_targets, _currentFilter.AdditionalArguments);
        _currentFilter = null;
        //GameMaster.Instance.ChangeState(_previousState);
        _previousState = null;
        _targets = new List<IBaseEventUser>();
        Callback = null;
    }

    private void CancelTargeting()
    {
        EventManager.Publish(null, EventNames.StopTargettingInfo, null);
        Arrow.SetAnimationAndShow(false);
        _currentFilter = null;
        _currentHover = null;
        _targets = new List<IBaseEventUser>();
        Callback = null;
        Cancel();
        //GameMaster.Instance.ChangeState(_previousState);
        _previousState = null;
        Cancel = null;
    }

    public void Targeting_Callback(IBaseEventUser sender, TargettingSystemCallbacks callbackType)
    {
        if(callbackType == TargettingSystemCallbacks.Entered)
        {
            _currentHover = sender;
            Arrow.SetArrowColor(Colors.HexToColor(Constants.SelectedArrowColor));
        }
        else if(callbackType == TargettingSystemCallbacks.Exited)
        {
            if(_currentHover == sender)
            {
                _currentHover = null;
                Arrow.SetArrowColor(Colors.HexToColor(Constants.DefaultArrowColor));
            }
        }
    }
}

public enum TargettingSystemCallbacks
{
    Entered,
    Exited,
    Up
}

public enum Targets
{
    Players = 1 >> 1,
    Cards = 1 >> 2,
    Pawns = 1 >> 3,
    Aether = 1 >> 4
}

public class TargetSelectionFilter
{
    public int NumberOfTargets { get; set; }
    public Vector3 Position { get; set; }
    public string Message { get; set; }
    public string CardText { get; set; }
    public Func<ICard, bool> IsTarget;
    public List<object> AdditionalArguments { get; set; }

    public TargetSelectionFilter(string cardtext, string mes, int num, Vector3 position, Func<ICard, bool> inFilterFunction)
    {
        CardText = cardtext;
        Message = mes;
        NumberOfTargets = num;
        IsTarget = inFilterFunction;
    }

    public TargetSelectionFilter(Func<ICard, bool> inFilterFunction, List<object> args = null)
    {
        CardText = null;
        Message = null;
        NumberOfTargets = -1;
        IsTarget = inFilterFunction;
        AdditionalArguments = args;
    }

    public override string ToString()
    {
        //if (NumberOfTargets < 0)
        //{
        //    return GetStringForAll();
        //}
        //else
        //{
        //    return GetStringForTargets();
        //}

        return Message;
    }

    //public string GetStringForAll()
    //{
    //    if (TargetTypes.Has(Targets.Players))
    //    {
    //        return (ControllerType.Has(ControllerTypes.Enemy) && ControllerType.Has(ControllerTypes.Ally)) 
    //            ? "each player" : ControllerType.Has(ControllerTypes.Enemy) ? "each opponent" : "each ally";
    //    }
    //    else if (TargetTypes.Has(Targets.Pawns))
    //    {
    //        var output = "Each";
    //        if (CardTypeBlacklist.Any())
    //        {
    //            for (var i = 0; i < CardTypeBlacklist.Count; ++i)
    //            {
    //                output += " Non-" + GetCardTypeString(CardTypeBlacklist[i]);
    //            }
    //        }

    //        if (CardTypeWhitelist.Any())
    //        {
    //            for (var i = 0; i < CardTypeWhitelist.Count; ++i)
    //            {
    //                output += " " + GetCardTypeString(CardTypeWhitelist[i]);
    //            }
    //        }

    //        if (NumberOfTargets > 1)
    //        {
    //            if (CardTypeBlacklist.Count > 1 || CardTypeWhitelist.Count > 1)
    //            {
    //                output += " Objects";
    //            }
    //            else
    //            {
    //                output += "s";
    //            }
    //        }

    //        return output;
    //    }
    //    else if (TargetTypes.Has(Targets.Cards))
    //    {
    //        var output = "Each";
    //        if (CardTypeBlacklist.Any())
    //        {
    //            for (var i = 0; i < CardTypeBlacklist.Count; ++i)
    //            {
    //                output += " Non-" + GetCardTypeString(CardTypeBlacklist[i]);
    //            }
    //        }

    //        if (CardTypeWhitelist.Any())
    //        {
    //            for (var i = 0; i < CardTypeWhitelist.Count; ++i)
    //            {
    //                output += " " + GetCardTypeString(CardTypeWhitelist[i]);
    //            }
    //        }

    //        return output + ((NumberOfTargets > 1) ? " cards" : "card");
    //    }
    //    else if (TargetTypes.Has(Targets.Aether))
    //    {
    //        var output = "Each";
    //        if (CardTypeBlacklist.Any())
    //        {
    //            for (var i = 0; i < CardTypeBlacklist.Count; ++i)
    //            {
    //                output += " Non-" + GetCardTypeString(CardTypeBlacklist[i]);
    //            }
    //        }

    //        if (CardTypeWhitelist.Any())
    //        {
    //            for (var i = 0; i < CardTypeWhitelist.Count; ++i)
    //            {
    //                output += " " + GetCardTypeString(CardTypeWhitelist[i]);
    //            }
    //        }

    //        return output + " Aether";
    //    }
    //    else
    //    {
    //        return (NumberOfTargets > 1 ? "Targets" : "Target");
    //    }
    //}

    //public string GetStringForTargets()
    //{
    //    if (TargetTypes.Has(Targets.Players) && TargetTypes.Has(Targets.Pawns))
    //    {
    //        return (NumberOfTargets > 1 ? "Targets" : "Target");
    //    }
    //    else if (TargetTypes.Has(Targets.Players))
    //    {
    //        return (NumberOfTargets > 1 ? NumberOfTargets + " target players" : "Target player");
    //    }
    //    else if (TargetTypes.Has(Targets.Pawns))
    //    {
    //        var output = (NumberOfTargets > 1 ? NumberOfTargets + " target" : "Target");
    //        if (CardTypeBlacklist.Any())
    //        {
    //            for(var i = 0; i < CardTypeBlacklist.Count; ++i)
    //            {
    //                output += " Non-" + GetCardTypeString(CardTypeBlacklist[i]);
    //            }
    //        }

    //        if (CardTypeWhitelist.Any())
    //        {
    //            for (var i = 0; i < CardTypeWhitelist.Count; ++i)
    //            {
    //                output += " " + GetCardTypeString(CardTypeWhitelist[i]);
    //            }
    //        }

    //        if(NumberOfTargets > 1)
    //        {
    //            if(CardTypeBlacklist.Count > 1 || CardTypeWhitelist.Count > 1)
    //            {
    //                output += " Objects";
    //            }
    //            else
    //            {
    //                output += "s";
    //            }
    //        }

    //        return output;
    //    }
    //    else if (TargetTypes.Has(Targets.Cards))
    //    {
    //        var output = (NumberOfTargets > 1 ? NumberOfTargets + " target" : "Target");
    //        if (CardTypeBlacklist.Any())
    //        {
    //            for (var i = 0; i < CardTypeBlacklist.Count; ++i)
    //            {
    //                output += " Non-" + GetCardTypeString(CardTypeBlacklist[i]);
    //            }
    //        }

    //        if (CardTypeWhitelist.Any())
    //        {
    //            for (var i = 0; i < CardTypeWhitelist.Count; ++i)
    //            {
    //                output += " " + GetCardTypeString(CardTypeWhitelist[i]);
    //            }
    //        }

    //        return output + ((NumberOfTargets > 1) ? " cards" : "card");
    //    }
    //    else if (TargetTypes.Has(Targets.Aether))
    //    {
    //        var output = (NumberOfTargets > 1 ? NumberOfTargets + " target" : "Target");
    //        if (CardTypeBlacklist.Any())
    //        {
    //            for (var i = 0; i < CardTypeBlacklist.Count; ++i)
    //            {
    //                output += " Non-" + GetCardTypeString(CardTypeBlacklist[i]);
    //            }
    //        }

    //        if (CardTypeWhitelist.Any())
    //        {
    //            for (var i = 0; i < CardTypeWhitelist.Count; ++i)
    //            {
    //                output += " " + GetCardTypeString(CardTypeWhitelist[i]);
    //            }
    //        }

    //        return output + " Aether";
    //    }
    //    else
    //    {
    //        return (NumberOfTargets > 1 ? "Targets" : "Target");
    //    }
    //}

    private string GetCardTypeString(CardType type)
    {
        switch (type)
        {
            case CardType.ABILITY:
                return "Ability";
            case CardType.ATTACHMENT:
                return "Attachment";
            case CardType.CHARM:
                return "Charm";
            case CardType.FAST:
                return "Fast";
            case CardType.HERO:
                return "Hero";
            case CardType.KEYWORD:
                return "Keyword";
            case CardType.LOCATION:
                return "Location";
            case CardType.RELIC:
                return "Relic";
            case CardType.UNIQUE:
                return "Unique";
            case CardType.UNIT:
                return "Unit";
            default:
                return "";
        }
    }
}

public static class Filters
{
    public static Func<ICard, bool> CardsInHand
    {
        get
        {
            return ((ICard c) => {
                return c.GetCard().IsInHand();
            });
        }
    }

    public static Func<ICard, bool> UnitsOnTheField
    {
        get
        {
            return ((ICard c) => {
                return c.GetCard().Type.Contains(CardType.UNIT) && c.GetCard().IsOnField();
            });
        }
    }

    public static Func<ICard, bool> Players
    {
        get
        {
            return ((ICard c) => {
                return c.GetType().IsSubclassOf(typeof(BasePlayerController));
            });
        }
    }
}

public struct TargettingSystemInfo
{
    public TargetSelectionFilter Filter { get; set; }
    public UnityAction<IBaseEventUser, TargettingSystemCallbacks> TargetingCallback { get; set; }
}

public enum ControllerTypes
{
    None = 1,
    Your = 2,
    Ally = 4,
    Enemy = 8
}