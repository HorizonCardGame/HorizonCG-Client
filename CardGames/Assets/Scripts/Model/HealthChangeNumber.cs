﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthChangeNumber : MonoBehaviour
{
    public CText Text;

    private const float MINIMUM_VELOCITY = 1f;
    private const float MAXIMUM_VELOCITY = 10f;
    private const float GRAVITY_MULTIPLIER = 0.5f;
    private const int FONT_SIZE = 50;
    private const float TIME_TO_LIVE = 0.5f;

    private Vector3 _currentVelocity;
    private float _constructTime;

    public void Constructor(int displayAmount, Vector3 startLocation)
    {
        transform.position = startLocation;
        Text.UpdateText(string.Format("<color=\"#{1}\">{0}</color>", displayAmount, displayAmount > 0 ? Constants.HealthGainColor : Constants.DamageColor), FONT_SIZE);
        _currentVelocity = new Vector2(Random.Range(-MAXIMUM_VELOCITY, MAXIMUM_VELOCITY), Random.Range(MINIMUM_VELOCITY, MAXIMUM_VELOCITY));
        _constructTime = Time.time;
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        var timeFactor = Time.time - _constructTime;
        if (timeFactor > TIME_TO_LIVE)
        {
            Destroy(this.gameObject);
        }
        else
        {
            var gravityEffect = GRAVITY_MULTIPLIER * Physics2D.gravity.y * timeFactor;
            _currentVelocity = _currentVelocity + new Vector3(0, gravityEffect);

            transform.position = transform.position + _currentVelocity;
        }
	}
}
