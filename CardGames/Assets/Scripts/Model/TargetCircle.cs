﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetCircle : MonoBehaviour {

    public SpriteRenderer TargetCircleSprite;
    public SpriteRenderer InsideTargetCircle;

    private const string SelectedColor  = "0CC81BFF";
    private const string DefaultColor   = "FFFFFFFF";
    private const string AttackingColor = "CC0000FF";
    private const string BlockingColor = "7070DDFF";
    private const string ExhaustedColor = "515151FF";

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void SetExhaust()
    {
        SetColor(Colors.HexToColor(ExhaustedColor));
    }

    public void SetAttacking()
    {
        SetColor(Colors.HexToColor(Constants.AttackColor));
    }

    public void SetBlocking()
    {
        SetColor(Colors.HexToColor(Constants.BlockingColor));
    }

    public void SetSelected()
    {
        SetColor(Colors.HexToColor(Constants.SelectedArrowColor));
    }

    public void SetSelectable()
    {
        SetColor(Colors.HexToColor(Constants.SelectableColor));
    }

    public void SetMouseOver()
    {
        SetColor(Colors.HexToColor(Constants.DefaultMouseOverColor));
    }

    public void SetSpecialActivated()
    {
        SetColor(Colors.HexToColor(Constants.SpecialActivatedColor));
    }

    public void SetOff()
    {
        var color = Colors.HexToColor(Constants.DisabledColor);
        color.a = 60;
        SetColor(color);
    }

    private void SetColor(Color color)
    {
        TargetCircleSprite.color = color;
        InsideTargetCircle.color = color;
    }
}