﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class BattleInformation
{
    public int NumberOfLocalPlayers;
    public int NumberOfComputerPlayers;

}
