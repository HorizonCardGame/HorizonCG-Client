﻿using UnityEngine;
using System.Collections;
using System.Text;

public class Lists
{
    public static string IEnumerableToString(IEnumerable list)
    {
        var sb = new StringBuilder();

        foreach(var obj in list)
        {
            sb.Append("\"" + obj.ToString() + "\", ");
        }

        return "[ " + sb.ToString().Substring(0, sb.ToString().Length - 2) + " ]";
    }
}
