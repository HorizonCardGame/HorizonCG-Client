﻿public struct Size
{
    public float Width { get; set; }
    public float Height { get; set; }

    public static Size GetNullSize()
    {
        return new Size { Width = -1, Height = -1 };
    }
}