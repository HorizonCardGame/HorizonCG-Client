﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : BaseBehaviour {

	public virtual State CurrentState
    {
        get { return _currentState; }
        set { Transition(value); }
    }

    protected State _currentState;
    protected bool _inTransition;

    public virtual T GetState<T>() where T : State
    {
        T target = GetComponent<T>();
        if(target == null)
        {
            target = gameObject.AddComponent<T>();
        }
        return target;
    }

    public virtual void ChangeState<T>() where T : State
    {
        CurrentState = GetState<T>();
    }

    public virtual void ChangeState(State state)
    {
        CurrentState = state;
    }

    public virtual bool IsCurrentState<T>() where T : State
    {
        return CurrentState.GetType().Equals(typeof(T));
    }

    protected virtual void Transition(State value)
    {
        if (_currentState == value || _inTransition)
            return;

        _inTransition = true;

        if(_currentState != null)
        {
            _currentState.Exit();
        }

        _currentState = value;

        if(_currentState != null)
        {
            _currentState.Enter();
        }

        _inTransition = false;
    }
}
