﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRig : MonoBehaviour {

    public float Speed = 3f;
    public Transform Follow;
    private Transform _transform;

	void Awake () {
        _transform = transform;
	}
	
	void Update () {
        if (Follow)
        {
            _transform.position = Vector3.Lerp(_transform.position, Follow.position, Speed * Time.deltaTime);
        }
	}
}
