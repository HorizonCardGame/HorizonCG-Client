﻿namespace TCG.Enums
{
    public enum Attribute
    {
        VIGOR,
        UNITY,
        WRATH,
        DECAY,
        PENANCE,
        AVARICE
    }

    public enum CardClass
    {
        FIGHTER,
        BRUISER,
        TANK,
        MAGE,
        SUPPORT,
        TECHNICIAN
    }

    public enum CardZones
    {
        Hand,
        Field,
        Discard,
        Deck,
        Stack,
        Banished
    }

    public enum CardType
    {
        UNIT,
        ABILITY,
        FAST,
        CHARM,
        RELIC,
        HERO,
        KEYWORD,
        LOCATION,
        ATTACHMENT,
        UNIQUE,
        RESTRICTED
    }

    public enum DamageTypes
    {
        Melee,
        Ranged,
        Fire,
        Cold,
        Shadow,
        Light,
        Acidic,
        Electric
    }

    public enum CardRarity
    {
        White,      //Common
        Green,      //Uncommon
        Blue,       //Rare
        Purple,     //Mythical
        Orange,     //Legendary
        Violet,     //Extrodianry
        Pink,       //Angelic
        Pearlescent //Pearlescent i.e. Promo
    }

    public enum ZoneTypes
    {
        NONE,
        DISCARD,
        DECK,
        BANISHED,
        AETHERIZED
    }

    public enum ComputerStates
    {
        Idle,
        StartOfTurn,
        AetherizeCards,
        PlayCards,
        Attack,
        FinishedAttack,
        EndOfTurn
    }

    public enum TimerStates
    {
        Pause,
        Run,
        Stop
    }

    public enum GameStates
    {
        Active,
        Wait
    }

    public enum VisualStates
    {
        Selectable,
        Selected,
        Off,
        Attacking,
        Exhausted,
        SpecialActivation
    }
}