﻿public enum CardTextProperties
{
    None = 1 << 0,
    PeriodAtEnd = 1 << 1,
    MiddleOf = 1 << 2,
    CommaAtEnd = 1 << 3,
    NoModification = 1 << 4
}
