﻿public static class EventNames
{
    public const string StartOfTurn = "StartOfTurn";
    public const string EndOfTurn = "EndOfTurn";
    public const string PlayedCard = "PlayedCard";
    public const string EnteredField = "EnteredField";
    public const string LeftField = "LeftField";
    public const string PutInDiscard = "PutInDiscard";
    public const string DrawsCard = "DrawsCard";
    public const string Banished = "Banished";
    public const string Aetherized = "Aetherized";

    public const string OnAttack = "OnAttack";
    public const string OnBlock = "OnBlock";
    public const string DealtDamage = "DealtDamage";
    public const string LostHealth = "LostHealth";
    public const string GainHealth = "GainHealth";
    public const string GainArmor = "GainArmor";

    public const string DisplayMessage = "DisplayMessage";
    public const string DisplayError = "DisplayError";
    public const string PreviewCard = "PreviewCard";
    public const string ShowOptionsMenu = "ShowOptionsMenu";
    public const string ShowOtherZone = "ShowOtherZone";
    public const string UpdateUI = "UpdateUI";

    public const string SetTargettingInfo = "SetTargettingInfo";
    public const string StopTargettingInfo = "StopTargettingInfo";

    //GamePhases
    public const string OnMainPhase = "OnMainPhase";
}
