﻿
public class Localization
{
    public const string CanNotPlayCard = "You do not have the Attributes or Aether to play this card.";
    public const string CanNotAetherizeMore = "Can not Aetherize more this turn.";
    public const string CurrentPlayersTurn = "Your Turn";
    public const string OtherPlayersTurn = "{0}'s Turn";
}
