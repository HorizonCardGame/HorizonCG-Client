﻿using System.Collections.Generic;
using TCG.Enums;
using UnityEngine;

public static class Constants
{
    public const float CardWidthHeightRatio = 248f / 348f;
    public const int DefaultNumberOfAetherizes = 1;
    public const float ReactTimerAmount = 2f;
    public const float TotalPlayerTimeInMinutes = 25.0f;
    public const string DatabaseFilePath = "devDatabaseFile.json";
    public const string DatabaseImageFolderPath = "downloads";
    public const string DatabaseImagePath = DatabaseImageFolderPath + "\\{0}.png";
    public static readonly List<Attribute> AttributesOrder = new List<Attribute> { Attribute.VIGOR, Attribute.UNITY, Attribute.WRATH, Attribute.DECAY, Attribute.PENANCE, Attribute.AVARICE };
    public static readonly List<Color> ColorsForPlayers = new List<Color> { Color.cyan, Color.red, Color.green, Color.yellow, Color.magenta, Color.blue, Color.black, Color.grey };

    //Action Construction System
    public const string ImplementedClassAction = "CardImplementations.Actions_{0}";

    //MessageSystem
    public const float StandardMessageTime = 3f;

    //Custom Characters
    public const string TextSpritesheetPath = "CustomText/TextSpritesheet_v0-1";
    public const string CCharacterPath = "CustomText/CCharacter";
    public const int TextCharacterWidth = 48;
    public const int TextCharacterHeight = 64;
    public const int TrueTextSpriteWidth = 200;
    public const int TrueTextSpriteHeight = 267;

    //TextSystem
    public const string HeroActionBreak = "<hr/>";

    //RightClick Menu
    public const string DefaultColor = "9c9c9cFF";
    public const string SelectedColor = "FF4000FF";
    public const string SelectableColor = "0C90C8FF";
    public const string DefaultMouseOverColor = "bcbcbcFF";
    public const string SpecialActivatedColor = "900CC8FF";
    public const string DisabledColor = "515151FF";
    public const string AttackColor = "C80C3FFF";
    public const string BlockingColor = "7070DDFF";

    //Colors
    public const string HealthGainColor = "3DD20BFF";
    public const string DamageColor = "C80C3FFF";
    public const string ErrorRed = "CC0000FF";
    public const string RarityCommon = "FFFFFF";
    public const string RarityUncommon = "3DD20B";
    public const string RarityRare = "2F78FF";
    public const string RarityPromo = "9132C8";
    public const string RaritySuperRare = "FF9600";
    public const string RarityViolet = "FF00FF";
    public const string RarityPink = "FF6CB4";
    public const string RarityPearlescent = "00FFFF";

    //Selection
    public const string DefaultArrowColor = "0C90C890";
    public const string SelectedArrowColor = "FF400090";
}