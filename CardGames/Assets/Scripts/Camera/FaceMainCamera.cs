﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceMainCamera : MonoBehaviour {

    void Update()
    {
        var target = Camera.main.transform;
        transform.LookAt(transform.position + target.rotation * Vector3.forward,
            target.transform.rotation * Vector3.up);
    }
}
