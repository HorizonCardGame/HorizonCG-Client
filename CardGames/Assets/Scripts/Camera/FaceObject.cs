﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceObject : MonoBehaviour {

    public Transform target;

    void Update()
    {
        transform.LookAt(transform.position + target.rotation * Vector3.forward,
            target.transform.rotation * Vector3.up);
    }
}
