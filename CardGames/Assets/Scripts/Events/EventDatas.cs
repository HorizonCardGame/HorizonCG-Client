﻿using System.Collections.Generic;
using TCG.Enums;
using UnityEngine;

public struct PlayCardEventData
{
    public Card Card { get; set; }
}

public struct EnteredFieldEventData
{
    public Card Card { get; set; }
}

public struct LeftFieldEventData
{
    public Card Card { get; set; }
    public Vector3 LastPosition { get; set; }
}

public struct PutInGraveyardData
{
    public Card Card { get; set; }
    public Vector3 Position { get; set; }
}

public struct BanishedData
{
    public Card Card { get; set; }
}

public struct DealtDamageEventData
{
    public object Source { get; set; }
    public int Amount { get; set; }
    public DamageTypes DamageType { get; set; }
}

public struct LostHealthEventData
{
    public object Source { get; set; }
    public int Amount { get; set; }
}

public struct GainHealthEventData
{
    public object Source { get; set; }
    public int Amount { get; set; }
}

public struct GainArmorEventData
{
    public object Source { get; set; }
    public int Amount { get; set; }
}

public struct OptionsMenuEventData
{
    public IEnumerable<RightClickItem> Items { get; set; }
    public Vector2 ClickPosition { get; set; }
}

public struct OtherCardZoneEventData
{
    public ZoneTypes Zone { get; set; }
    public IEnumerable<Card> Cards { get; set; }
}
