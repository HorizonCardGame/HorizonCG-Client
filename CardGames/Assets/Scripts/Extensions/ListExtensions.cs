﻿using System.Collections.Generic;
using System.Text;

public static class ListExtensions
{
    public static string ToStringExpanded<T>(this List<T> list)
    {
        var sb = new StringBuilder("[");

        foreach(var o in list)
        {
            sb.Append(o.ToString() + ", ");
        }

        return sb.ToString().Substring(0, sb.ToString().Length - 2) + "]";
    }
}