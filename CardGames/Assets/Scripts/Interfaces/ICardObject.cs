﻿using System.Collections;
using UnityEngine;

public interface ICardObject
{
    Material DefaultMaterial { get; }
    void ChangeMaterial(Material material);
    void ResetToDefaultMaterial();
    void UpdateCardState();
    void UpdateCardState(Card c);
    Transform GetTransform();
    Card GetCard();
    void Destroy();
    void Banish();
    void Aetherize();
    void OnStartOfTurn();
}