﻿using System.Collections.Generic;
using TCG.Enums;
using TCG.Utility;
using UnityEngine;
using UnityEngine.Events;

public interface IPlacable
{
    GameObject GetGameObject();
    void ChangePosition(Vector3 position);
    void ChangePosition(Vector3 position, Quaternion rotation);
    void Remove();
}

public interface IMovable
{
    void Move(Vector2 position);
}

public interface IDamageable
{
    void DealtDamage(object source, int amount, DamageTypes dt);
    Transform GetTransform();
}

public interface IPlayerController
{
    void Constructor(Color color);
    void OnStartOfTurn();
    void OnEndOfTurn();
}

public interface IPawn
{
    void Constructor(Card card, BasePlayerController owner);
    void ChangeOnClick(UnityAction<IPawn> onClick);
    void CancelOnClick();
    void SetVisualState(VisualStates state);
    void Attack();
    void AssignAttacking(IDamageable defender);
    void AssignBlocking(IPawn attacker);
    void AttackComplete();
    void BlockingComplete();
    void DealDamage(IDamageable target);
    void Transform(Card target);
    void Revert();
    void AdjustStatistics(JSONNode statistics);
    void Exhaust();
    void Recover();
    Card GetCard();
    BasePlayerController GetController();
    Transform GetTransform();
    GameObject GetGameObject();
    IDamageable GetDamageable();
    bool IsRemoved();
}

public interface ICardController
{
    void Constructor(Card card);
    void UpdateCardState();
    void UpdateCardFace();
    void AddedToAHand();
    void PlayCard();
    void Destroy();
    void MoveToDiscard();
    void Transform(Card target);
    void Revert();
    void AdjustStatistics(JSONNode statistics);
    void Exhaust();
    void Recover();
    void DealDamage(IDamageable target);
    void AttackComplete();
    Card GetCard();
    RectTransform GetRect();
    Transform GetTransform();
    GameObject GetGameObject();
    void SetSizeByHeight(float height);
    void SetSizeByWidth(float width);
    void SetSize(float width, float height);
}

public interface IDeck
{
    void Shuffle();
    IEnumerable<Card> DrawCard(int num = 1);
    void PutCardsInDeck(IEnumerable<Card> cards, int placement);
    int GetNumberOfCardsInDeck();
}

public interface IHandController
{
    void Constructor(BasePlayerController owner);
    void AddCardToHand(IEnumerable<Card> cards);
    void RemoveCardFromHand(ICardController cc);
}

public interface IPlayerWaitSystem
{
    void Reset();
    void Start();
    void Continue();
    void Finish();
}
