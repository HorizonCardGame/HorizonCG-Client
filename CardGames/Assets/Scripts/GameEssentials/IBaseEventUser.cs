﻿using System;
using UnityEngine.Events;

public interface IBaseEventUser
{
    Guid CreateId();
    Guid GetId();
    void Subscribe(string eventName, UnityAction<IBaseEventUser, object> action, string group = null);
    void Unsubscribe(string eventName, string group = null);
    void Publish(string eventName, object eventData, string group = null);

}
