﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Randomizer : MonoBehaviour {

    public System.Random Random { get; set; }

    public static Randomizer _instance;
    public static Randomizer Instance
    {
        get
        {
            return _instance;
        }
    }
    void GetInstance()
    {
        _instance = this;
    }

    // Use this for initialization
    public void Constructor() {
        GetInstance();

        Random = new System.Random();
	}
}
