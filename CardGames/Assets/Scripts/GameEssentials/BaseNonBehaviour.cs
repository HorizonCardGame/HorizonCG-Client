﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

public class BaseNonBehaviour : IBaseEventUser
{
    public Guid BaseId { get; private set; }

    public BaseNonBehaviour()
    {
        CreateId();
    }

    public Guid CreateId()
    {
        BaseId = Guid.NewGuid();
        return BaseId;
    }

    public Guid GetId()
    {
        return BaseId;
    }

    public void Publish(string eventName, object eventData, string group = null)
    {
        EventManager.Publish(this, eventName, eventData, group);
    }

    public void Subscribe(string eventName, UnityAction<IBaseEventUser, object> action, string group = null)
    {
        EventManager.Subscribe(this, eventName, action, group);
    }

    public void Unsubscribe(string eventName, string group = null)
    {
        EventManager.Unsubscribe(this, eventName, group);
    }
}
