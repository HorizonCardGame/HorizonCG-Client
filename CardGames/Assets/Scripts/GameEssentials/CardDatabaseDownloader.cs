﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using TCG.Utility;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Events;
using System.Net;
using System;

public class CardDatabaseDownloader : MonoBehaviour
{
    public string AirtableAutorization = "keyc7NV7u8oxNsAjM";
    public bool ReloadDatabase = false;
    private JSONNode _cardDatabase { get; set; }
    private JSONNode _cardSets { get; set; }
    private JSONNode _sets { get; set; }

    public bool CompletelyFinished { get { return totalCompleted == totalStarted && databaseDownloadFinished; } }
    private int totalStarted { get; set; }
    private int totalCompleted { get; set; }
    private bool databaseDownloadFinished { get; set; }

    public bool IsDatabaseReady()
    {
        return File.Exists(GetFilePath(Constants.DatabaseFilePath)) && !ReloadDatabase;
    }

    public void DownloadDatabase()
    {
        _cardDatabase = new JSONArray();
        _cardSets = new JSONArray();
        _sets = new JSONArray();
        totalStarted = 0;
        totalCompleted = 0;
        databaseDownloadFinished = false;

        DownloadCardDatabase();
    }

    IEnumerator GetFromAirtable(string url, UnityAction<string> onSuccess)
    {
        using (UnityWebRequest req = UnityWebRequest.Get(url))
        {
            req.SetRequestHeader("Authorization", "Bearer " + AirtableAutorization);
            yield return req.SendWebRequest();
            while (!req.isDone)
                yield return null;
            byte[] result = req.downloadHandler.data;
            string str = System.Text.Encoding.Default.GetString(result);
            //_onUpdate(string.Format("Downloaded from {0}. Response {1}", url, str));
            onSuccess(str);
        }
    }

    public void DownloadCardDatabase()
    {
        Directory.CreateDirectory(GetFilePath(Constants.DatabaseImageFolderPath));
        StartCoroutine(GetFromAirtable("https://api.airtable.com/v0/appO4CFqCg4WJohsy/Card%20Database?view=All", ProcessCardDatabase));
    }

    public void ProcessCardDatabase(string strJson)
    {
        var cdJSON = JSON.Parse(strJson);

        if (cdJSON.HasKey("records"))
        {
            foreach (JSONNode cardNode in cdJSON["records"].AsArray)
            {
                var fields = cardNode["fields"];
                fields["id"] = cardNode["id"];
                _cardDatabase.Add(fields);
            }
        }

        if (cdJSON.HasKey("offset"))
        {
            StartCoroutine(GetFromAirtable(string.Format("https://api.airtable.com/v0/appO4CFqCg4WJohsy/Card%20Database?view=All&offset={0}", cdJSON["offset"].Value), ProcessCardDatabase));
        }
        else
        {
            StartCoroutine(GetFromAirtable("https://api.airtable.com/v0/appO4CFqCg4WJohsy/CardSets?view=All", ProcessCardSets));
        }
    }

    public void ProcessCardSets(string strJson)
    {
        var csJSON = JSON.Parse(strJson);

        if (csJSON.HasKey("records"))
        {
            foreach (JSONNode cardSetNode in csJSON["records"].AsArray)
            {
                var fields = cardSetNode["fields"];
                var cardId = fields["Card"].AsArray[0].Value;
                fields["setId"] = fields["Set"].AsArray[0].Value;
                fields["cardId"] = cardId;
                fields["id"] = cardSetNode["id"].Value;
                _cardSets.Add(fields);

                if(fields["Card Image"] != null)
                {
                    Debug.Log(fields["Card Image"].AsArray[0]["url"].Value);
                    totalStarted++;
                    StartCoroutine(DownloadImageEnumerator(fields["Card Image"].AsArray[0]["url"].Value, fields["id"].QValue, ImageDownloadCompleted));
                }
            }
        }

        if (csJSON.HasKey("offset"))
        {
            StartCoroutine(GetFromAirtable(string.Format("https://api.airtable.com/v0/appO4CFqCg4WJohsy/CardSets?view=All&offset={0}", csJSON["offset"].Value), ProcessCardSets));
        }
        else
        {
            StartCoroutine(GetFromAirtable("https://api.airtable.com/v0/appO4CFqCg4WJohsy/Sets?view=All", ProcessSets));
        }
    }

    private IEnumerator DownloadImageEnumerator(string cardImageUrl, string cardSetId, UnityAction imageDownloadComplete)
    {
        var fileName = GetFilePath(string.Format(Constants.DatabaseImagePath, cardSetId));

        var imageURLWWW = new WWW(cardImageUrl);
        yield return imageURLWWW;


        var data = imageURLWWW.bytes;
        if (data != null && data.Length > 0)
        {
            if (!File.Exists(fileName))
            {
                File.Create(fileName);
            }
            File.WriteAllBytes(fileName, data);
        }

        imageDownloadComplete();
    }

    public void ProcessSets(string strJson)
    {
        var sJSON = JSON.Parse(strJson);

        if (sJSON.HasKey("records"))
        {
            foreach (JSONNode setNode in sJSON["records"].AsArray)
            {
                var fields = setNode["fields"];
                fields["id"] = setNode["id"].Value;
                _sets.Add(fields);
            }
        }

        if (sJSON.HasKey("offset"))
        {
            StartCoroutine(GetFromAirtable(string.Format("https://api.airtable.com/v0/appO4CFqCg4WJohsy/Sets?view=All&offset={0}", sJSON["offset"].Value), ProcessCardSets));
        }
        else
        {
            Finish();
        }
    }

    public void ImageDownloadCompleted()
    {
        totalCompleted++;
        CheckIfCompletelyFinished();
    }

    public void Finish()
    {
        var json = new JSONClass();
        json["Sets"] = _sets;
        json["CardSets"] = _cardSets;
        json["Cards"] = _cardDatabase;

        var filePath = GetFilePath(Constants.DatabaseFilePath);

        if (!File.Exists(filePath))
        {
            File.Create(filePath);
        }
        File.WriteAllText(filePath, json.ToString());
        Debug.Log("Database Download Finished");

        databaseDownloadFinished = true;
        CheckIfCompletelyFinished();
    }

    public void CheckIfCompletelyFinished()
    {
        if (totalCompleted == totalStarted && databaseDownloadFinished)
        {
            
        }
    }

    public string GetFilePath(string path)
    {
        return Path.Combine(Application.persistentDataPath, path);
    }
}
