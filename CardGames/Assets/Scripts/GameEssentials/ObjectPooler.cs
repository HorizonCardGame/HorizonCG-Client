﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {

	public GameObject GetPooledObject(GameObject obj, Transform parent = null)
    {
        foreach(Transform child in transform)
        {
            var goChild = child.gameObject;
            if (!goChild.activeInHierarchy && child.name == obj.name + "(Clone)")
            {
                child.transform.parent = parent;
                goChild.SetActive(true);
                return goChild;
            }
        }

        GameObject output = Instantiate(obj, parent);
        return output;
    }

    public void ReturnPoolObject(GameObject obj)
    {
        obj.transform.parent = this.transform;
        obj.transform.localPosition = Vector3.zero;
        obj.SetActive(false);
    }

    public void PreparePool(GameObject obj, int number)
    {
        for(var i = 0; i < number; i++)
        {
            GameObject output = (GameObject)Instantiate(obj, this.transform);
            ReturnPoolObject(output);
        }
    }
}
