﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using TCG.Utility;
using System;

public class EventManager : MonoBehaviour
{

    private Dictionary<string, GameEvent> eventDictionary;

    private static EventManager eventManager;

    public static EventManager Instance
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;

                if (!eventManager)
                {
                    Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                }
                else
                {
                    eventManager.Init();
                }
            }

            return eventManager;
        }
    }

    void Init()
    {
        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, GameEvent>();
        }
    }

    public static void Subscribe(IBaseEventUser baseEventUser, string eventName, UnityAction<IBaseEventUser, object> listener, string group = null)
    {
        GameEvent thisEvent = null;
        eventName = eventName + (group != null ? ":" + group : "");
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Add(baseEventUser.GetId(), listener);
        }
        else
        {
            thisEvent = new GameEvent();
            thisEvent.Add(baseEventUser.GetId(), listener);
            Instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void Unsubscribe(IBaseEventUser baseEventUser, string eventName, string group = null)
    {
        if (eventManager == null) return;
        GameEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Remove(baseEventUser.GetId());
        }

        if (!string.IsNullOrEmpty(group))
            Unsubscribe(baseEventUser, eventName + ":" + group);
    }

    public static void Publish(IBaseEventUser sender, string eventName, object arguments, string group = null)
    {
        GameEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(sender, arguments);
        }

        if (!string.IsNullOrEmpty(group))
            Publish(sender, eventName + ":" + group, arguments);
    }
}

public class GameEvent
{
    public Dictionary<Guid, UnityAction<IBaseEventUser, object>> Subscribers { get; set; }

    public GameEvent()
    {
        Subscribers = new Dictionary<Guid, UnityAction<IBaseEventUser, object>>();
    }

    public void Add(Guid baseId, UnityAction<IBaseEventUser, object> action)
    {
        if (Subscribers.ContainsKey(baseId))
        {
            Subscribers[baseId] = action;
        }
        else
        {
            Subscribers.Add(baseId, action);
        }
    }

    public void Remove(Guid baseId)
    {
        if (Subscribers.ContainsKey(baseId))
        {
            Subscribers.Remove(baseId);
        }
    }

    public void Invoke(IBaseEventUser sender, object arguments)
    {
        foreach(var key in Subscribers.Keys)
        {
            Subscribers[key].Invoke(sender, arguments);
        }
    }
}