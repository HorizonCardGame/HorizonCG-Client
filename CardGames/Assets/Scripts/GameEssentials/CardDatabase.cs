﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using TCG.Utility;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Events;
using System.IO;
using System;
using System.Linq;

public class CardDatabase : MonoBehaviour
{
    private Dictionary<string, JSONNode> _cardDatabase { get; set; }
    private Dictionary<string, JSONNode> _cardSets { get; set; }
    private Dictionary<string, JSONNode> _sets { get; set; }
    private Dictionary<string, Sprite> _cardImagePool { get; set; }

    private UnityAction<string> _onUpdate { get; set; }
    private UnityAction _onFinish { get; set; }

    private int numberStarted { get; set; }
    private int numberCompleted { get; set; }
    private bool dictionariesLoaded { get; set; }

    public bool CompletelyFinished { get { return numberStarted == numberCompleted && dictionariesLoaded; } }

    public static CardDatabase _instance;
    public static CardDatabase Instance
    {
        get
        {
            return _instance;
        }
    }
    void GetInstance()
    {
        _instance = this;
    }

    public void Constructor()
    {
        GetInstance();

        //_onUpdate = onUpdate;
        //_onFinish = onFinish;

        _cardDatabase = new Dictionary<string, JSONNode>();
        _cardSets = new Dictionary<string, JSONNode>();
        _sets = new Dictionary<string, JSONNode>();
        _cardImagePool = new Dictionary<string, Sprite>();

        DownloadCardDatabase();
    }

    public const string TestCardJson = "[" +
        "{ \"Name\": \"Test Hero\", \"Set\":\"Testers\", \"Cost\":1, \"Attribute\":[], \"Class\":[], \"Type\":\"Hero\", \"Subtype\":\"Blob\", \"Attack\":0, \"DamageType\":\"Melee\", \"Defense\":5, \"Text\":\"\", \"Image\":\"TesterHero\"}," +
        "{ \"Name\": \"Test Unit\", \"Set\":\"Testers\", \"Cost\":1, \"Attribute\":\"Unity,Decay,Avarice\", \"Class\":\"Bruiser,Mage,Technician\", \"Type\":\"Unit\", \"Subtype\":\"Blob\", \"Attack\":3, \"DamageType\":\"Melee\", \"Defense\":3, \"Text\":\"\", \"Image\":\"Ooze-Card-Image-01\"}," +
        "{ \"Name\": \"Test Unit 2\", \"Set\":\"Testers\", \"Cost\":1, \"Attribute\":\"Vigor,Wrath,Penance\", \"Class\":\"Fighter,Tank,Support\", \"Type\":\"Unit\", \"Subtype\":\"Blob\", \"Attack\":3, \"DamageType\":\"Ranged\", \"Defense\":3, \"Text\":\"\", \"Image\":\"Ooze-Card-Image-01\"}," +
    "]";

    IEnumerator GetFromAirtable(string url, UnityAction<string> onSuccess)
    {
        using (UnityWebRequest req = UnityWebRequest.Get(url))
        {
            req.SetRequestHeader("Authorization", "Bearer keyc7NV7u8oxNsAjM");
            yield return req.SendWebRequest();
            while (!req.isDone)
                yield return null;
            byte[] result = req.downloadHandler.data;
            string str = System.Text.Encoding.Default.GetString(result);
            _onUpdate(string.Format("Downloaded from {0}. Response {1}", url, str));
            onSuccess(str);
        }
    }

    public void DownloadCardDatabase()
    {
        if (!File.Exists(GetFilePath(Constants.DatabaseFilePath)))
        {
            throw new System.Exception("You must download database first before running");
        }

        var fileStr = File.ReadAllText(GetFilePath(Constants.DatabaseFilePath));
        var jsonClass = JSON.Parse(fileStr);

        //Card Database
        foreach(JSONClass node in jsonClass["Cards"].AsArray)
        {
            _cardDatabase.Add(node["Name"], node);
        }

        //Card Sets Database
        foreach(JSONClass node in jsonClass["CardSets"].AsArray)
        {
            _cardSets.Add(node["id"], node);

            if(node["Card Image"] != null)
            {
                numberStarted++;
                StartCoroutine(GetCardImageEnumerator(node["id"], ImageLoaded_Callback));
            }
        }

        //Sets Database
        foreach(JSONClass node in jsonClass["Sets"].AsArray)
        {
            _sets.Add(node["id"], node);
        }

        dictionariesLoaded = true;
    }

    private void ImageLoaded_Callback()
    {
        numberCompleted++;
    }

    public Card GetCard(string name, string set = null)
    {
        if (!_cardDatabase.ContainsKey(name))
        {
            throw new Exception(string.Format("Card Database is missing \"{0}\" from set \"{1}\"", name, set));
        }

        var cardNode = _cardDatabase[name];
        JSONNode setNode = null; JSONNode csNode = null;

        if (set != null)
        {
            foreach (JSONNode node in cardNode["CardSets"].AsArray)
            {
                var id = node.Value;
                csNode = _cardSets[id];
                var setN = GetSetFromId(csNode["Set"][0]);
                if (setN != null && setN[name].Value.Equals(set))
                {
                    setNode = setN;
                }
            }
        }

        if (setNode == null)
        {
            csNode = _cardSets[cardNode["CardSets"][0]];
            setNode = GetSetFromId(csNode["Set"][0]);
        }

        return new Card(cardNode, csNode, setNode);
    }

    public JSONNode GetSetFromId(string id)
    {
        return _sets[id];
    }

    public Sprite GetCardImage(string cardSetId)
    {
        return _cardImagePool[cardSetId];
    }

    private IEnumerator GetCardImageEnumerator(string cardSetId, UnityAction callback)
    {
        var fileName = Path.Combine(Application.persistentDataPath, string.Format(Constants.DatabaseImagePath, cardSetId));

        var imageFileWWW = new WWW(fileName);
        yield return imageFileWWW;

        if (imageFileWWW.texture != null && imageFileWWW.texture.width == 744 && imageFileWWW.texture.height == 1044)
        {
            Sprite sprite = Sprite.Create(imageFileWWW.texture, new Rect(0, 0, 744, 1044), Vector2.zero);
            if (!_cardImagePool.ContainsKey(cardSetId))
            {
                _cardImagePool.Add(cardSetId, sprite);
            }
        }

        callback();
    }

    public static string GetFilePath(string path)
    {
        return Path.Combine(Application.persistentDataPath, path);
    }
}