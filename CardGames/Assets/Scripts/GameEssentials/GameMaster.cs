﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TCG.Enums;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.Analytics;
using TCG.Utility;

[RequireComponent(typeof(StackReponseResolutionController))]
[RequireComponent(typeof(CardDatabaseDownloader))]
[RequireComponent(typeof(CardDatabase))]
[RequireComponent(typeof(ObjectPooler))]
public class GameMaster : StateMachine {

    public BasePlayerController LocalPlayer;
    public BasePlayerController ComputerPlayer;
    public PawnController DefaultPawn;
    public CardController DefaultBasicCard;
    public ActionVisualController ActionVisualPrefab;
    public Button CurrentPlayersEndButton;
    public GameObject DebugPanel;
    public Camera MainCamera;
    public LineRenderer PrefabArc;

    public StackReponseResolutionController Stack;
    private ObjectPooler Pooler;

    [SerializeField]
    private GameObject DraggableUI;
    [SerializeField]
    private RectTransform PlayArea;
    [SerializeField]
    private RectTransform AetherizedArea;

    public int CurrentTurn { get; set; }
    public BasePlayerController CurrentPlayer { get { return PlayerList[CurrentPlayerIndex].Player; } }
    public int CurrentPlayerIndex { get; set; }
    public List<PlayerNode> PlayerList = new List<PlayerNode>();

    public static GameMaster _instance;
    public static GameMaster Instance
    {
        get
        {
            return _instance;
        }
    }
    void GetInstance()
    {
        _instance = this;
    }

    public List<CardNode> CardsInPlay = new List<CardNode>();
    private GameObject GameItems { get; set; }
    private List<GameObject> DestroyedObjects { get; set; }

    // Use this for initialization
    void Start()
    {
        GetInstance();
        Stack = GetComponent<StackReponseResolutionController>();
        Pooler = GetComponent<ObjectPooler>();
        GetComponent<Randomizer>().Constructor();
        CurrentPlayersEndButton.onClick.AddListener(PhaseChangeButton_Clicked);
        DestroyedObjects = new List<GameObject>();

        Pooler.PreparePool(DefaultPawn.gameObject, 5);
        Pooler.PreparePool(DefaultBasicCard.gameObject, 10);
        Pooler.PreparePool(ActionVisualPrefab.gameObject, 5);

        ChangeState<InitBattleState>();
    }

    public void AddToTheStack(IStackData data)
    {
        Stack.Add(data);
    }

    public void StateBaseActions()
    {
        Publish(EventNames.UpdateUI, null);
    }

    public CardController CreateCardController(Card card)
    {
        CardController output = Instantiate(DefaultBasicCard);
        output.AlwaysShowFaceUp = true;
        output.DisableHandMouseOverBehavior = true;
        output.Constructor(card);

        return output;
    }
    public CardController CreateCardController(Card card, bool isFaceUp = false, bool disable = false)
    {
        CardController output = Instantiate(DefaultBasicCard);
        output.Constructor(card, isFaceUp, disable);

        return output;
    }

    #region PlayerInformation
    public bool SetCurrentPlayer(int index)
    {
        if (CurrentPlayerIndex >= PlayerList.Count)
        {
            CurrentPlayerIndex = 0;
            return true;
        }
        else
        {
            return false;
        }
    }

    public List<BasePlayerController> GetAllPlayers()
    {
        return PlayerList.Select(y => y.Player).ToList();
    }

    public List<BasePlayerController> GetEnemyPlayers(BasePlayerController askingPlayer)
    {
        PlayerNode apNode = PlayerList.Where(p => p.Player == askingPlayer).FirstOrDefault();
        return PlayerList.Where(x => x != apNode && !apNode.TeamMembers.Contains(x.Player))
                           .Select(y => y.Player).ToList();
    }

    public List<BasePlayerController> GetAllyPlayers(BasePlayerController askingPlayer)
    {
        PlayerNode apNode = PlayerList.Where(p => p.Player == askingPlayer).FirstOrDefault();
        return PlayerList.Where(x => x != apNode && apNode.TeamMembers.Contains(x.Player))
                           .Select(y => y.Player).ToList();
    }
    #endregion

    #region Overlays
    private bool _isDebugPanelShown = false;
    public void ToggleDebugPanel()
    {
        if (!_isDebugPanelShown)
        {
            DebugPanel.SetActive(true);
            _isDebugPanelShown = true;
        }
        else
        {
            DebugPanel.SetActive(false);
            _isDebugPanelShown = false;
        }
    }

    public void ShowDraggableUI()
    {
        DraggableUI.SetActive(true);
    }
    public void HideDraggableUI()
    {
        DraggableUI.SetActive(false);
    }

    public Bounds GetPlayAreaBounds()
    {
        var size = PlayArea.GetSize();
        return new Bounds { center = new Vector3(PlayArea.position.x, PlayArea.position.y), size = new Vector3(size.x, size.y) };
    }
    public Bounds GetAetherizedAreaBounds()
    {
        var size = AetherizedArea.GetSize();
        return new Bounds { center = new Vector3(AetherizedArea.position.x, AetherizedArea.position.y), size = new Vector3(size.x, size.y) };
    }

    #endregion

    #region EndTurnButton

    public int CurrentNumberOfCombatPhases = 0;
    public int MaxCombatPhases = 1;

    public bool ShouldGoToCombat()
    {
        return CurrentNumberOfCombatPhases < MaxCombatPhases;
    }

    private UnityAction _buttonClickAction;
    public void ChangeButtonLook(string text, Color color, Color highlighted, Color pressed, UnityAction action)
    {
        CurrentPlayersEndButton.gameObject.SetActive(true);
        CurrentPlayersEndButton.colors = new ColorBlock() { normalColor = color,
                                                            highlightedColor = highlighted,
                                                            pressedColor = pressed,
                                                            disabledColor = Color.gray,
                                                            colorMultiplier = 1
                                                        };
        CurrentPlayersEndButton.GetComponentInChildren<Text>().text = text;
        _buttonClickAction = action;
    }

    public void DisableButton()
    {
       CurrentPlayersEndButton.gameObject.SetActive(false);
    }

    private void PhaseChangeButton_Clicked()
    {
        if(_buttonClickAction != null)
            _buttonClickAction.Invoke();
    }
    #endregion

    private void Update()
    {
        if((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyDown(KeyCode.D))
        {
            ToggleDebugPanel();
        }
    }

    #region BattleState Utility
    private BattleState _finishState;
    public void PhaseChange<T>() where T : BattleState
    {
        _finishState = GetState<T>();
        StartReactionSequence(CurrentPlayer, Finish_PhaseChange);
    }

    public void Finish_PhaseChange()
    {
        ChangeState(_finishState);
    }

    #endregion

    #region Get Reaction Sequence
    private int _startingIndex = 1;
    private int _currentIndex = -1;
    private UnityAction _reactionCallback;
    public void StartReactionSequence(BasePlayerController priorityPlayer, UnityAction callback)
    {
        _startingIndex = PlayerList.IndexOf(PlayerList.FirstOrDefault(x => x.Player == priorityPlayer));
        _currentIndex = -1;
        _reactionCallback = callback;
        StartCoroutine(Begin());
    }

    IEnumerator Begin()
    {
        yield return null;
        Continue();
    }

    public void Continue()
    {
        if (_currentIndex == _startingIndex)
        {
            Finish();
            return;
        }
        else if (_currentIndex < 0)
        {
            _currentIndex = _startingIndex;
        }

        var currentPlayer = PlayerList[_currentIndex];

        if (++_currentIndex >= PlayerList.Count)
        {
            _currentIndex = 0;
        }

        currentPlayer.Player.StartReactionTimer(Continue);
    }

    public void Break()
    {
        
    }

    public void Finish()
    {
        _startingIndex = -1;
        _currentIndex = -1;
        _reactionCallback();
        _reactionCallback = null;
    }


    #endregion

    #region Utility
    public void Destroy(GameObject obj)
    {
        Pooler.ReturnPoolObject(obj);
    }

    public void DestroyChildren(Transform parent)
    {
        var children = new List<GameObject>();
        foreach (Transform child in parent) { children.Add(child.gameObject); }
        children.ForEach(x => Destroy(x));
    }

    public void RunCoroutine(IEnumerator enumerator)
    {
        StartCoroutine(enumerator);
    }

    public GameObject Instantiate(GameObject obj, Transform parent = null)
    {
        return Pooler.GetPooledObject(obj, parent);
    }
    
    public new T Instantiate<T>(T obj, Transform parent = null) where T : Component
    {
        var go = Instantiate(obj.gameObject, parent);

        return go.GetComponent<T>();
    }

    public GameObject Instantiate(string path, Vector3 position, float timeToLive = -1f)
    {
        var objectToInstantiate = Resources.Load<GameObject>(path);
        var instantiated = Instantiate(objectToInstantiate, this.transform);
        instantiated.transform.position = position;
        if (timeToLive > -1)
        {
            Destroy(instantiated, timeToLive);
        }

        return instantiated;
    }

    public CardController InstantiateCardController()
    {
        var instantiated = Instantiate(DefaultBasicCard.gameObject);

        return instantiated.GetComponent<CardController>();
    }

    public CardController InstantiateCardController(Transform parent)
    {
        var instantiated = Instantiate(DefaultBasicCard.gameObject, parent);

        return instantiated.GetComponent<CardController>();
    }

    public PawnController InstantiatePawn()
    {
        var instantiated = Instantiate(DefaultPawn.gameObject);

        return instantiated.GetComponent<PawnController>();
    }

    public PawnController InstantiatePawn(Transform parent)
    {
        var instantiated = Instantiate(DefaultPawn.gameObject, parent);

        return instantiated.GetComponent<PawnController>();
    }

    public ActionVisualController InstantiateActionVisual(Transform parent)
    {
        var instantiated = Instantiate(ActionVisualPrefab.gameObject, parent);

        return instantiated.GetComponent<ActionVisualController>();
    }
    #endregion

    #region Arc
    private List<GameObject> _arcs = new List<GameObject>();
    private const float ARC_WIDTH = 0.25f;
    private const float ARC_HEIGHT = 9f;
    private const string BattleArcsObjectName = "BattleArcs";

    public GameObject MakeArc(Vector3 startingPos, Vector3 endingPos, Color color, int accuracy = 20)
    {
        var archInstance = Instantiate(PrefabArc, this.transform);
        var arc = archInstance.GetComponent<LineRenderer>();
        arc.transform.position = startingPos + (endingPos - startingPos) / 2;
        var curve = new AnimationCurve();
        curve.AddKey(0.0f, ARC_WIDTH);
        curve.AddKey(1.0f, ARC_WIDTH);
        arc.widthCurve = curve;
        arc.widthMultiplier = 1.0f;
        arc.useWorldSpace = true;
        arc.positionCount = accuracy;
        //arc.material = new Material(arc.material);
        arc.material.color = color;

        var listPos = new List<Vector3>();
        listPos.Add(startingPos);
        var difference = endingPos - startingPos;

        for(var i = 0; i < accuracy - 2; ++i)
        {
            float acc = (1.0f *i) / (accuracy - 2);
            float x = startingPos.x + (difference.x * acc);
            float y = (i > (accuracy - 2) / 2.0f) ? ARC_HEIGHT * (1.0f - acc) : ARC_HEIGHT * acc; //TODO: Make a more proper equation
            float z = startingPos.z + (difference.z * acc);
            listPos.Add(new Vector3(x, y, z));
        }
        listPos.Add(endingPos);
        arc.SetPositions(listPos.ToArray());

        return archInstance.gameObject;
    }

    public void ConstructBattleArcs(List<Tuple<IPawn, IDamageable>> attackers)
    {
        if(_arcs == null)
        {
            _arcs = new List<GameObject>();
        }
        else
        {
            for(var i = 0; i < _arcs.Count; ++i)
            {
                Destroy(_arcs[i]);
            }
            _arcs.Clear();
        }

        foreach(var tuple in attackers)
        {
            var color = (tuple.Second is BasePlayerController) ? Colors.HexToColor(Constants.AttackColor) : Colors.HexToColor(Constants.BlockingColor);
            _arcs.Add(MakeArc(tuple.First.GetTransform().position, tuple.Second.GetTransform().position, color));
        }
    }

    public void ClearBattleArcs()
    {
        for (var i = 0; i < _arcs.Count; ++i)
        {
            Destroy(_arcs[i]);
        }
        _arcs.Clear();
    }
    #endregion
}

public class CardNode
{
    public Card Card { get; set; }
    public PawnController Pawn { get; set; }
    public BasePlayerController Player { get; set; }

    public CardNode(Card c, PawnController p, BasePlayerController pl)
    {
        this.Card = c;
        this.Pawn = p;
        this.Player = pl;
    }
}

public class PlayerWaitSystem<T> : IPlayerWaitSystem
{
    private List<BasePlayerController> _playerList;
    private int _startingIndex = 1;
    private int _currentIndex = -1;
    public T Payload { get; set; }

    private UnityAction<T> OnFinish;

    public PlayerWaitSystem(T payload, List<BasePlayerController> players, int startingIndex, UnityAction<T> onFinish)
    {
        Payload = payload;
        _playerList = players;
        _startingIndex = startingIndex;
        _currentIndex = -1;
        OnFinish = onFinish;
    }

    public void Reset()
    {
        _currentIndex = -1;
    }

    public void Start()
    {
        Continue();
    }

    public void Continue()
    {
        if (_currentIndex == _startingIndex)
        {
            Finish();
            return;
        }
        else if(_currentIndex < 0)
        {
            _currentIndex = _startingIndex;
        }
        
        var currentPlayer = _playerList[_currentIndex];

        if (++_currentIndex >= _playerList.Count)
        {
            _currentIndex = 0;
        }

        currentPlayer.StartReactionTimer(Continue);
    }

    public void Finish()
    {
        OnFinish(Payload);
    }
}