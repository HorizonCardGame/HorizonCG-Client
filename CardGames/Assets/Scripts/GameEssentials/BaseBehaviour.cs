﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using TCG.Utility;
using System.Collections.Generic;
using System.Linq;
using System;

public class BaseBehaviour : MonoBehaviour, IBaseEventUser
{
    public Guid BaseId { get; private set; }
    public TargettingSystemController TargetSystem { get; set; }

    protected bool ShouldBeTarget { get; set; }

    private List<Tuple<string, UnityAction<IBaseEventUser, object>>> _events = new List<Tuple<string, UnityAction<IBaseEventUser, object>>>();

    // Use this for initialization
    protected virtual void Awake()
    {
        TargetSystem = FindObjectOfType(typeof(TargettingSystemController)) as TargettingSystemController;
        Subscribe(EventNames.SetTargettingInfo, SetTargetingInfo);
        Subscribe(EventNames.StopTargettingInfo, StopTargetinInfo);
        CreateId();
    }

    public void Destroy(GameObject obj)
    {
        GameMaster.Instance.Destroy(obj);
    }

    protected virtual void OnEnable()
    {
        foreach(var ent in _events)
        {
            EventManager.Subscribe(this, ent.First, ent.Second);
        }
    }

    protected virtual void OnDisable()
    {
        foreach (var ent in _events)
        {
            EventManager.Unsubscribe(this, ent.First);
        }
    }

    public void Subscribe(string eventName, UnityAction<IBaseEventUser, object> action, string group = null)
    {
        _events.Add(new Tuple<string, UnityAction<IBaseEventUser, object>>(eventName+(group != null ? ":" + group : ""), action));
        EventManager.Subscribe(this, eventName, action, group);
    }

    public void Unsubscribe(string eventName, string group = null)
    {
        var ent = _events.Where(e => e.First == eventName + (group != null ? ":" + group : "")).FirstOrDefault();
        if(ent != null)
        {
            EventManager.Unsubscribe(this, ent.First);
        }
    }

    public void Publish(string eventName, object args, string group = null)
    {
        EventManager.Publish(this, eventName, args, group);
    }

    public void SetTargetingInfo(IBaseEventUser sender, object data)
    {
        SetTargetingInfo(sender, (TargettingSystemInfo)data);
    }

    public virtual void SetTargetingInfo(IBaseEventUser sender, TargettingSystemInfo targettingInfo)
    {

    }

    public virtual void StopTargetinInfo(IBaseEventUser sender, object data)
    {
        ShouldBeTarget = false;
    }

    public Guid CreateId()
    {
        BaseId = Guid.NewGuid();

        return BaseId;
    }

    public Guid GetId()
    {
        return BaseId;
    }

    public override string ToString()
    {
        return "Id " + GetId() + " (" + this.GetType() + ")";
    }
}
