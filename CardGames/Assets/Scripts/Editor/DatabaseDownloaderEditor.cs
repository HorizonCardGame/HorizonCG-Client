﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CardDatabaseDownloader))]
public class DatabaseDownloaderEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        //if (GUILayout.Button("Download Database")) {
        //    CardDatabaseDownloader downloader = target as CardDatabaseDownloader;
        //    downloader.DownloadDatabase();
        // }
    }
}
