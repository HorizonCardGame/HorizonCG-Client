﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TCG.Utility;
using System.Text;

public class CText : MonoBehaviour {

    private CCharacter CCharacterPrefab { get; set; }
    [SerializeField]
    [TextArea(5,20)]
    private string Text = "Sample Text";
    [SerializeField]
    private float FontSize = 16f;
    public Size TrueFontSize { get; private set; }
    [SerializeField]
    [Range(0.1f, 2f)]
    private float NewLineBreakRatio = 0.5f;
    [SerializeField]
    private VerticalAlignment Vertical = VerticalAlignment.Top;
    [SerializeField]
    private HorizontalAlignment Horizontal = HorizontalAlignment.Left;
    public bool ResizeToFit = false;
    private float MinimumSize = -1;
    public Color DefaultColor = Color.black;
    public bool HasOutline = false;
    public Color DefaultOutlineColor = Color.black;
    public float DefaultOutlineSize = 1f;
    public UnityAction[] MouseOverActions;
    public UnityAction[] ClickedMouseActions;

    public RectTransform rectTransform { get; set; }
    public Size PreferredSize { get; set; }
    public Size CurrentSize { get; set; }
    private List<List<Word>> _trueText { get; set; }
    private int[] _wordsPerLine { get; set; }
    private string _customTextHolderName = "Custom Text Holder";

	// Use this for initialization
	void Awake () {
        Construct(true);
	}

    public void UpdateText(string text)
    {
        UpdateText(text, FontSize);
    }

    public void UpdateText(string text, float fontSize)
    {
        Text = text;
        FontSize = fontSize;
        Construct(true);
    }

    public void Resize(Size newSize)
    {
        rectTransform.SetHeight(newSize.Height);
        rectTransform.SetWidth(newSize.Width);
        Construct(false);
    }

    public void Resize(Vector3 newScale)
    {
        var w = rectTransform.GetWidth() * newScale.y;
        var h = rectTransform.GetHeight() * newScale.x;

        Resize(new Size() { Width = w, Height = h });
    }

    public void Construct(bool recontrustText)
    {
        if(rectTransform == null)
        {
            rectTransform = GetComponent<RectTransform>();
            CCharacterPrefab = Resources.Load<CCharacter>(Constants.CCharacterPath);
        }
        if (transform.Find(_customTextHolderName))
        {
            DestroyImmediate(transform.Find(_customTextHolderName).gameObject);
        }
        var customTextHolder = new GameObject(_customTextHolderName);

        var targetTransform = customTextHolder.transform;
        targetTransform.parent = transform;
        targetTransform.localPosition = Vector3.zero;
        targetTransform.localScale = transform.localScale;

        if (recontrustText)
        {
            _trueText = ParseText(this.Text);
        }

        TrueFontSize = GetProperFontSize();
        var newLineSpacing = TrueFontSize.Height * NewLineBreakRatio;
        var targetLine = new Line(); var lines = new List<Line>();
        for (var h =0; h < _trueText.Count; h++)
        {
            var line = _trueText[h];
            var currentWordLength = 0;
            foreach(var word in line)
            {
                targetLine.Add(word);

                if (++currentWordLength >= _wordsPerLine[lines.Count(x => x.GetType() != typeof(LineBreak))])
                {
                    currentWordLength = 0;
                    lines.Add(targetLine);
                    targetLine = new Line();
                }
            }

            if(h != _trueText.Count - 1)
                lines.Add(new LineBreak());
        }

        var totalLineHeight = lines.Sum(x => (x.GetType() == typeof(LineBreak)) ? newLineSpacing : TrueFontSize.Height);
        var heightOffset = (Vertical == VerticalAlignment.Bottom) ? totalLineHeight :
                           (Vertical == VerticalAlignment.Center) ? totalLineHeight/2 + rectTransform.GetHeight()/2 :
                           rectTransform.GetHeight();
        var currentHeight = TrueFontSize.Height;
        for(var j = 0; j < lines.Count; j++)
        {
            var line = lines[j];
            if (line.GetType() == typeof(LineBreak))
            {
                currentHeight += newLineSpacing;
            }
            else
            {
                var currentWidth = 0f;
                var widthOffset = (Horizontal == HorizontalAlignment.Right) ? rectTransform.GetWidth() - line.WidthAt(TrueFontSize.Height) :
                                  (Horizontal == HorizontalAlignment.Center) ? (rectTransform.GetWidth() / 2) - (line.WidthAt(TrueFontSize.Height) / 2) :
                                  0f;
                for (var i = 0; i < line.Words.Count; i++)
                {
                    var word = line.Words[i];
                    foreach (var character in word.Characters)
                    {
                        if (character.GetType() == typeof(MouseActions))
                        {

                        }
                        else if (character.GetType() == typeof(ClickableCharacters))
                        {

                        }
                        else
                        {
                            var cc = Instantiate(CCharacterPrefab, targetTransform);
                            cc.Constructor(character.Value, TrueFontSize.Height, character.Style, character.Color, HasOutline, DefaultOutlineSize, DefaultOutlineColor);
                            cc.SetLocalPosition(new Vector2(widthOffset + currentWidth, heightOffset - currentHeight));
                            currentWidth += cc.GetSize().Width;
                        }
                    }

                    if (i != line.Words.Count - 1)
                    {
                        var space = Instantiate(CCharacterPrefab, targetTransform);
                        space.Constructor(" ", TrueFontSize.Height);
                        space.SetLocalPosition(new Vector2(widthOffset + currentWidth, heightOffset - currentHeight));
                        currentWidth += space.GetSize().Width;
                    }
                }

                currentHeight += TrueFontSize.Height;
            }
        }
        
    }

    private List<List<Word>> ParseText(string text)
    {
        var output = new List<List<Word>>();
        var charArray = Resources.LoadAll<Sprite>(Constants.TextSpritesheetPath);

        var splitLines = text.Split(new string[] { "\n" }, StringSplitOptions.None);
        foreach(var splits in splitLines)
        {
            var line = new List<Word>();
            var word = new Word(charArray);
            var carryOver = "";
            var style = new Stack<TextStyle>(); style.Push(TextStyle.Normal);
            var color = new Stack<Color>(); color.Push(DefaultColor);
            foreach (char character in splits)
            {
                if(character == '<' || carryOver.Length > 0)
                {
                    carryOver += character;

                    if (character == '>')
                    {
                        if(carryOver.Equals("<b>"))
                        {
                            style.Push(TextStyle.Bold);
                        }
                        else if (carryOver.Equals("</b>"))
                        {
                            if (style.Peek() == TextStyle.Bold)
                                style.Pop();
                            else
                                throw new Exception("Trying to put an </b> before starting tag.");
                        }
                        else if (carryOver.Equals("<i>"))
                        {
                            style.Push(TextStyle.Italic);
                        }
                        else if (carryOver.Equals("</i>"))
                        {
                            if (style.Peek() == TextStyle.Italic)
                                style.Pop();
                            else
                                throw new Exception("Trying to put an </i> before starting tag.");
                        }
                        else if (carryOver.Contains("<color=\""))
                        {
                            var colorTag = carryOver.Replace("<color=\"", "").Replace("\">", "");
                            if (colorTag.Contains("#"))
                            {
                                color.Push(Colors.HexToColor(colorTag));
                            }
                            else
                            {
                                var colors = colorTag.Split(',');
                                if (colors.Length == 4)
                                    color.Push(new Color(float.Parse(colors[0]), float.Parse(colors[1]), float.Parse(colors[2]), float.Parse(colors[3])));
                                else
                                    color.Push(new Color(float.Parse(colors[0]), float.Parse(colors[1]), float.Parse(colors[2])));
                            }
                        }
                        else if (carryOver.Equals("</color>"))
                        {
                            if (color.Count > 1)
                                color.Pop();
                            else
                                throw new Exception("Trying to put an </color> before starting tag.");
                        }
                        else if (carryOver.Contains("<mouseOver=\""))
                        {
                            word.Add(new MouseOverCharacters(MouseOverActions[int.Parse(carryOver.Replace("<mouseOver=\"", "").Replace("\">", ""))], false));
                        }
                        else if (carryOver.Equals("</mouseOver>"))
                        {
                            word.Add(new MouseOverCharacters(null, true));
                        }
                        else if (carryOver.Contains("<click=\""))
                        {
                            word.Add(new ClickableCharacters(ClickedMouseActions[int.Parse(carryOver.Replace("<click=\"", "").Replace("\">", ""))], false));
                        }
                        else if (carryOver.Equals("</click>"))
                        {
                            word.Add(new ClickableCharacters(null, true));
                        }
                        else
                        {
                            word.Add(new Character(carryOver, Color.white, TextStyle.Normal));
                        }

                        carryOver = "";
                    }
                }
                else if(character == ' ')
                {
                    if(word.Length > 0)
                        line.Add(word);

                    word = new Word(charArray);
                }
                else
                {
                    word.Add(new Character(character.ToString(), color.Peek(), style.Peek()));
                }
            }
            line.Add(word);
            output.Add(line);

            style.Clear(); color.Clear();
        }

        return output;
    }

    private Size GetProperFontSize()
    {
        var output = Size.GetNullSize();
        var nextSize = FontSize; var firstOne = true;
        while(output.Width == -1)
        {
            var currentHeight = GetTotalHeightAtFontSize(nextSize);
            if (firstOne)
            {
                PreferredSize = new Size() { Height = currentHeight.First, Width = rectTransform.GetWidth() };
                firstOne = false;
            }
            CurrentSize = new Size() { Height = currentHeight.First, Width = rectTransform.GetWidth() };

            if (currentHeight.First <= rectTransform.GetHeight())
            {
                output.Height = nextSize;
                output.Width = CCharacter.GetFontWidth(nextSize);
                _wordsPerLine = currentHeight.Second;
            }
            else
            {
                var difference = currentHeight.First - rectTransform.GetHeight();
                nextSize -= difference / currentHeight.Second.Length;

                if(MinimumSize > 0 && nextSize < MinimumSize)
                {
                    output.Height = MinimumSize;
                    output.Width = CCharacter.GetFontWidth(MinimumSize);
                    _wordsPerLine = currentHeight.Second;
                }
            }
        }
        return output;
    }

    private Tuple<float, int[]> GetTotalHeightAtFontSize(float fontSize)
    {
        var newLineSpacing = fontSize * NewLineBreakRatio;
        var amountPerLine = rectTransform.GetWidth();
        var widthOfSpace = CCharacter.GetFontWidth(fontSize) / 2;
        var wordsPerLine = new List<int>();
        foreach(var line in _trueText)
        {
            var currentLength = 0f; var wordsForThisLine = 0;
            foreach (var word in line)
            {
                var wordLength = word.WidthAt(fontSize);
                if(currentLength + wordLength > amountPerLine)
                {
                    wordsPerLine.Add(wordsForThisLine);
                    currentLength = wordLength;
                    wordsForThisLine = 1;
                }
                else if(currentLength + wordLength == amountPerLine)
                {
                    wordsPerLine.Add(++wordsForThisLine);
                    currentLength = 0f;
                }
                else
                {
                    wordsForThisLine++;
                    currentLength += wordLength + widthOfSpace;
                }
            }

            if (wordsForThisLine > 0)
            {
                wordsPerLine.Add(wordsForThisLine);
            }
        }

        var totalHeight = (fontSize * wordsPerLine.Count) + (newLineSpacing * (_trueText.Count() - 1));
        return new Tuple<float, int[]>(totalHeight, wordsPerLine.ToArray());
    }

    private void Update()
    {
        if (transform.hasChanged)
        {
            transform.hasChanged = false;
            Construct(false);
        }
    }
}

public class Character
{
    public string Value { get; set; }
    public Color Color { get; set; }
    public TextStyle Style { get; set; }

    public Character(string value, Color c, TextStyle style)
    {
        Value = value;
        Color = c;
        Style = style;
    }
}

public class MouseOverCharacters : Character
{
    public UnityAction MouseOverAction { get; set; }
    public bool IsEnd { get; set; }
    public MouseOverCharacters(UnityAction mouseOverAction, bool isEnd) : base(null, Color.clear, TextStyle.Normal)
    {
        MouseOverAction = mouseOverAction;
        IsEnd = isEnd;
    }
}

public class ClickableCharacters : Character
{
    public UnityAction ClickingAction { get; set; }
    public bool IsEnd { get; set; }
    public ClickableCharacters(UnityAction mouseOverAction, bool isEnd) : base(null, Color.clear, TextStyle.Normal)
    {
        ClickingAction = mouseOverAction;
        IsEnd = isEnd;
    }
}

public class Word
{
    public List<Character> Characters { get; set; }
    public int Length { get { return Characters.Where(y => y.GetType() != typeof(MouseOverCharacters) && y.GetType() != typeof(ClickableCharacters)).Count(); } }
    private Sprite[] CharactersArray { get; set; }

    public Word(Sprite[] charactersArray)
    {
        CharactersArray = charactersArray;
        Characters = new List<Character>();
    }

    public void Add(Character cc)
    {
        Characters.Add(cc);
    }

    public override string ToString()
    {
        var output = new StringBuilder();
        foreach(var character in Characters)
        {
            output.Append(character.Value);
        }
        return output.ToString();
    }

    public float WidthAt(float fontSize)
    {
        return Characters.Select(x => GetWidthOfCharacterAtHeight(x.Value, x.Style, fontSize)).Sum();
    }

    public float GetWidthOfCharacterAtHeight(string character, TextStyle style, float height)
    {
        if (character == " ")
        {
            return CCharacter.GetFontWidth(height) / 2;
        }

        var cc = CharactersArray[CCharacter.GetCharacterIndex(character, style)];
        var border = cc.border * (height / cc.rect.height);

        return CCharacter.GetFontWidth(height) - (border.x + border.z);
    }
}

public class Line
{
    public List<Word> Words { get; set; }
    public int Length { get { return Words.Count(); } }
    public int CharacterLength { get { return Words.Sum(x => x.Length) + (Length - 1); } }

    public Line()
    {
        Words = new List<Word>();
    }

    public Line(List<Word> words)
    {
        Words = words;
    }

    public void Add(Word word)
    {
        Words.Add(word);
    }

    public float WidthAt(float fontSize)
    {
        if (!Words.Any())
            return 0f;

        var spaceSize = Words.First().GetWidthOfCharacterAtHeight(" ", TextStyle.Normal, fontSize);

        return Words.Sum(x => x.WidthAt(fontSize)) + (Words.Count() - 1) * spaceSize;
    }
}

public class LineBreak : Line
{
    public LineBreak()
    {
        Words = null;
    }
}

public enum HorizontalAlignment
{
    Left,
    Center,
    Right
}
public enum VerticalAlignment
{
    Top,
    Center,
    Bottom
}
