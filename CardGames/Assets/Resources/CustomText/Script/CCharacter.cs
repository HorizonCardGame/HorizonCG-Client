﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CCharacter : MonoBehaviour {

    public Image Character;
    public Outline Outline;

    public RectTransform rectTransform { get; set; }
    public string CharacterStr { get; set; }
    public int CharacterId { get; set; }
    public float FontSize { get; set; }
    public Rect SpriteRect { get { return Character.sprite.rect; } }
    public Vector4 Border { get { return Character.sprite.border * (FontSize/SpriteRect.height); } }

    private Sprite[] CharactersArray { get; set; }

    // Use this for initialization
    void Awake () {
        CharactersArray = Resources.LoadAll<Sprite>(Constants.TextSpritesheetPath);
        rectTransform = GetComponent<RectTransform>();
	}

    public void Constructor(string character, float fontSize, TextStyle style, Color color, bool outline, float outlineThickness, Color outlineColor)
    {
        if(CharactersArray == null)
        {
            CharactersArray = Resources.LoadAll<Sprite>(Constants.TextSpritesheetPath);
            rectTransform = GetComponent<RectTransform>();
        }

        CharacterStr = character;
        FontSize = fontSize;

        if (character.Equals(" "))
        {
            Character.gameObject.SetActive(false);
            CharacterId = -1;
        }
        else
        {
            Character.gameObject.SetActive(true);
            CharacterId = GetCharacterIndex(character, style);
            Character.sprite = CharactersArray[CharacterId];
            Character.color = color;
            Outline.enabled = outline;
            Outline.effectDistance = new Vector2(outlineThickness, -outlineThickness);
            Outline.effectColor = outlineColor;
        }

        rectTransform.SetHeight(FontSize);
        var fontWidth = GetFontWidth(FontSize);
        rectTransform.SetWidth((CharacterId > -1) ? fontWidth : fontWidth/2);
    }
    public void Constructor(string character, float fontSize)
    {
        Constructor(character, fontSize, TextStyle.Normal, Color.black, false, 0f, Color.clear);
    }
    public void Constructor(string character, float fontSize, TextStyle style, Color color)
    {
        Constructor(character, fontSize, style, color, false, 0f, Color.clear);
    }

    #region Accessors
    public Size GetSize()
    {
        return new Size { Width = rectTransform.GetWidth() - ((Character.hasBorder) ? (Border.x + Border.z) : 0), Height = rectTransform.GetHeight() };
    }

    public void SetLocalPosition(Vector2 pos)
    {
        rectTransform.localPosition = pos - new Vector2(((Character.hasBorder) ? Border.x : 0), 0);
    }
    #endregion

    #region Static
    public static int GetCharacterIndex(string character, TextStyle style)
    {
        var output = 0;
        switch (character)
        {
            case "A": output = 0; break;
            case "B": output = 1; break;
            case "C": output = 2; break;
            case "D": output = 3; break;
            case "E": output = 4; break;
            case "F": output = 5; break;
            case "G": output = 6; break;
            case "H": output = 7; break;
            case "I": output = 8; break;
            case "J": output = 9; break;
            case "K": output = 10; break;
            case "L": output = 11; break;
            case "M": output = 12; break;
            case "N": output = 13; break;
            case "O": output = 14; break;
            case "P": output = 15; break;
            case "Q": output = 16; break;
            case "R": output = 17; break;
            case "S": output = 18; break;
            case "T": output = 19; break;
            case "U": output = 20; break;
            case "V": output = 21; break;
            case "W": output = 22; break;
            case "X": output = 23; break;
            case "Y": output = 24; break;
            case "Z": output = 25; break;
            case "a": output = 26; break;
            case "b": output = 27; break;
            case "c": output = 28; break;
            case "d": output = 29; break;
            case "e": output = 30; break;
            case "f": output = 31; break;
            case "g": output = 32; break;
            case "h": output = 33; break;
            case "i": output = 34; break;
            case "j": output = 35; break;
            case "k": output = 36; break;
            case "l": output = 37; break;
            case "m": output = 38; break;
            case "n": output = 39; break;
            case "o": output = 40; break;
            case "p": output = 41; break;
            case "q": output = 42; break;
            case "r": output = 43; break;
            case "s": output = 44; break;
            case "t": output = 45; break;
            case "u": output = 46; break;
            case "v": output = 47; break;
            case "w": output = 48; break;
            case "x": output = 49; break;
            case "y": output = 50; break;
            case "z": output = 51; break;
            case "1": output = 52; break;
            case "2": output = 53; break;
            case "3": output = 54; break;
            case "4": output = 55; break;
            case "5": output = 56; break;
            case "6": output = 57; break;
            case "7": output = 58; break;
            case "8": output = 59; break;
            case "9": output = 60; break;
            case "0": output = 61; break;
            case "-": output = 62; break;
            case "_": output = 63; break;
            case "+": output = 64; break;
            case ":": output = 65; break;
            case "*": output = 66; break;
            case "\"": output = 67; break;
            case "\"2": output = 68; break;
            case "'1": output = 69; break;
            case "'2": output = 70; break;
            case "'": output = 71; break;
            case "(": output = 72; break;
            case ")": output = 73; break;
            case ".": output = 74; break;
            case ",": output = 75; break;
            case "?": output = 76; break;
            case "!": output = 77; break;
            case "/": output = 78; break;
            case "&": output = 79; break;
            case "<0/>": return 240 + 0;
            case "<1/>": return 240 + 1;
            case "<2/>": return 240 + 2;
            case "<3/>": return 240 + 3;
            case "<4/>": return 240 + 4;
            case "<5/>": return 240 + 5;
            case "<6/>": return 240 + 6;
            case "<7/>": return 240 + 7;
            case "<8/>": return 240 + 8;
            case "<9/>": return 240 + 9;
            case "<X/>": return 240 + 10;
            case "<Def/>": return 240 + 11;
            case "<Atk/>": return 240 + 12;
            case "<dM/>": return 240 + 13;
            case "<dR/>": return 240 + 14;
            case "<dA/>": return 240 + 15;
            case "<dE/>": return 240 + 16;
            case "<dF/>": return 240 + 17;
            case "<dI/>": return 240 + 18;
            case "<dL/>": return 240 + 19;
            case "<dS/>": return 240 + 20;
            case "<aV/>": return 240 + 24;
            case "<aU/>": return 240 + 25;
            case "<aW/>": return 240 + 26;
            case "<aD/>": return 240 + 27;
            case "<aP/>": return 240 + 28;
            case "<aA/>": return 240 + 29;
            case " ": return output = -1;
            default:
                Debug.LogErrorFormat("Character {0} is not known.", character);
                return 240 + 79;
        }

        return (style == TextStyle.Bold) ? 80 + output : ( style == TextStyle.Italic) ? 160 + output : output;
    }

    public static float GetFontWidth(float height)
    {
        return (height * Constants.TextCharacterWidth) / Constants.TextCharacterHeight;
    }
    #endregion
}

public enum TextStyle
{
    Normal,
    Bold,
    Italic
}